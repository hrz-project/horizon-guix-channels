# Guix channels

Horizon project uses [GNU guix](https://guix.gnu.org/) to manage dependencies and try to tend towards bit-by-bit reproducibility.

This repository adds a new channel to the list of channel that invoking `guix pull` will track.

## Usage

Edit `~/.config/guix/channels.scm` and add:

``` scheme
;; Add horizon packages to those Guix provides:
(cons (channel
       (name 'horizon)
       (url "https://gitlab.com/hrz-project/horizon-guix-channels.git"))
      %default-channels)
```

## Note for guix devs

> Although it is encouraged to directly push contributions to the guix project, I would rather keep this in the horizon scope for now and submit for contributions directly to guix once I am sure the dependencies it tracks are well described.

# License

[![gpl-v3](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)
