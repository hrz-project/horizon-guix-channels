(define-module (horizon packages metagenomics)
  ;;
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages check)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cpp)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages django)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages machine-learning)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages time)
  #:use-module (gnu packages wget)
  ;;
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (guix build-system python)
  #:use-module (guix build-system r)
  #:use-module ((guix build cmake-build-system) #:prefix cmake)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  ;; horizon packages --------------------------------------------------------
  #:use-module (horizon packages bioinformatics)
  #:use-module (horizon packages alignment)
  #:use-module (horizon packages python))

(define-public anvio
  (package
    (name "anvio")
    (version "6.2")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "https://github.com/merenlab/anvio/releases/download/v"
             version "/anvio-" version ".tar.gz"))
       (sha256
        (base32 "0n4djbb2hlwmpb8rcrb00a6sskhblb7p2b9yy38xcqcl3h0ql9dg"))))
    (build-system python-build-system)
    (inputs
     `(("python-numpy" ,python-numpy)
       ("python-scipy" ,python-scipy)
       ("python-bottle" ,python-bottle)
       ("python-pysam" ,python-pysam)
       ("python-ete3" ,python-ete3)
       ("python-scikit-learn" ,python-scikit-learn)
       ("python-django" ,python-django)
       ("python-requests" ,python-requests)
       ("python-mistune" ,python-mistune)
       ("python-six" ,python-six)
       ("python-matplotlib" ,python-matplotlib)
       ("python-statsmodel" ,python-statsmodels)
       ("python-colored" ,python-colored)
       ("python-illumina-utils" ,python-illumina-utils)
       ("python-tabulate" ,python-tabulate)
       ("python-numba" ,python-numba-0.51.2)
       ("python-paste" ,python-paste)
       ("python-pyani" ,python-pyani)
       ("python-psutil" ,python-psutil)
       ("python-pandas" ,python-pandas)
       ("python-cython" ,python-cython)
       ("python-h5py" ,python-h5py)
       ("hdf5" ,hdf5)
       ;;
       ("snakemake" ,snakemake)
       ("zlib" ,zlib)
       ("gcc" ,gcc-9)
       ("openblas" ,openblas)
       ("lapack" ,lapack)
       ("prodigal" ,prodigal)
       ("hmmer" ,hmmer)
       ("sqlite" ,sqlite)))
    (arguments `(#:tests? #f))
    (home-page "http://merenlab.org/software/anvio/")
    (synopsis "An analysis and visualization platform for 'omics data")
    (description "Anvi’o is an open-source, community-driven analysis and
visualization platform for microbial ‘omics. It brings together many aspects
of today’s cutting-edge strategies including genomics, metagenomics,
metatranscriptomics, pangenomics, metapangenomics, phylogenomics, and
microbial population genetics in an integrated and easy-to-use fashion through
extensive interactive visualization capabilities.")
    (license license:gpl3)))

(define-public concoct
  (let ((commit "823dcd670bc42f6ea4622881c2484cda6c253a76"))
    (package
      (name "concoct")
      (version "1.10")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/BinPro/CONCOCT")
               (commit commit)))
         (sha256
          (base32 "0cy77w5knvrkp1rvnz0iif5klcp898zsyr0r8kq3vlxwj07cfh6g"))))
      (build-system python-build-system)
      (inputs
       `(("gsl" ,gsl)
         ("python" ,python-wrapper)
         ("python-biopython" ,python-biopython)
         ("python-nose" ,python-nose)
         ("python-numpy" ,python-numpy)
         ("python-pandas" ,python-pandas)
         ("python-dateutil" ,python-dateutil)
         ("python-pytz" ,python-pytz)
         ("python-scikit-learn" ,python-scikit-learn)
         ("python-scipy" ,python-scipy)
         ("python-cython" ,python-cython)))
      (propagated-inputs
       `(("bedtools" ,bedtools)
         ("samtools" ,samtools)
         ("mummer" ,mummer)))
      (arguments `(#:tests? #f))
      (home-page "https://github.com/BinPro/CONCOCT")
      (synopsis "Clustering Contigs With Coverage And Composition")
      (description "A program for unsupervised binning of metagenomic contigs
by using nucleotide composition, coverage data in multiple samples and linkage
data from paired end reads.")
      (license license:bsd-2))))

(define-public ganon
  (package
    (name "ganon")
    (version "0.3.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url  "https://github.com/pirovc/ganon")
             (commit version)))
       (sha256 (base32 "09mwv12h1h2apysrvyi911ja6frdvfg5d94scmhcw0qiq4w5wcdp"))))
    (build-system python-build-system)
    (native-inputs
     `(("cmake" ,cmake)
       ("gcc" ,gcc-9)
       ("zlib" ,zlib)))
    (inputs
     `(("zlib" ,zlib)
       ("seqan" ,(let ((commit "d8b3fcd55858c150bc1dd95fa3cf4c8ebb640829")
                       (name "seqan")
                       (revision "0"))
                   (origin
                     (method git-fetch)
                     (uri (git-reference
                           (url "https://github.com/eseiler/seqan")
                           (commit commit)))
                     (file-name (git-file-name name (git-version "2" revision commit)))
                     (sha256 (base32 "0wxm5723jdlbns80w8gwg9h3z3jr1m5756ri0ddyz478imlrl8ka")))))
       ("cxxopts" ,cxxopts)
       ("sdsl-lite" ,(let ((commit "9a0d5676fd09fb8b52af214eca2d5809c9a32dbe")
                           (revision "0")
                           (name "sdsl-lite"))
                       (origin
                         (method git-fetch)
                         (uri (git-reference
                               (url "https://github.com/xxsds/sdsl-lite/")
                               (commit commit)))
                         (file-name (git-file-name name (git-version "3" revision commit)))
                         (sha256 (base32 "0xmckzf6rxzp3qffc2nbcys3l3kg17g4a69ivgac195ybhnrm7q8")))))
       ("catch2" ,catch-framework2)))
    (propagated-inputs
     `(("python" ,python-wrapper)
       ("python-pandas" ,python-pandas)
       ("gawk" ,gawk)
       ("grep" ,grep)
       ("tar" ,tar)
       ("curl" ,curl)
       ("wget" ,wget)
       ("zcat" ,coreutils)
       ("pylca" ,python-pylca)
       ("taxsbp" ,python-taxsbp)))
    (arguments
     `(;; #:configure-flags
       ;; (list "-DGANON_OFFSET=ON"
       ;;       "-DCONDA=OFF"
       ;;       "-DCMAKE_EXPORT_COMPILE_COMMANDS=ON")
       #:phases
       (modify-phases (@ (guix build python-build-system) %standard-phases)
         (delete 'check)
         (add-after 'unpack 'add-third-party
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((unpack (lambda (source target)
                             (with-directory-excursion (string-append "libs/" target)
                               (if (file-is-directory? (assoc-ref inputs source))
                                   (copy-recursively (assoc-ref inputs source) ".")
                                   (invoke "tar" "xvf"
                                           (assoc-ref inputs source)
                                           "--strip-components=1"))))))
               (and
                (unpack "seqan" "seqan")
                (unpack "sdsl-lite" "sdsl-lite")))))
         (add-after 'install 'configure-cmake
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out"))
                   (srcdir (getcwd)))
               (mkdir "../build") (chdir "../build")
               (format #t "build directory: ~s~%" (getcwd))
               (let ((args `(,srcdir
                             ,(string-append "-DCMAKE_BUILD_TYPE=" "Release")
                             ,(string-append "-DCMAKE_INSTALL_PREFIX=" out)
                             ;; ensure that the libraries are installed into /lib
                             "-DCMAKE_INSTALL_LIBDIR=lib"
                             ;; add input libraries to rpath
                             "-DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE"
                             ;; add (other) libraries of the project itself to rpath
                             ,(string-append "-DCMAKE_INSTALL_RPATH=" out "/lib")
                             ;; enable verbose output from builds
                             "-DCMAKE_VERBOSE_MAKEFILE=ON"
                             "-DVERBOSE_CONFIG=ON"
                             "-DGANON_OFFSET=ON"
                             "-DCMAKE_EXPORT_COMPILE_COMMANDS=ON"
                             "-DCONDA=OFF")))
                 (apply invoke "cmake" args)))))
         (add-after 'configure-cmake 'build-ganon
           (lambda _ (invoke "make" (string-append "-j" (number->string (parallel-job-count)))) #t))
         (add-after 'build-ganon 'install-ganon
           (lambda _ (invoke "make" "install") #t)))))
    (home-page "https://github.com/pirovc/ganon")
    (synopsis "A read classification tool based on Interleaved Bloom Filters")
    (description "A k-mer based read classification tool which uses
Interleaved Bloom Filters in conjunction with a taxonomic clustering and
a k-mer counting-filtering scheme.")
    (license license:expat)))

(define-public kraken
  (package
    (name "kraken")
    (version "2.1.0")
    (source (origin
              (method url-fetch)
              (uri (string-append
                    "https://github.com/DerrickWood/kraken2/archive/v"
                    version
                    ".tar.gz"))
              (sha256 (base32 "1cki3yq493sjbmcwj9kjwia4i67801klh6plmycf5w7zsfngd8y2"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("gcc" ,gcc-9)
       ("sed" ,sed)))
    (inputs
     `(("perl" ,perl)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'check)
         (delete 'install)
         (add-after 'unpack 'patch-install-script
           (lambda _
             (invoke "sed" "-i"
                     (string-append
                      "26d;"
                      "27iexport KRAKEN2_DIR=\"\\$1\"")
                     "install_kraken2.sh")
             (invoke "sed" "-i"
                     (string-append
                      "29s|make -C src install|"
                      "make -C src -j" (number->string (parallel-job-count))
                      " install"
                      " CXX=" (which "g++")
                      "|")
                     "install_kraken2.sh")
             #t))
         (replace 'build
           (lambda* (#:key outputs #:allow-other-keys)
             (invoke "./install_kraken2.sh" (string-append (assoc-ref outputs "out") "/bin")))))))
    (home-page "https://ccb.jhu.edu/software/kraken2/")
    (synopsis "Taxonomic sequence classification system")
    (description "Kraken is a taxonomic sequence classifier that assigns
taxonomic labels to DNA sequences. Kraken examines the k-mers within a query
sequence and uses the information within those k-mers to query
a database. That database maps k-mers to the lowest common ancestor (LCA) of
all genomes known to contain a given k-mer.")
    (license license:expat)))

(define-public metamvgl
  (let ((commit "8d6e1a94092ab6443687ff354a9336cadac66002")
        (revision "0"))
    (package
      (name "metamvgl")
      (version (git-version "0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/ZhangZhenmiao/METAMVGL")
                      (commit commit)))
                (sha256 (base32 "1knkf4943p4n6fm9pfgwacv4di8ixyylgzr1fggi8d601a18ipjd"))))
      (build-system gnu-build-system)
      (inputs
       `(("htslib" ,htslib-1.10)
         ("gcc" ,gcc-9)))
      (propagated-inputs
       `(("python" ,python-wrapper)
         ("python-numpy" ,python-numpy)
         ("python-scipy" ,python-scipy)
         ("python-networkx" ,python-networkx)
         ("megahit" ,megahit)))
      (arguments
       `(#:imported-modules
         (,@%gnu-build-system-modules
          (guix build python-build-system))
         #:modules
         ((guix build gnu-build-system)
          (guix build utils)
          ((guix build python-build-system) #:prefix py:))
         #:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (delete 'check)
           (add-after 'build 'chmods
             (lambda _ (chmod "METAMVGL.py" #o555)))
           (add-before 'build 'set-htslib-path
             (lambda* (#:key inputs #:allow-other-keys)
               (let ((htslib (assoc-ref inputs "htslib")))
                 (substitute* '("Makefile")
                   (("-O3") (string-append "-O3 -I" htslib "/include/htslib"))
                   (("-lhts") (string-append "-lhts -L" htslib "/lib"))))))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin")))
                 (install-file "METAMVGL.py" bin)
                 (install-file "prep_graph" bin))))
           (add-after 'install 'wrap
             (assoc-ref py:%standard-phases 'wrap)))))
      (home-page "https://github.com/ZhangZhenmiao/METAMVGL")
      (synopsis "A multi-view graph-based metagenomic contig binning algorithm
by integrating assembly and paired-end graphs")
      (description "METAMVGL, a multi-view graph-based metagenomic contig
binning algorithm by integrating both assembly and paired-end graphs. It could
strikingly rescue the short contigs and correct the binning errors from dead
ends subgraphs. METAMVGL could learn the graphs’ weights automatically and
predict the contig labels in a uniform multi-view label propagation
framework. In the experiments, we observed METAMVGL significantly increased
the high-confident edges in the combined graph and linked dead ends to the
main graph. It also outperformed with many state-of-the-art binning methods,
MaxBin2, MetaBAT2, MyCC, CONCOCT, SolidBin and Graphbin on the metagenomic
sequencing from simulation, two mock communities and real Sharon data.")
      (license #f))))
