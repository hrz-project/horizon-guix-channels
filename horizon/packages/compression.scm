(define-module (horizon packages compression)
  #:use-module (guix licenses)
  #:use-module ((guix build utils) #:hide (delete which))
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  ;;
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages python))

(define-public csnappy
  (let ((commit "6c10c305e8dde193546e6b33cf8a785d5dc123e2")
        (version "0"))
    (package
      (name "csnappy")
      (version (git-version "0" version commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/zeevt/csnappy")
               (commit commit)))
         (sha256 (base32 "0xkl2pir91yl2dd8aznf30z8fc0x5y0sc5wn3mcx03gbw1v11f5c"))))
      (inputs
       `(("python" ,python-3)))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (delete 'bootstrap)
           (delete 'configure)
           (delete 'check)
           (add-after 'unpack 'fix-prefix-path
             (lambda* (#:key outputs #:allow-other-keys)
               (substitute* '("Makefile")
                 (("^PREFIX := /usr")
                  (string-append "PREFIX = " (assoc-ref outputs "out"))))))
           (add-before 'build 'setenv-cc
             (lambda _ (setenv "CC" (which "gcc")) #t))
           (replace 'build
             (lambda _
               (invoke "make" "libcsnappy.so")
               #t)))))
      (build-system gnu-build-system)
      (home-page "https://github.com/zeevt/csnappy")
      (synopsis "Google snappy in C for Linux Kernel")
      (description "cut out or ported Google Snappy to plain ANSI C the
necessary code and headers.  To cause less confusion, I call this project (and
files) csnappy.  The API looks right, but I welcome comments.  The code *has*
been tested in kernel-space using a patched zram and it works.")
      (license #f))))

(define-public miniz
  (package
    (name "miniz")
    (version "2.1.0")
    (source
     (origin
       (method url-fetch)
       (uri (format #f "https://github.com/richgel999/miniz/archive/~a.tar.gz"
                    version))
       (sha256 (base32 "1shzzk6kwrs7f0xr6g9cr0lp6fvjsm73m8l90dkx56i1j8yb5ycm"))))
    (build-system cmake-build-system)
    (arguments
     `(#:configure-flags
       (list "-DCMAKE_C_FLAGS=-fPIC")
       #:tests? #f))
    (home-page "https://github.com/richgel999/miniz")
    (synopsis "miniz: Single C source file zlib-replacement library")
    (description "Miniz is a lossless, high performance data compression
library in a single source file that implements the zlib (RFC 1950) and
Deflate (RFC 1951) compressed data format specification standards. It supports
the most commonly used functions exported by the zlib library, but is
a completely independent implementation so zlib's licensing requirements do
not apply. Miniz also contains simple to use functions for writing .PNG format
image files and reading/writing/appending .ZIP format archives. Miniz's
compression speed has been tuned to be comparable to zlib's, and it also has
a specialized real-time compressor function designed to compare well against
fastlz/minilzo.")
    (license expat)))
