;; GNU Guix for Horizon

;;; Copyright © 2020 Samuel Barreto <samuel.barreto8@gmail.com>

(define-module (horizon packages development)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  ;;
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system go)
  #:use-module (guix build-system gnu)
  ;;
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages jemalloc)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libunwind)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages python)
  #:use-module (gnu packages tls))

(define-public go-github-com-kr-pty
  (package
    (name "go-github-com-kr-pty")
    (version "1.1.8")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/kr/pty")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                ;; FIXME sha
                "9dwhch53vqxpnbiqvfa27cliabx9ma2m4dax4adlrz8rami4sakw"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/kr/pty"))
    (synopsis "Pty is a Go package for using unix pseudo-terminals.")
    (description "Pty is a Go package for using unix pseudo-terminals.")
    (home-page "https://github.com/kr/pty")
    (license #f)))                      ; MIT


(define-public go-github-com-creack-pty
  (package
    (name "go-github-com-creack-pty")
    (version "1.1.11")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/creack/pty")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0dwhch53vqxpnbiqvfa27cliabx9ma2m4dax4adlrz8rami4sakw"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/creack/pty"))
    (synopsis "Pty is a Go package for using unix pseudo-terminals.")
    (description "Pty is a Go package for using unix pseudo-terminals.")
    (home-page "https://github.com/creack/pty")
    (license #f)))                      ; MIT


(define-public go-github-com-kortschak-utter
  (package
    (name "go-github-com-kortschak-utter")
    (version "1.0.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/kortschak/utter")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1pwnpzpdmrpc2irwi9gmm0746gis1jyyqsvhj37b3d7psyzf9p9a"))))
    (build-system go-build-system)
    (arguments
     '(#:import-path "github.com/kortschak/utter"))
    (synopsis "Utter is a tool for taking snapshots of data structures to include in tests or other code")
    (description "utter is a fork of the outstanding go-spew tool.
Where go-spew is an aid for debugging, providing annotation of dumped
datastructures, utter is a tool for taking snapshots of data
structures to include in tests or other code. An utter dump will not
construct cyclic structure literals and a number of pseudo-code
representations of pointer-based structures will require subsequent
processing.")
    (home-page "https://github.com/kortschak/utter")
    (license license:isc)))


(define-public safestringlib
  (let ((commit "245c4b8cff1d2e7338b7f3a82828fc8e72b29549"))
    (package
      (name "safestringlib")
      (version "1.0.0")
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://github.com/intel/safestringlib")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32 "0hv75sg4kw9pnmp8vjw7a7vj0b5mijaimafd8hzb3jxcm0x3k435"))))
      (build-system gnu-build-system)
      (arguments
       `(#:tests?
         #f
         #:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (replace 'build
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke "make")))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (lib (string-append out "/lib"))
                      (inc (string-append out "/include")))
                 (mkdir-p inc)
                 (for-each (lambda (file) (install-file file inc))
                           (find-files "include" ".*"))
                 (install-file "libsafestring.a" lib)
                 #t))))))
      (home-page "https://github.com/intel/safestringlib")
      (synopsis "C library for safe string and memory operations")
      (description "This library includes routines for safe string
operations (like strcpy) and memory routines (like memcpy) that are
recommended for Linux/Android operating systems, and will also work
for Windows. This library is especially useful for cross-platform
situations where one library for these routines is preferred.")
      (license #f))))                      ; FIXME when mit become available


(define-public folly
  (package
    (name "folly")
    (version "2020.10.05.00")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/facebook/folly")
             (commit (string-append "v" version))))
       (sha256 (base32 "0q4w4cvjxffc462hvs8h4zryq4965j7015zvkwagcm6cj6wmz3cn"))))
    (build-system cmake-build-system)
    (inputs
     `(("gcc" ,gcc-9)
       ("boost" ,boost)
       ("libevent" ,libevent)
       ("double-conversion" ,double-conversion)
       ("google-glog" ,glog)
       ("gflags" ,gflags)
       ("libiberty" ,libiberty)
       ("lz4" ,lz4)
       ("lzma" ,xz)
       ("snappy" ,snappy)
       ("zlib" ,zlib)
       ("binutils" ,binutils)
       ("jemalloc" ,jemalloc)
       ("ssl" ,openssl)
       ("pkg-config" ,pkg-config)
       ("libunwind" ,libunwind)
       ("fmt" ,fmt)
       ("python" ,python-wrapper)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
           (lambda* (#:key outputs (configure-flags '()) (out-of-source? #t)
                           #:allow-other-keys)
             "Configure the given package."
             (let* ((out        (assoc-ref outputs "out"))
                    (abs-srcdir (getcwd))
                    (srcdir     (if out-of-source?
                                    (string-append "../" (basename abs-srcdir))
                                    ".")))
               (format #t "source directory: ~s (relative from build: ~s)~%"
                       abs-srcdir srcdir)
               (when out-of-source?
                 (mkdir-p "../build")
                 (chdir "../build"))
               (format #t "build directory: ~s~%" (getcwd))

    (let ((args `(,srcdir
                  ,(string-append "-DCMAKE_INSTALL_PREFIX=" out)
                  ;; ensure that the libraries are installed into /lib
                  "-DCMAKE_INSTALL_LIBDIR=lib"
                  ;; add input libraries to rpath
                  "-DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE"
                  ;; add (other) libraries of the project itself to rpath
                  ,(string-append "-DCMAKE_INSTALL_RPATH=" out "/lib")
                  ;; enable verbose output from builds
                  "-DCMAKE_VERBOSE_MAKEFILE=ON"
                  ,@configure-flags)))
      (format #t "running 'cmake' with arguments ~s~%" args)
      (apply invoke "cmake" args))))))))
    (home-page "https://github.com/facebook/folly")
    (synopsis "An open-source C++ library developed and used at Facebook.")
    (description "Folly (acronymed loosely after Facebook Open Source
Library) is a library of C++14 components designed with practicality and
efficiency in mind. Folly contains a variety of core library components used
extensively at Facebook. In particular, it's often a dependency of Facebook's
other open source C++ efforts and place where those projects can share
code.")
    (license license:asl2.0)))


(define-public libmaus2
  (let ((commit "cf12f07b23f20bfa5c84fd730d46350f32b0e105")
        (revision "1"))
    (package
      (name "libmaus2")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/akahles/libmaus2")
               (commit commit)))
         (sha256 (base32 "1xkz25qw1f4ffak9k2xg7w46a5jkac024y0kaiz6mvs5ikyjrqs9"))))
      (build-system cmake-build-system)
      (inputs
       `(("zlib" ,zlib)))
      (arguments `(#:tests? #f))
      (home-page "https://github.com/akahles/libmaus2")
      (synopsis "collection of data structures and algorithms")
      (description "libmaus2 is a collection of data structures and algorithms. It contains

@itemize
@item I/O classes (single byte and UTF-8)
@item bitio classes (input, output and various forms of bit level manipulation)
@item text indexing classes (suffix and LCP array, fulltext and minute (FM), ...)
@item BAM sequence alignment files input/output (simple and collating)
@end itemize

and many lower level support classes.")
      (license license:gpl3))))


(define-public sdsl-lite-hmusta
  (let ((commit "c47378a5d569812bb55e90978b654fd94387581f")
        (revision "0"))
    (package
      (inherit sdsl-lite)
      (name "sdsl-lite-hmusta")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/hmusta/sdsl-lite")
               (commit commit)
               (recursive? #t)))
         (sha256 (base32 "0apdgwjspz3f1c0gbi9x5zn1041mqp2nhidznn4n37795r28ybh8"))))
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f))
      (home-page "https://github.com/hmusta/sdsl-lite"))))
