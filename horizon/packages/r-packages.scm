;; GNU Guix for Horizon

;;; Copyright © 2020 Samuel Barreto <samuel.barreto8@gmail.com>

(define-module (horizon packages r-packages)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages java)
  #:use-module (gnu packages statistics)
  #:use-module (guix build utils)
  #:use-module (guix build-system r)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  ;;
  #:use-module (horizon packages development))


(define-public r-fastqcr
  (package
    (name "r-fastqcr")
    (version "0.1.2")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "fastqcr" version))
       (sha256
        (base32
         "12x3lkg5zc4ckyg4x3xxqb779yhrr0fys7asf5b8shz49f86fmm9"))))
    (properties `((upstream-name . "fastqcr")))
    (build-system r-build-system)
    (inputs
     `(("fastqc" ,fastqc)))
    (propagated-inputs
     `(("r-dplyr" ,r-dplyr)
       ("r-ggplot2" ,r-ggplot2)
       ("r-gridextra" ,r-gridextra)
       ("r-magrittr" ,r-magrittr)
       ("r-readr" ,r-readr)
       ("r-rmarkdown" ,r-rmarkdown)
       ("r-rvest" ,r-rvest)
       ("r-scales" ,r-scales)
       ("r-tibble" ,r-tibble)
       ("r-tidyr" ,r-tidyr)
       ("r-xml2" ,r-xml2)))
    (home-page
     "http://www.sthda.com/english/rpkgs/fastqcr/")
    (synopsis "Quality Control of Sequencing Data")
    (description
     "'FASTQC' is the most widely used tool for evaluating the quality
of high throughput sequencing data. It produces, for each sample, an
html report and a compressed file containing the raw data. If you have
hundreds of samples, you are not going to open up each 'HTML' page.
You need some way of looking at these data in aggregate. 'fastqcr'
Provides helper functions to easily parse, aggregate and analyze
'FastQC' reports for large numbers of samples. It provides
a convenient solution for building a 'Multi-QC' report, as well as,
a 'one-sample' report with result interpretations.")
    (license license:gpl2)))



(define-public r-pheatmap
  (package
   (name "r-pheatmap")
   (version "1.0.12")
   (source
    (origin
     (method url-fetch)
     (uri (cran-uri "pheatmap" version))
     (sha256
      (base32
       "1hdh74az3vyzz6dqa311rhxdm74n46lyr03p862kn80p0kp9d7ap"))))
   (properties `((upstream-name . "pheatmap")))
   (build-system r-build-system)
   (propagated-inputs
    `(("r-gtable" ,r-gtable)
      ("r-rcolorbrewer" ,r-rcolorbrewer)
      ("r-scales" ,r-scales)))
   (home-page
    "https://cran.r-project.org/web/packages/pheatmap")
   (synopsis "Pretty Heatmaps")
   (description
    "Implementation of heatmaps that offers more control over dimensions and appearance.")
   (license license:gpl2)))

(define-public r-ape
  (package
    (name "r-ape")
    (version "5.4-1")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "ape" version))
       (sha256
        (base32
         "1r7fwyz30ippcl1ibqiv1xryf44x5crcks5kx19h146ffj76qcgh"))))
    (properties `((upstream-name . "ape")))
    (build-system r-build-system)
    (propagated-inputs
     `(("r-lattice" ,r-lattice)
       ("r-nlme" ,r-nlme)
       ("r-rcpp" ,r-rcpp)))
    (home-page "http://ape-package.ird.fr/")
    (synopsis
     "Analyses of Phylogenetics and Evolution")
    (description
     "This package provides functions for reading, writing, plotting, and manipulating phylogenetic trees, analyses of comparative data in a phylogenetic framework, ancestral character analyses, analyses of diversification and macroevolution, computing distances from DNA sequences, reading and writing nucleotide sequences as well as importing from BioConductor, and several tools such as Mantel's test, generalized skyline plots, graphical exploration of phylogenetic data (alex, trex, kronoviz), estimation of absolute evolutionary rates and clock-like trees using mean path lengths and penalized likelihood, dating trees with non-contemporaneous sequences, translating DNA into AA sequences, and assessing sequence alignments.  Phylogeny estimation can be done with the NJ, BIONJ, ME, MVR, SDM, and triangle methods, and several methods handling incomplete distance matrices (NJ*, BIONJ*, MVR*, and the corresponding triangle method).  Some functions call external applications (PhyML, Clustal, T-Coffee, Muscle) whose results are returned into R.")
    (license #f)))

(define-public r-phytools
  (package
    (name "r-phytools")
    (version "0.7-47")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "phytools" version))
       (sha256
        (base32
         "18scm5p9g4ivkg108r21lxhaailc3h91nblxrfwjfah88bqb1cgy"))))
    (properties `((upstream-name . "phytools")))
    (build-system r-build-system)
    (propagated-inputs
     `(("r-animation" ,r-animation)
       ("r-ape" ,r-ape)
       ("r-clustergeneration" ,r-clustergeneration)
       ("r-coda" ,r-coda)
       ("r-combinat" ,r-combinat)
       ("r-expm" ,r-expm)
       ("r-gtools" ,r-gtools)
       ("r-maps" ,r-maps)
       ("r-mass" ,r-mass)
       ("r-mnormt" ,r-mnormt)
       ("r-nlme" ,r-nlme)
       ("r-numderiv" ,r-numderiv)
       ("r-phangorn" ,r-phangorn)
       ("r-plotrix" ,r-plotrix)
       ("r-scatterplot3d" ,r-scatterplot3d)))
    (home-page
     "http://github.com/liamrevell/phytools")
    (synopsis
     "Phylogenetic Tools for Comparative Biology (and Other Things)")
    (description
     "This package provides a wide range of functions for phylogenetic analysis.  Functionality is concentrated in phylogenetic comparative biology, but also includes numerous methods for visualizing, manipulating, reading or writing, and even inferring phylogenetic trees and data.  Included among the functions in phylogenetic comparative biology are various for ancestral state reconstruction, model-fitting, simulation of phylogenies and data, and multivariate analysis.  There are a broad range of plotting methods for phylogenies and comparative data which include, but are not restricted to, methods for mapping trait evolution on trees, for projecting trees into phenotypic space or a geographic map, and for visualizing correlated speciation between trees.  Finally, there are a number of functions for reading, writing, analyzing, inferring, simulating, and manipulating phylogenetic trees and comparative data not covered by other packages.  For instance, there are functions for randomly or non-randomly attaching species or clades to a phylogeny, for estimating supertrees or consensus phylogenies from a set, for simulating trees and phylogenetic data under a range of models, and for a wide variety of other manipulations and analyses that phylogenetic biologists might find useful in their research.")
    (license license:gpl2+)))

(define-public r-animation
  (package
    (name "r-animation")
    (version "2.6")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "animation" version))
       (sha256
        (base32
         "02jv4h9hpp8niw9656r5n36kqr71jbyynxnywkkkdi0aj8w3cach"))))
    (properties `((upstream-name . "animation")))
    (build-system r-build-system)
    (propagated-inputs `(("r-magick" ,r-magick)))
    (home-page "https://yihui.name/animation")
    (synopsis
     "A Gallery of Animations in Statistics and Utilities to Create Animations")
    (description
     "This package provides functions for animations in statistics, covering topics in probability theory, mathematical statistics, multivariate statistics, non-parametric statistics, sampling survey, linear models, time series, computational statistics, data mining and machine learning.  These functions may be helpful in teaching statistics and data analysis.  Also provided in this package are a series of functions to save animations to various formats, e.g.  Flash, 'GIF', HTML pages, 'PDF' and videos. 'PDF' animations can be inserted into 'Sweave' / 'knitr' easily.")
    (license (list license:gpl2+ license:gpl3+))))

(define-public r-clustergeneration
  (package
    (name "r-clustergeneration")
    (version "1.3.4")
    (source
     (origin
       (method url-fetch)
       (uri (cran-uri "clusterGeneration" version))
       (sha256
        (base32
         "1ak8p2sxz3y9scyva7niywyadmppg3yhvn6mwjq7z7cabbcilnbw"))))
    (properties
     `((upstream-name . "clusterGeneration")))
    (build-system r-build-system)
    (propagated-inputs `(("r-mass" ,r-mass)))
    (home-page
     "https://cran.r-project.org/web/packages/clusterGeneration")
    (synopsis
     "Random Cluster Generation (with Specified Degree of Separation)")
    (description
     "We developed the clusterGeneration package to provide functions for generating random clusters, generating random covariance/correlation matrices, calculating a separation index (data and population version) for pairs of clusters or cluster distributions, and 1-D and 2-D projection plots to visualize clusters.  The package also contains a function to generate random clusters based on factorial designs with factors such as degree of separation, number of clusters, number of variables, number of noisy variables.")
    (license license:gpl2+)))

(define-public r-distill
  (package
    (name "r-distill")
    (version "0.8")
    (source
      (origin
        (method url-fetch)
        (uri (cran-uri "distill" version))
        (sha256
          (base32
            "0crih4x2745aq0w2fqrzcnbw82yi2pxcx8j2hv0fi5yfvgjjsi86"))))
    (properties `((upstream-name . "distill")))
    (build-system r-build-system)
    (propagated-inputs
      `(("r-base64enc" ,r-base64enc)
        ("r-bookdown" ,r-bookdown)
        ("r-digest" ,r-digest)
        ("r-downloader" ,r-downloader)
        ("r-htmltools" ,r-htmltools)
        ("r-jsonlite" ,r-jsonlite)
        ("r-knitr" ,r-knitr)
        ("r-lubridate" ,r-lubridate)
        ("r-mime" ,r-mime)
        ("r-openssl" ,r-openssl)
        ("r-png" ,r-png)
        ("r-progress" ,r-progress)
        ("r-rmarkdown" ,r-rmarkdown)
        ("r-rprojroot" ,r-rprojroot)
        ("r-rstudioapi" ,r-rstudioapi)
        ("r-stringr" ,r-stringr)
        ("r-whisker" ,r-whisker)
        ("r-xfun" ,r-xfun)
        ("r-xml2" ,r-xml2)
        ("r-yaml" ,r-yaml)))
    (home-page "https://github.com/rstudio/distill")
    (synopsis
      "'R Markdown' Format for Scientific and Technical Writing")
    (description
      "Scientific and technical article format for the web. 'Distill' articles feature attractive, reader-friendly typography, flexible layout options for visualizations, and full support for footnotes and citations.")
    (license license:asl2.0)))
