(define-module (horizon packages rust)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system cargo)
  #:use-module (gnu packages)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages rust)
  #:use-module (gnu packages crates-io))

(define-public rust-triple-accel
  (package
    (name "rust-triple-accel")
    (version "0.3.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "triple_accel" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0v795l496crk3h6yff9zh1cjyrh5s9v23fbgccc4dpz25z70jav2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-criterion" ,rust-criterion-0.3)
         ("rust-rand" ,rust-rand-0.7))))
    (home-page
      "https://github.com/Daniel-Liu-c0deb0t/triple_accel")
    (synopsis
      "Rust edit distance routines accelerated using SIMD. Supports fast Hamming, Levenshtein, restricted Damerau-Levenshtein, etc. distance calculations and string search.")
    (description
      "Rust edit distance routines accelerated using SIMD.  Supports fast Hamming, Levenshtein, restricted Damerau-Levenshtein, etc.  distance calculations and string search.")
    (license license:expat)))

(define-public rust-strum-macros
  (package
    (name "rust-strum-macros")
    (version "0.19.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum_macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mgqs5x3g0d3bmr8dhalgqrzh29dwc90a06fpy0cnich52zb06z6"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-heck" ,rust-heck-0.3)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))
        #:cargo-development-inputs
        (("rust-strum" ,rust-strum))))
    (home-page
      "https://github.com/Peternator7/strum")
    (synopsis
      "Helpful macros for working with enums and strings")
    (description
      "Helpful macros for working with enums and strings")
    (license license:expat)))

(define-public rust-strum
  (package
    (name "rust-strum")
    (version "0.19.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1d8i5xwkc2z7z02ibln80z1bmpjhpi9k5ckpljwj0mrvgrm2i6mq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-strum-macros" ,rust-strum-macros))
        #:cargo-development-inputs
        (("rust-strum-macros" ,rust-strum-macros))))
    (home-page
      "https://github.com/Peternator7/strum")
    (synopsis
      "Helpful macros for working with enums and strings")
    (description
      "Helpful macros for working with enums and strings")
    (license license:expat)))

(define-public rust-statrs
  (package
    (name "rust-statrs")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "statrs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0r00b60zlsn6srb6m6bzbw3w5cyihcy4w2rfjav64x4viy5bad0y"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-nalgebra" ,rust-nalgebra-0.21)
         ("rust-rand" ,rust-rand-0.7))
        #:cargo-development-inputs
        (("rust-criterion" ,rust-criterion-0.3))))
    (home-page "https://github.com/boxtown/statrs")
    (synopsis
      "Statistical computing library for Rust")
    (description
      "Statistical computing library for Rust")
    (license license:expat)))

(define-public rust-snafu-derive
  (package
    (name "rust-snafu-derive")
    (version "0.6.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "snafu-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1rfkf10q3sqj12jv9bxfiwvksh3z0q393641clz2z7x86a3l8wvh"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page "")
    (synopsis "An ergonomic error handling library")
    (description
      "An ergonomic error handling library")
    (license (list license:expat license:asl2.0))))

(define-public rust-snafu
  (package
    (name "rust-snafu")
    (version "0.6.9")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "snafu" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02cz2myk1nmsxnlr444a72jwsnsidvyh7dniiy8zw6k9wi360klw"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-backtrace" ,rust-backtrace-0.3)
         ("rust-doc-comment" ,rust-doc-comment-0.3)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-futures" ,rust-futures-0.3)
         ("rust-futures-core" ,rust-futures-core-0.3)
         ("rust-pin-project" ,rust-pin-project-0.4)
         ("rust-snafu-derive" ,rust-snafu-derive))))
    (home-page "")
    (synopsis "An ergonomic error handling library")
    (description
      "An ergonomic error handling library")
    (license (list license:expat license:asl2.0))))

(define-public rust-semver-0.1.0
  (package
    (name "rust-semver")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "semver" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
           "00b56gkxal340rnypw4rph7ddqvcahr7cqcbxi04xlb0v1nq2aqm"))))
    (arguments
     `(#:rust ,rust-1.46))
    (build-system cargo-build-system)
    (home-page "https://docs.rs/crate/semver/")
    (synopsis
      "Semantic version parsing and comparison.
")
    (description
      "Semantic version parsing and comparison.
")
    (license (list license:expat license:asl2.0))))

(define-public rust-semver-0.1.20
  (package
    (name "rust-semver")
    (version "0.1.20")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "semver" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1b10m0hxrr947gp41lj9vnmgl5ddwx3d41vnblsg06ppvkz11x6l"))))
    (build-system cargo-build-system)
    (home-page "https://docs.rs/crate/semver/")
    (synopsis
      "Semantic version parsing and comparison.
")
    (description
      "Semantic version parsing and comparison.
")
    (license (list license:expat license:asl2.0))))



(define-public rust-rustc-version-0.1.7
  (package
    (name "rust-rustc-version")
    (version "0.1.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rustc_version" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1160jjsqhqr25cvhr48hmpp8v61bjvjcnxzb0cyf4373lmp3gxf5"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs (("rust-semver" ,rust-semver-0.1.20))))
    (home-page "")
    (synopsis
      "A library for querying the version of a installed rustc compiler")
    (description
      "This package provides a library for querying the version of a installed rustc compiler")
    (license (list license:expat license:asl2.0))))


(define-public rust-newtype-derive-0.1.6
  (package
    (name "rust-newtype-derive")
    (version "0.1.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "newtype_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1v3170xscs65gjx5vl1zjnqp86wngbzw3n2q74ibfnqqkx6x535c"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
        (("rust-custom-derive" ,rust-custom-derive-0.1)
         ("rust-rustc-version" ,rust-rustc-version-0.1.7))))
    (home-page "")
    (synopsis
      "This crate provides macros for deriving common traits for newtype structures.")
    (description
      "This crate provides macros for deriving common traits for newtype structures.")
    (license (list license:expat license:asl2.0))))

(define-public rust-multimap
  (package
    (name "rust-multimap")
    (version "0.8.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "multimap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0cq3hlqwyxz0hmcpbajghhc832ln6h0qszvf89kv8fx875hhfm8j"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-serde" ,rust-serde-1))
        #:cargo-development-inputs
        (("rust-serde-test" ,rust-serde-test-1))))
    (home-page "")
    (synopsis "A multimap implementation.")
    (description
      "This package provides a multimap implementation.")
    (license (list license:expat license:asl2.0))))

(define-public rust-getset
  (package
  (name "rust-getset")
  (version "0.0.9")
  (source
    (origin
      (method url-fetch)
      (uri (crate-uri "getset" version))
      (file-name
        (string-append name "-" version ".tar.gz"))
      (sha256
        (base32
          "0aaldwfs2690rjqg2ygan27l2qa614w2p6zj7k99n36pv2vzbcsv"))))
  (build-system cargo-build-system)
  (arguments
    `(#:cargo-inputs
      (("rust-proc-macro2" ,rust-proc-macro2-1)
       ("rust-quote" ,rust-quote-1)
       ("rust-syn" ,rust-syn-1))))
  (home-page "")
  (synopsis
   "A procedural macro for generating the most basic getters and setters on fields.")
  (description
   "A procedural macro for generating the most basic getters and setters on fields.")
  (license license:expat)))

(define-public rust-quote-1.0.7
  (package
    (name "rust-quote")
    (version "1.0.7")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quote" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0drzd6pq7whq7qhdvvs8wn6pbb0hhc12pz8wv80fb05ixhbksmma"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1))
        #:cargo-development-inputs
        (("rust-rustversion" ,rust-rustversion-1)
         ("rust-trybuild" ,rust-trybuild-1))))
    (home-page "")
    (synopsis "Quasi-quoting macro quote!(...)")
    (description "Quasi-quoting macro quote!(...)")
    (license (list license:expat license:asl2.0))))

(define-public rust-syn-1.0.48
  (package
    (name "rust-syn")
    (version "1.0.48")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1b0rdf7rvgc8am2n36yi8wys7kpdg8ly9891l917yizwxzzildyc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-unicode-xid" ,rust-unicode-xid-0.2))
        #:cargo-development-inputs
        (("rust-anyhow" ,rust-anyhow-1.0)
         ("rust-flate2" ,rust-flate2-1)
         ("rust-insta" ,rust-insta-0.16)
         ("rust-rayon" ,rust-rayon-1)
         ("rust-ref-cast" ,rust-ref-cast-1.0)
         ("rust-regex" ,rust-regex-1)
         ("rust-reqwest" ,rust-reqwest-0.10)
         ("rust-syn-test-suite" ,rust-syn-test-suite-0)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-termcolor" ,rust-termcolor-1)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page "")
    (synopsis "Parser for Rust source code")
    (description "Parser for Rust source code")
    (license (list license:expat license:asl2.0))))


(define-public rust-enum-map-derive-0.4.6
  (package
    (name "rust-enum-map-derive")
    (version "0.4.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "enum-map-derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0mg43p1x90cz604zddk9qzss077v2id04qmmbpa1i7jc637m1i75"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1.0.7)
         ("rust-syn" ,rust-syn-1.0.48))))
    (home-page "")
    (synopsis
      "Macros 1.1 implementation of #[derive(Enum)]")
    (description
      "Macros 1.1 implementation of #[derive(Enum)]")
    (license (list license:expat license:asl2.0))))

(define-public rust-array-macro
  (package
    (name "rust-array-macro")
    (version "1.0.5")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "array-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "19mdx2xlppnqwl6rhsbzylx61a0kkp2ql8q16195b7iga977ps86"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "Array multiple elements constructor syntax")
    (description
      "Array multiple elements constructor syntax")
    (license (list license:expat license:asl2.0))))

(define-public rust-enum-map-0.6.4
  (package
    (name "rust-enum-map")
    (version "0.6.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "enum-map" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0m8qg32fnjdf6z64j4wmyp935p5838wd31gibkiqpbpl76c9k1s1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-array-macro" ,rust-array-macro)
         ("rust-enum-map-derive" ,rust-enum-map-derive-0.4.6)
         ("rust-serde" ,rust-serde-1))
        #:cargo-development-inputs
        (("rust-bincode" ,rust-bincode-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-serde-test" ,rust-serde-test-1))))
    (home-page "")
    (synopsis
      "A map with C-like enum keys represented internally as an array")
    (description
      "This package provides a map with C-like enum keys represented internally as an array")
    (license (list license:expat license:asl2.0))))

(define-public rust-feature-probe
  (package
    (name "rust-feature-probe")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "feature-probe" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1nhif9zpr2f17gagf0qb0v914wc3jr9sfjzvnpi7b7pcs73ksnl3"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "Probe for rustc features from build.rs")
    (description
      "Probe for rustc features from build.rs")
    (license (list license:expat license:asl2.0))))

(define-public rust-feature-probe-0.1
  (package
    (name "rust-feature-probe")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "feature-probe" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1k37j5lzvq155r4xazajwp52w68ccdpp4p834134vbqavar2w619"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "Probe for rustc features from build.rs")
    (description
      "Probe for rustc features from build.rs")
    (license (list license:expat license:asl2.0))))

(define-public rust-bv
  (package
    (name "rust-bv")
    (version "0.11.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bv" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0h5kbl54fsccznfixw83xndbripw39y2qkqjwf709p75iqfvnd48"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-serde" ,rust-serde-1))
        #:cargo-development-inputs
        (("rust-feature-probe" ,rust-feature-probe-0.1)
         ("rust-quickcheck" ,rust-quickcheck-0.6))))
    (home-page "")
    (synopsis "Bit-vectors and bit-slices")
    (description "Bit-vectors and bit-slices")
    (license (list license:expat license:asl2.0))))

(define-public rust-bio-types
  (package
    (name "rust-bio-types")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bio-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "16vrhbmsnphkqn92sinzyyn0wwbr6br26zggcziyrgzqm6m12h4x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-derive-new" ,rust-derive-new-0.5)
         ("rust-lazy-static" ,rust-lazy-static-1.4)
         ("rust-quick-error" ,rust-quick-error-1.2)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1))))
    (home-page "https://rust-bio.github.io")
    (synopsis
      "A collection of common biomedical types for use in rust-bio and rust-htslib.")
    (description
      "This package provides a collection of common biomedical types for use in rust-bio and rust-htslib.")
    (license license:expat)))

(define-public rust-openblas-src
  (package
    (name "rust-openblas-src")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openblas-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0syy38a5bgv5mj6mb1n1zk1d6l5gqqrswvbmwkwx6h4z9wfrsql4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/blas-lapack-rs/openblas-src")
    (synopsis
      "The package provides a source of BLAS and LAPACK via OpenBLAS.")
    (description
      "The package provides a source of BLAS and LAPACK via OpenBLAS.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-netlib-src
  (package
    (name "rust-netlib-src")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "netlib-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04l2ggdaq0bjc64prsw2f8ddxn84m1rmpnkjb9nr0ijdpcv1zx1r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-cmake" ,rust-cmake-0.1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/blas-lapack-rs/netlib-src")
    (synopsis
      "The package provides a source of BLAS and LAPACK via Netlib.")
    (description
      "The package provides a source of BLAS and LAPACK via Netlib.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-quote-0.3.15
  (package
    (name "rust-quote")
    (version "0.3.15")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quote" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0yhnnix4dzsv8y4wwz4csbnqjfh73al33j35msr10py6cl5r4vks"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis "Quasi-quoting macro quote!(...)")
    (description "Quasi-quoting macro quote!(...)")
    (license (list license:expat license:asl2.0))))

(define-public rust-syn-0.11.11
  (package
    (name "rust-syn")
    (version "0.11.11")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syn" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1b8x8jdsmj6r9ck7n1pg371526n1q90kx6rv6ivcb22w06wr3f6k"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-quote" ,rust-quote-0.3)
         ("rust-synom" ,rust-synom-0.11)
         ("rust-unicode-xid" ,rust-unicode-xid-0.0))
        #:cargo-development-inputs
        (("rust-syntex-pos" ,rust-syntex-pos-0.58)
         ("rust-syntex-syntax" ,rust-syntex-syntax-0.58)
         ("rust-tempdir" ,rust-tempdir-0.3)
         ("rust-walkdir" ,rust-walkdir-1))))
    (home-page "")
    (synopsis "Parser for Rust source code")
    (description "Parser for Rust source code")
    (license (list license:expat license:asl2.0))))

(define-public rust-enum-to-u8-slice-derive
  (package
    (name "rust-enum-to-u8-slice-derive")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "enum_to_u8_slice_derive" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1avb25kmhclrhrsk47xnavix97j6js0gvlf9d84m4x3gnhsnxha8"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-quote" ,rust-quote-0.3.15)
         ("rust-syn" ,rust-syn-0.11.11))))
    (home-page "")
    (synopsis
      "A simple fork of enum_to_str_derive (by @DCjanus), convert enum to u8 slice ref")
    (description
      "This package provides a simple fork of enum_to_str_derive (by @DCjanus), convert enum to u8 slice ref")
    (license license:bsd-3)))

(define-public rust-base64-0.10
  (package
    (name "rust-base64")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "base64" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1qmrlwns4ckz75dxi1hap0a9bkljsrn3b5cvzgbqd3q0p3ncf7v2"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-1))
        #:cargo-development-inputs
        (("rust-rand" ,rust-rand-0.7))))
    (home-page "")
    (synopsis
      "encodes and decodes base64 as bytes or utf8")
    (description
      "encodes and decodes base64 as bytes or utf8")
    (license (list license:expat license:asl2.0))))

(define-public rust-mesalink
  (package
    (name "rust-mesalink")
    (version "1.1.0-cratesio")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mesalink" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "02lp27j5vxdc95bf5g983yr660cm6vljikk0yqry4j6cjvfnyq85"))))
    (build-system cargo-build-system)
    (arguments
     `(#:tests? #f
       #:cargo-inputs
        (("rust-base64" ,rust-base64-0.10)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-enum-to-u8-slice-derive" ,rust-enum-to-u8-slice-derive)
         ("rust-env-logger" ,rust-env-logger-0.6)
         ("rust-jemallocator" ,rust-jemallocator-0.3)
         ("rust-lazy-static" ,rust-lazy-static-1.4)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-parking-lot" ,rust-parking-lot-0.9)
         ("rust-ring" ,rust-ring-0.16)
         ("rust-rustls" ,rust-rustls-0.16)
         ("rust-sct" ,rust-sct-0.6)
         ("rust-untrusted" ,rust-untrusted-0.7)
         ("rust-webpki" ,rust-webpki-0.21)
         ("rust-webpki-roots" ,rust-webpki-roots-0.17))
        #:cargo-development-inputs
        (("rust-env-logger" ,rust-env-logger-0.7)
         ("rust-log" ,rust-log-0.4)
         ("rust-walkdir" ,rust-walkdir-2))))
    (home-page
      "https://github.com/mesalock-linux/mesalink")
    (synopsis
      "MesaLink is a memory-safe and OpenSSL-compatible TLS library based on Rustls and Ring.")
    (description
      "MesaLink is a memory-safe and OpenSSL-compatible TLS library based on Rustls and Ring.")
    (license license:bsd-3)))

(define-public rust-curl-sys
  (package
    (name "rust-curl-sys")
    (version "0.4.37+curl-7.72.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "curl-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1kswvmpbbxfm5bpvdgkzd0w4yfwrw0zpk78sszy624pqcn0zsrss"))))
    (build-system cargo-build-system)
    (native-inputs
     `(("zlib" ,zlib)
       ("curl" ,curl)
       ("openssl" ,openssl)
       ("pkg-config" ,pkg-config)))
    (arguments
      `(#:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-libnghttp2-sys" ,rust-libnghttp2-sys-0.1)
         ("rust-libz-sys" ,rust-libz-sys-1)
         ("rust-walkdir" ,rust-walkdir-2)
         ("rust-mesalink" ,rust-mesalink)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-winapi" ,rust-winapi-0.3))
        #:cargo-development-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-vcpkg" ,rust-vcpkg-0.2))))
    (home-page "")
    (synopsis
      "Native bindings to the libcurl library")
    (description
      "Native bindings to the libcurl library")
    (license license:expat)))

(define-public rust-anyhow-1.0.31
  (package
    (name "rust-anyhow")
    (version "1.0.31")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "anyhow" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0pqrpvlaicjpaf5zfqgwq1rg39gfvqhs9sz6a1acm5zc13671fw5"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-futures" ,rust-futures-0.3)
         ("rust-rustversion" ,rust-rustversion-1)
         ("rust-thiserror" ,rust-thiserror-1)
         ("rust-trybuild" ,rust-trybuild-1))))
    (home-page "")
    (synopsis
      "Flexible concrete Error type built on std::error::Error")
    (description
      "Flexible concrete Error type built on std::error::Error")
    (license (list license:expat license:asl2.0))))


(define-public rust-curl
  (package
    (name "rust-curl")
    (version "0.4.34")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "curl" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0vkm6fyizf8m9yxpv3n5pm9ag3bwlyqa6nz2ga8qkzm5y4m1cs72"))))
    (build-system cargo-build-system)
    (propagated-inputs
     `(("zlib" ,zlib)
       ("openssl" ,openssl)
       ("pkg-config" ,pkg-config)
       ("curl" ,curl)))
    (arguments
     `(#:tests? #f
       #:rust ,rust-1.46
       #:cargo-inputs
        (("rust-curl-sys" ,rust-curl-sys)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-openssl-probe" ,rust-openssl-probe-0.1)
         ("rust-openssl-sys" ,rust-openssl-sys-0.9)
         ("rust-schannel" ,rust-schannel-0.1)
         ("rust-socket2" ,rust-socket2-0.3)
         ("rust-winapi" ,rust-winapi-0.3))
        #:cargo-development-inputs
        (("rust-anyhow" ,rust-anyhow-1.0.31)
         ("rust-mio" ,rust-mio-0.6)
         ("rust-mio-extras" ,rust-mio-extras-2))))
    (home-page
      "https://github.com/alexcrichton/curl-rust")
    (synopsis
      "Rust bindings to libcurl for making HTTP requests")
    (description
      "Rust bindings to libcurl for making HTTP requests")
    (license license:expat)))

(define-public rust-intel-mkl-tool
  (package
    (name "rust-intel-mkl-tool")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intel-mkl-tool" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1myyrxvmyij4c60w9x15npwzhlbjm8y8c94lvfsnrl5pbyakz8md"))))
    (build-system cargo-build-system)
    (propagated-inputs
     `(("zlib" ,zlib)
       ("openssl" ,openssl)
       ("pkg-config" ,pkg-config)
       ("curl" ,curl)))
    (arguments
     `(#:rust
       ,rust-1.46
       #:cargo-inputs
        (("rust-curl" ,rust-curl-sys-0.4)
         ("rust-dirs" ,rust-dirs-2.0)
         ("rust-env-logger" ,rust-env-logger-0.7)
         ("rust-failure" ,rust-failure-0.1)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-log" ,rust-log-0.4)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-structopt" ,rust-structopt-0.3)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-zstd" ,rust-zstd-0.5))))
    (home-page "")
    (synopsis
      "CLI utility for redistributiing Intel(R) MKL")
    (description
      "CLI utility for redistributiing Intel(R) MKL")
    (license license:expat)))


(define-public rust-intel-mkl-src
  (package
    (name "rust-intel-mkl-src")
    (version "0.5.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "intel-mkl-src" version))
       (file-name
        (string-append name "-" version ".tar.gz"))
       (sha256
        (base32
         "155q49a7nfbq1lllchsyx8jv2q2pijrjh1w08awvrbjyfcxb6q3j"))))
    (build-system cargo-build-system)
    (inputs
     `(("zlib" ,zlib)
       ("openssl" ,openssl)
       ("pkg-config" ,pkg-config)
       ("curl" ,curl)))
    (arguments
     `(#:rust
       ,rust-1.46
       #:skip-build? #t
       #:cargo-development-inputs
       (("rust-intel-mkl-tool" ,rust-intel-mkl-tool)
        ("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis
     "Redistribution of Intel(R) MKL as a crate")
    (description
     "Redistribution of Intel(R) MKL as a crate")
    (license #f)))

(define-public rust-accelerate-src
  (package
    (name "rust-accelerate-src")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "accelerate-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17fiqyq7f9k41pbsyrvk9pxyx9z6fw399wq036cvwkbmb14xcpj1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/blas-lapack-rs/accelerate-src")
    (synopsis
      "The package provides a source of BLAS and LAPACK via the Accelerate framework.")
    (description
      "The package provides a source of BLAS and LAPACK via the Accelerate framework.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-openblas-src
  (package
    (name "rust-openblas-src")
    (version "0.9.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "openblas-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0syy38a5bgv5mj6mb1n1zk1d6l5gqqrswvbmwkwx6h4z9wfrsql4"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/blas-lapack-rs/openblas-src")
    (synopsis
      "The package provides a source of BLAS and LAPACK via OpenBLAS.")
    (description
      "The package provides a source of BLAS and LAPACK via OpenBLAS.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-netlib-src
  (package
    (name "rust-netlib-src")
    (version "0.8.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "netlib-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "04l2ggdaq0bjc64prsw2f8ddxn84m1rmpnkjb9nr0ijdpcv1zx1r"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-cmake" ,rust-cmake-0.1)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/blas-lapack-rs/netlib-src")
    (synopsis
      "The package provides a source of BLAS and LAPACK via Netlib.")
    (description
      "The package provides a source of BLAS and LAPACK via Netlib.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-intel-mkl-tool-0.2
  (package
    (name "rust-intel-mkl-tool")
    (version "0.2.0+mkl2020.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intel-mkl-tool" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1w4ffl6q8inr7xzwda660z2c8j4iz8spqcfqscg46mpwxbk3676x"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-anyhow" ,rust-anyhow-1.0)
         ("rust-curl" ,rust-curl)
         ("rust-derive-more" ,rust-derive-more-0.99)
         ("rust-dirs" ,rust-dirs-2.0)
         ("rust-glob" ,rust-glob-0.3)
         ("rust-pkg-config" ,rust-pkg-config-0.3)
         ("rust-structopt" ,rust-structopt-0.3)
         ("rust-tar" ,rust-tar-0.4)
         ("rust-zstd" ,rust-zstd-0.5))
        #:cargo-development-inputs
        (("rust-paste" ,rust-paste-0.1))))
    (home-page "")
    (synopsis
      "CLI utility for redistributiing Intel(R) MKL")
    (description
      "CLI utility for redistributiing Intel(R) MKL")
    (license license:expat)))

(define-public rust-intel-mkl-src-0.2
  (package
    (name "rust-intel-mkl-src")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "intel-mkl-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1dwy21pk99hnb9yw6hxjpbm0r79jk1cnvqlzw38w2avqh69bw12q"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "Redistribution of Intel(R) MKL as a crate")
    (description
      "Redistribution of Intel(R) MKL as a crate")
    (license #f)))

(define-public rust-accelerate-src-0.3.2
  (package
    (name "rust-accelerate-src")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "accelerate-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "17fiqyq7f9k41pbsyrvk9pxyx9z6fw399wq036cvwkbmb14xcpj1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/blas-lapack-rs/accelerate-src")
    (synopsis
      "The package provides a source of BLAS and LAPACK via the Accelerate framework.")
    (description
      "The package provides a source of BLAS and LAPACK via the Accelerate framework.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-blas-src-0.2
  (package
    (name "rust-blas-src")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "blas-src" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1jamnjdd9vkp756qrflij7b6ppyl15c4ml54qhxx1ffy36g7ms56"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-accelerate-src" ,rust-accelerate-src)
         ("rust-intel-mkl-src" ,rust-intel-mkl-src-0.2)
         ("rust-netlib-src" ,rust-netlib-src)
         ("rust-openblas-src" ,rust-openblas-src))))
    (home-page
      "https://github.com/blas-lapack-rs/blas-src")
    (synopsis
      "The package provides a BLAS source of choice.")
    (description
      "The package provides a BLAS source of choice.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-ndarray-0.13
  (package
    (name "rust-ndarray")
    (version "0.13.0")
    (source
     (origin
       (method url-fetch)
       (uri (crate-uri "ndarray" version))
       (file-name
        (string-append name "-" version ".tar.gz"))
       (sha256
        (base32
         "0yy5mi5ljmsyqa4g1xs8k0fgdcg6dhwxhyxpbwv6kwjx5zy03c15"))))
    (build-system cargo-build-system)
    (arguments
     `(#:cargo-inputs
       (("rust-approx" ,rust-approx-0.3)
        ("rust-blas-src" ,rust-blas-src-0.2)
        ("rust-cblas-sys" ,rust-cblas-sys-0.1)
        ("rust-itertools" ,rust-itertools-0.8)
        ("rust-matrixmultiply" ,rust-matrixmultiply-0.2)
        ("rust-num-complex" ,rust-num-complex-0.3)
        ("rust-num-integer" ,rust-num-integer-0.1)
        ("rust-num-traits" ,rust-num-traits-0.2)
        ("rust-rawpointer" ,rust-rawpointer-0.2)
        ("rust-rayon" ,rust-rayon-1)
        ("rust-serde" ,rust-serde-1))
       #:cargo-development-inputs
       (("rust-approx" ,rust-approx-0.3)
        ("rust-defmac" ,rust-defmac-0.2)
        ("rust-itertools" ,rust-itertools-0.8)
        ("rust-quickcheck" ,rust-quickcheck-0.8))))
    (home-page "")
    (synopsis
     "An n-dimensional array for general elements and for numerics. Lightweight array views and slicing; views support chunking and splitting.")
    (description
     "An n-dimensional array for general elements and for numerics.  Lightweight array views and slicing; views support chunking and splitting.")
    (license (list license:expat license:asl2.0))))

(define-public rust-statrs-0.12.0
  (package
    (name "rust-statrs")
    (version "0.12.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "statrs" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "10hk9194ig21w08947yavf4l97g0106ph4xxlzn8ps2kwrnnzqfc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs (("rust-rand" ,rust-rand-0.7))))
    (home-page "https://github.com/boxtown/statrs")
    (synopsis
      "Statistical computing library for Rust")
    (description
      "Statistical computing library for Rust")
    (license license:expat)))

(define-public rust-strum-macros-0.18.0
  (package
    (name "rust-strum-macros")
    (version "0.18.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0k3pwbv0c8q00jnsjshzfc2d5r3y6ppgf9fz7pyknrgaz2immj47"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-heck" ,rust-heck-0.3)
         ("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1))))
    (home-page
      "https://github.com/Peternator7/strum")
    (synopsis
      "Helpful macros for working with enums and strings")
    (description
      "Helpful macros for working with enums and strings")
    (license license:expat)))

(define-public rust-strum-0.18.0
  (package
    (name "rust-strum")
    (version "0.18.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "strum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0asjskn1qhqqfiq673np0gvmnd1rsp506m38vk53gi7l93mq3gap"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-strum-macros" ,rust-strum-macros-0.18.0))
        #:cargo-development-inputs
        (("rust-strum-macros" ,rust-strum-macros-0.18.0))))
    (home-page
      "https://github.com/Peternator7/strum")
    (synopsis
      "Helpful macros for working with enums and strings")
    (description
      "Helpful macros for working with enums and strings")
    (license license:expat)))


(define-public rust-x86test-types
  (package
    (name "rust-x86test-types")
    (version "0.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "x86test-types" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mzf90i6pcwx2jr77c2l5690q9kz9n480ykv4m8hckxs4zasf1s9"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs (("rust-x86" ,rust-x86))))
    (home-page "")
    (synopsis
      "Common types for x86test runnter and the x86test procedural macro.
")
    (description
      "Common types for x86test runnter and the x86test procedural macro.
")
    (license (list license:expat license:asl2.0))))

(define-public rust-x86test-macro
  (package
    (name "rust-x86test-macro")
    (version "0.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "x86test-macro" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "188flp8g46787fl9y0rk1c6xa33pdiw2iyfs37xk11hj5nmh6l57"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-proc-macro2" ,rust-proc-macro2-1)
         ("rust-quote" ,rust-quote-1)
         ("rust-syn" ,rust-syn-1)
         ("rust-x86test-types" ,rust-x86test-types))))
    (home-page "")
    (synopsis
      "Procedural macro plugin for x86test.
")
    (description
      "Procedural macro plugin for x86test.
")
    (license (list license:expat license:asl2.0))))

(define-public rust-mmap
  (package
    (name "rust-mmap")
    (version "0.1.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "mmap" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08xqhvr4l3rf1fkz2w4cwz3z5wd0m1jab1d34sxd4v80lr459j0b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-libc" ,rust-libc-0.2)
         ("rust-tempdir" ,rust-tempdir-0.3))))
    (home-page
      "https://github.com/rbranson/rust-mmap")
    (synopsis
      "A library for dealing with memory-mapped I/O
")
    (description
      "This package provides a library for dealing with memory-mapped I/O
")
    (license license:expat)))

(define-public rust-kvm-sys
  (package
    (name "rust-kvm-sys")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kvm-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0pc8jr6ymfhc88f3zy4cz2q49km1fr2hn2cf6w2ncnpdxdsprcff"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-clippy" ,rust-clippy-0.0)
         ("rust-errno" ,rust-errno-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-log" ,rust-log-0.4)
         ("rust-memmap" ,rust-memmap-0.7))
        #:cargo-development-inputs
        (("rust-gcc" ,rust-gcc-0.3))))
    (home-page "https://github.com/gz/kvm")
    (synopsis "Bindings for KVM interface")
    (description "Bindings for KVM interface")
    (license license:asl2.0)))

(define-public rust-x86test
  (package
    (name "rust-x86test")
    (version "0.0.3")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "x86test" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00y3mc3sah5r197658dm8ndknwlhycbxjly7klskm29li8zn3jns"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-klogger" ,rust-klogger)
         ("rust-kvm-sys" ,rust-kvm-sys)
         ("rust-log" ,rust-log-0.4)
         ("rust-mmap" ,rust-mmap)
         ("rust-x86" ,rust-x86)
         ("rust-x86test-macro" ,rust-x86test-macro)
         ("rust-x86test-types" ,rust-x86test-types))))
    (home-page "")
    (synopsis
      "Custom test runner for bare-metal x86 tests.
")
    (description
      "Custom test runner for bare-metal x86 tests.
")
    (license (list license:expat license:asl2.0))))

(define-public rust-termcodes
  (package
    (name "rust-termcodes")
    (version "0.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "termcodes" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "11am1kadmh3iym8qglxj91avc8z6ihcfajlfry38xm5l1f46myir"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "A library for styled output in ANSI terminals.")
    (description
      "This package provides a library for styled output in ANSI terminals.")
    (license license:expat)))

(define-public rust-klogger
  (package
    (name "rust-klogger")
    (version "0.0.6")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "klogger" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0khxh743lfmvbd7p1l3d4h4g39m97njpm0b9qjgkpgiya29v9d4d"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-heapless" ,rust-heapless-0.5)
         ("rust-log" ,rust-log-0.4)
         ("rust-spin" ,rust-spin-0.5)
         ("rust-termcodes" ,rust-termcodes)
         ("rust-x86" ,rust-x86))))
    (home-page "")
    (synopsis "Library for logging in kernel mode.")
    (description
      "Library for logging in kernel mode.")
    (license license:expat)))

(define-public rust-core-affinity
  (package
    (name "rust-core-affinity")
    (version "0.5.10")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "core_affinity" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "07qpwyxps4gp3gci2p6c5h4cmcql7551bp91qgbv0ky3bh8h72kz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-kernel32-sys" ,rust-kernel32-sys-0.2)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-num-cpus" ,rust-num-cpus-1)
         ("rust-winapi" ,rust-winapi-0.3))))
    (home-page
      "https://github.com/Elzair/core_affinity_rs")
    (synopsis "Manages CPU affinities")
    (description "Manages CPU affinities")
    (license (list license:expat license:asl2.0))))

(define-public rust-raw-cpuid
  (package
    (name "rust-raw-cpuid")
    (version "8.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "raw-cpuid" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0wry932lx7gqyxn7w54mg61b7hiwywyir754jhfxiws3pnfpvpqz"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bitflags" ,rust-bitflags-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1))
        #:cargo-development-inputs
        (("rust-cc" ,rust-cc-1)
         ("rust-core-affinity" ,rust-core-affinity)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-rustc-version" ,rust-rustc-version-0.2)
         ("rust-rustversion" ,rust-rustversion-1))))
    (home-page "https://github.com/gz/rust-cpuid")
    (synopsis
      "A library to parse the x86 CPUID instruction, written in rust with no external dependencies. The implementation closely resembles the Intel CPUID manual description. The library does only depend on libcore.")
    (description
      "This package provides a library to parse the x86 CPUID instruction, written in rust with no external dependencies.  The implementation closely resembles the Intel CPUID manual description.  The library does only depend on libcore.")
    (license license:expat)))

(define-public rust-bit-field
  (package
    (name "rust-bit-field")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bit_field" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "192rsg8g3ki85gj8rzslblnwr53yw5q4l8vfg6bf1lkn4cfdvdnw"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "Simple bit field trait providing get_bit, get_bits, set_bit, and set_bits methods for Rust's integral types.")
    (description
      "Simple bit field trait providing get_bit, get_bits, set_bit, and set_bits methods for Rust's integral types.")
    (license (list license:asl2.0 license:expat))))

(define-public rust-x86
  (package
    (name "rust-x86")
    (version "0.34.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "x86" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "06f8j4ahzx76kyjxrgbg1il3994gmakmkhbqffc7dq3ifk2cnin1"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bit-field" ,rust-bit-field)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-phf" ,rust-phf-0.8)
         ("rust-raw-cpuid" ,rust-raw-cpuid))
        #:cargo-development-inputs
        (("rust-csv" ,rust-csv-1.1)
         ("rust-klogger" ,rust-klogger)
         ("rust-libc" ,rust-libc-0.2)
         ("rust-phf-codegen" ,rust-phf-codegen-0.8)
         ("rust-serde-json" ,rust-serde-json-1)
         ("rust-x86test" ,rust-x86test))))
    (home-page "https://github.com/gz/rust-x86")
    (synopsis
      "Library to program x86 (amd64) hardware. Contains x86 specific data structure descriptions, data-tables, as well as convenience function to call assembly instructions typically not exposed in higher level languages.")
    (description
      "Library to program x86 (amd64) hardware.  Contains x86 specific data structure descriptions, data-tables, as well as convenience function to call assembly instructions typically not exposed in higher level languages.")
    (license license:expat)))

(define-public rust-rusty-fork-0.3.0
  (package
    (name "rust-rusty-fork")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "rusty-fork" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0kxwq5c480gg6q0j3bg4zzyfh2kwmc3v2ba94jw8ncjc8mpcqgfb"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-fnv" ,rust-fnv-1)
         ("rust-quick-error" ,rust-quick-error-1.2)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-wait-timeout" ,rust-wait-timeout-0.2))))
    (home-page "")
    (synopsis
      "Cross-platform library for running Rust tests in sub-processes using a
fork-like interface.
")
    (description
      "Cross-platform library for running Rust tests in sub-processes using a
fork-like interface.
")
    (license (list license:expat license:asl2.0))))

(define-public rust-proptest-0.10.0
  (package
    (name "rust-proptest")
    (version "0.10.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "proptest" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1hd0wkghxaqlcqzaqj4jipg8lxw3q63yj06jw9hklsngfdizw815"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-bit-set" ,rust-bit-set-0.5)
         ("rust-bitflags" ,rust-bitflags-1)
         ("rust-byteorder" ,rust-byteorder-1)
         ("rust-lazy-static" ,rust-lazy-static-1.4)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-quick-error" ,rust-quick-error-1.2)
         ("rust-rand" ,rust-rand-0.7)
         ("rust-rand-chacha" ,rust-rand-chacha-0.2)
         ("rust-rand-xorshift" ,rust-rand-xorshift-0.2)
         ("rust-regex-syntax" ,rust-regex-syntax-0.6)
         ("rust-rusty-fork" ,rust-rusty-fork-0.3.0)
         ("rust-tempfile" ,rust-tempfile-3)
         ("rust-x86" ,rust-x86))
        #:cargo-development-inputs
        (("rust-regex" ,rust-regex-1))))
    (home-page
      "https://altsysrq.github.io/proptest-book/proptest/index.html")
    (synopsis
      "Hypothesis-like property-based testing and shrinking.
")
    (description
      "Hypothesis-like property-based testing and shrinking.
")
    (license (list license:expat license:asl2.0))))


(define-public rust-bio-0.32
  (package
    (name "rust-bio")
    (version "0.32.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wj6s3hzpx5xhajcnvdabbgpalgghdni7gmlhjl6i9pfh1xiq5pi"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-approx" ,rust-approx-0.3)
         ("rust-bio-types" ,rust-bio-types)
         ("rust-bit-set" ,rust-bit-set-0.5)
         ("rust-feature-probe" ,rust-feature-probe-0.1)
         ("rust-bv" ,rust-bv)
         ("rust-bytecount" ,rust-bytecount-0.6)
         ("rust-csv" ,rust-csv-1.1)
         ("rust-custom-derive" ,rust-custom-derive-0.1)
         ("rust-enum-map" ,rust-enum-map-0.6.4)
         ("rust-fnv" ,rust-fnv-1)
         ("rust-fxhash" ,rust-fxhash-0.2)
         ("rust-getset" ,rust-getset)
         ("rust-itertools" ,rust-itertools-0.9)
         ("rust-itertools-num" ,rust-itertools-num-0.1)
         ("rust-lazy-static" ,rust-lazy-static-1.4)
         ("rust-multimap" ,rust-multimap)
         ("rust-ndarray" ,rust-ndarray-0.13)
         ("rust-newtype-derive" ,rust-newtype-derive-0.1.6)
         ("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-num-traits" ,rust-num-traits-0.2)
         ("rust-ordered-float" ,rust-ordered-float-1.0)
         ("rust-petgraph" ,rust-petgraph-0.5)
         ("rust-quick-error" ,rust-quick-error-1.2)
         ("rust-regex" ,rust-regex-1)
         ("rust-serde" ,rust-serde-1)
         ("rust-serde-derive" ,rust-serde-derive-1)
         ("rust-snafu" ,rust-snafu)
         ("rust-statrs" ,rust-statrs-0.12.0)
         ("rust-strum" ,rust-strum-0.18.0)
         ("rust-strum-macros" ,rust-strum-macros-0.18.0)
         ("rust-triple-accel" ,rust-triple-accel)
         ("rust-vec-map" ,rust-vec-map-0.8))
        #:cargo-development-inputs
        (("rust-proptest" ,rust-proptest-0.10.0)
         ("rust-tempfile" ,rust-tempfile-3))))
    (home-page "https://rust-bio.github.io")
    (synopsis
      "A bioinformatics library for Rust. This library provides implementations of many algorithms and data structures that are useful for bioinformatics, but also in other fields.")
    (description
      "This package provides a bioinformatics library for Rust.  This library provides implementations of many algorithms and data structures that are useful for bioinformatics, but also in other fields.")
    (license license:expat)))

(define-public rust-cue-sys-1.0.0
  (package
    (name "rust-cue-sys")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cue-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1s77lkl5xm82sw32shy2nrs2nlyj6k9qgy68yj8yjp6f623nif5v"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/mistydemeo/libcue.rs")
    (synopsis "Bindings for the libcue library")
    (description "Bindings for the libcue library")
    (license license:gpl2)))

(define-public rust-cue-1.0.0
  (package
    (name "rust-cue")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0ig64ygrd7zy3zckh9qgfrm0p0d1wp2ni7ybr77v628k6nk7mjix"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-cue-sys" ,rust-cue-sys-1.0.0)
         ("rust-libc" ,rust-libc-0.2))))
    (home-page
      "https://github.com/mistydemeo/libcue.rs")
    (synopsis
      "High-level bindings for the libcue library")
    (description
      "High-level bindings for the libcue library")
    (license license:gpl2)))

(define-public rust-bidir-map-0.3.2
  (package
    (name "rust-bidir-map")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bidir-map" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12izwm2j85bvh3cx0s5qq1bilnjx9qmjn04x2cgw4lslpy1h334k"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "Bidirectional map implementation for Rust")
    (description
      "Bidirectional map implementation for Rust")
    (license license:expat)))

(define-public rust-bidir-map-1.0.0
  (package
    (name "rust-bidir-map")
    (version "1.0.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bidir-map" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "00xlk9gws0d45yb014lzlf7hd40v41hpr9cb8pc07s059v7hbgvm"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "Bidirectional map implementation for Rust")
    (description
      "Bidirectional map implementation for Rust")
    (license license:expat)))

(define-public rust-log-0.3.2
  (package
    (name "rust-log")
    (version "0.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "log" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08l5l5lbnigqiaa6c8y4cg000jz3phhxxzxmlc8yl2zvjc154c8b"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs (("rust-libc" ,rust-libc-0.2))))
    (home-page "")
    (synopsis
      "A lightweight logging facade for Rust
")
    (description
      "This package provides a lightweight logging facade for Rust
")
    (license (list license:expat license:asl2.0))))


(define-public rust-advapi32-sys-0.1.2
  (package
    (name "rust-advapi32-sys")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "advapi32-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1mvbqq7v9ibbn2hxizhyf4qq9ghbi6fyw8h21ir8drb750rr4z1h"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.2))
        #:cargo-development-inputs
        (("rust-winapi-build" ,rust-winapi-build-0.1))))
    (home-page "")
    (synopsis
      "Contains function definitions for the Windows API library advapi32. See winapi for types and constants.")
    (description
      "Contains function definitions for the Windows API library advapi32.  See winapi for types and constants.")
    (license license:expat)))

(define-public rust-libc-0.1.12
  (package
    (name "rust-libc")
    (version "0.1.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "libc" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "08k14zb7bw25avmaj227calcdglb4ac394kklr9nv175fp7p0ap3"))))
    (build-system cargo-build-system)
    (home-page "https://github.com/rust-lang/libc")
    (synopsis
      "Raw FFI bindings to platform libraries like libc.
")
    (description
      "Raw FFI bindings to platform libraries like libc.
")
    (license (list license:expat license:asl2.0))))

(define-public rust-kernel32-sys-0.1.2
  (package
    (name "rust-kernel32-sys")
    (version "0.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "kernel32-sys" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0w8sw5cijh9wcanpirqcvdvlmy3dcwn2ig7f447ilcz3isph6rdk"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-winapi" ,rust-winapi-0.3)
         ("rust-winapi-build" ,rust-winapi-build-0.1))))
    (home-page "")
    (synopsis
      "Contains function definitions for the Windows API library kernel32. See winapi for types and constants.")
    (description
      "Contains function definitions for the Windows API library kernel32.  See winapi for types and constants.")
    (license license:expat)))


(define-public rust-time-0.1.32
  (package
    (name "rust-time")
    (version "0.1.32")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "time" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1wqj5r41w8kqzaawaklzl8k8hyn8f6vir4i8qdpw109v4ci3habs"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-kernel32-sys" ,rust-kernel32-sys-0.1.2)
         ("rust-libc" ,rust-libc-0.1.12)
         ("rust-rustc-serialize" ,rust-rustc-serialize-0.3)
         ("rust-winapi" ,rust-winapi-0.2))
        #:cargo-development-inputs
        (("rust-advapi32-sys" ,rust-advapi32-sys-0.1.2)
         ("rust-log" ,rust-log-0.3))))
    (home-page "")
    (synopsis
      "Date and time library. Fully interoperable with the standard library. Mostly compatible with #![no_std].")
    (description
      "Date and time library.  Fully interoperable with the standard library.  Mostly compatible with #![no_std].")
    (license (list license:expat license:asl2.0))))


(define-public rust-syncbox-0.2.4
  (package
    (name "rust-syncbox")
    (version "0.2.4")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "syncbox" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1iw57d91zj0jmgg6gxs19iy1jy35b746chbw1qnpmhlscmr2pg05"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-log" ,rust-log-0.3.2)
         ("rust-time" ,rust-time-0.1.32))
        #:cargo-development-inputs
        (("rust-env-logger" ,rust-env-logger-0.3))))
    (home-page
      "https://github.com/carllerche/syncbox")
    (synopsis "Concurrency utilities for Rust")
    (description "Concurrency utilities for Rust")
    (license license:expat)))

(define-public rust-crossbeam-0.2
  (package
    (name "rust-crossbeam")
    (version "0.2.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "crossbeam" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1gmrayc93lygb8v62bj0c5zwyflvj5bli7ari650k259nlyncrmx"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-rand" ,rust-rand-0.3))))
    (home-page
      "https://github.com/crossbeam-rs/crossbeam")
    (synopsis "Tools for concurrent programming")
    (description "Tools for concurrent programming")
    (license (list license:asl2.0 license:expat))))


(define-public rust-cue-0.1.0
  (package
    (name "rust-cue")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "cue" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "12glymb3p9kh5jrrd0h39cmabzrjrzi03gmikg0pl0j2xd3lbp81"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-crossbeam" ,rust-crossbeam-0.2)
         ("rust-log" ,rust-log-0.3)
         ("rust-syncbox" ,rust-syncbox-0.2.4))))
    (home-page
      "https://github.com/mistydemeo/libcue.rs")
    (synopsis
      "High-level bindings for the libcue library")
    (description
      "High-level bindings for the libcue library")
    (license license:expat)))

;;; RUST BIO 0.13 -------------------------------------------------------------

(define-public rust-csv-0.15
  (package
    (name "rust-csv")
    (version "0.15.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "csv" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "173dv65zmmm4kfqnjk66vhgj4w82vh9c14jq6r55c755qwvjpwky"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-byteorder" ,rust-byteorder-0.5)
         ("rust-memchr" ,rust-memchr-1.0)
         ("rust-rustc-serialize" ,rust-rustc-serialize-0.3))
        #:cargo-development-inputs
        (("rust-regex" ,rust-regex-0.1))))
    (home-page
      "https://github.com/BurntSushi/rust-csv")
    (synopsis
      "Fast CSV parsing with support for serde.")
    (description
      "Fast CSV parsing with support for serde.")
    (license (list license:unlicense license:expat))))

(define-public rust-quickcheck-macros-0.2
  (package
    (name "rust-quickcheck-macros")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quickcheck-macros" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "1v6xbxywvy8cam3a0c8p65fgc0ylfbbdix9y90hrqfasgn0bf498"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-quickcheck" ,rust-quickcheck-0.2))))
    (home-page
      "https://github.com/BurntSushi/quickcheck")
    (synopsis "A macro attribute for quickcheck.")
    (description
      "This package provides a macro attribute for quickcheck.")
    (license (list license:unlicense license:expat))))

(define-public rust-algebra-0.2
  (package
    (name "rust-algebra")
    (version "0.2.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "algebra" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0p7mrdj9z47xflfssvf1wmj2vrgarwzpd5svidq40ik45i0j7nrc"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-development-inputs
        (("rust-quickcheck" ,rust-quickcheck-0.2)
         ("rust-quickcheck-macros" ,rust-quickcheck-macros-0.2))))
    (home-page "")
    (synopsis "Abstract algebra for Rust")
    (description "Abstract algebra for Rust")
    (license license:asl2.0)))

(define-public rust-generic-array-0.2
  (package
    (name "rust-generic-array")
    (version "0.2.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "generic-array" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0rvkla5pr6icjyj4yw64nvs13zx0jqrdar3rbgcgsi69bfbs61il"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs (("rust-typenum" ,rust-typenum-1))))
    (home-page "")
    (synopsis
      "Generic types implementing functionality of arrays")
    (description
      "Generic types implementing functionality of arrays")
    (license license:expat)))


(define-public rust-typenum-1.3
  (package
    (name "rust-typenum")
    (version "1.3.2")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "typenum" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0pkvm39kpgasf5fq7v337bsywgkxsdipv9wrar3gpvbv0pcidwr0"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "Typenum is a Rust library for type-level numbers evaluated at compile time. It currently supports bits, unsigned integers, and signed integers. It also provides a type-level array of type-level numbers, but its implementation is incomplete.")
    (description
      "Typenum is a Rust library for type-level numbers evaluated at compile time.  It currently supports bits, unsigned integers, and signed integers.  It also provides a type-level array of type-level numbers, but its implementation is incomplete.")
    (license (list license:expat license:asl2.0))))

(define-public rust-nalgebra-0.10
  (package
    (name "rust-nalgebra")
    (version "0.10.1")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "nalgebra" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0paxvav03jkp4az675incvsfrg6ygpgq5qf2hn98kxd5s55zcm7p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-algebra" ,rust-algebra-0.2)
         ("rust-generic-array" ,rust-generic-array-0.2)
         ("rust-num" ,rust-num-0.1)
         ("rust-quickcheck" ,rust-quickcheck-0.2)
         ("rust-rand" ,rust-rand-0.3)
         ("rust-rustc-serialize" ,rust-rustc-serialize-0.3)
         ("rust-typenum" ,rust-typenum-1.3))))
    (home-page "https://nalgebra.org")
    (synopsis
      "Linear algebra library with transformations and statically-sized or dynamically-sized matrices.")
    (description
      "Linear algebra library with transformations and statically-sized or dynamically-sized matrices.")
    (license license:bsd-3)))

(define-public rust-stainless-0.1
  (package
    (name "rust-stainless")
    (version "0.1.12")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "stainless" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0y2cgd9labb46d064b1mhhc0z84mk0r2cgs3zsn2cvy8xpwgqvwh"))))
    (build-system cargo-build-system)
    (home-page "")
    (synopsis
      "Organized, flexible testing framework.")
    (description
      "Organized, flexible testing framework.")
    (license license:expat)))

(define-public rust-unreachable-0.1
  (package
    (name "rust-unreachable")
    (version "0.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "unreachable" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0xrp8gi0q3af11abvdcyldyibg09gxkyic7s1b46dsbzph6ajv5s"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs (("rust-void" ,rust-void-1))))
    (home-page "")
    (synopsis
      "An unreachable code optimization hint in stable rust.")
    (description
      "An unreachable code optimization hint in stable rust.")
    (license (list license:expat license:asl2.0))))

(define-public rust-ordered-float-0.3
  (package
    (name "rust-ordered-float")
    (version "0.3.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "ordered-float" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0l5mkxf5vsv58bd932gghjv37zgvki1xai4p0kjggzgnczgfmf1p"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-num-traits" ,rust-num-traits-0.1)
         ("rust-rustc-serialize" ,rust-rustc-serialize-0.3)
         ("rust-serde" ,rust-serde-0.8)
         ("rust-unreachable" ,rust-unreachable-0.1))
        #:cargo-development-inputs
        (("rust-stainless" ,rust-stainless-0.1))))
    (home-page "")
    (synopsis
      "Wrappers for total ordering on floats")
    (description
      "Wrappers for total ordering on floats")
    (license license:expat)))



(define-public rust-quick-error-1.1
  (package
    (name "rust-quick-error")
    (version "1.1.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "quick-error" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "0757jd5lbcpi7kkhjh8j0vj2k26f4swg9wdx5ni7vdkzilz61b8a"))))
    (build-system cargo-build-system)
    (home-page
      "http://github.com/tailhook/quick-error")
    (synopsis
      "    A macro which makes error types pleasant to write.
")
    (description
      "    A macro which makes error types pleasant to write.
")
    (license (list license:expat license:asl2.0))))


(define-public rust-vec-map-0.7
  (package
    (name "rust-vec-map")
    (version "0.7.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "vec_map" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "15qq6q6kb5ic1gb1cbvnn535c4ppczkb4zrmfbc8w6fh7fwwikgq"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-serde" ,rust-serde-0.9)
         ("rust-serde-derive" ,rust-serde-derive-0.9))))
    (home-page
      "https://github.com/contain-rs/vec-map")
    (synopsis
      "A simple map based on a vector for small integer keys")
    (description
      "This package provides a simple map based on a vector for small integer keys")
    (license (list license:expat license:asl2.0))))

(define-public rust-bio-0.13
  (package
    (name "rust-bio")
    (version "0.13.0")
    (source
      (origin
        (method url-fetch)
        (uri (crate-uri "bio" version))
        (file-name
          (string-append name "-" version ".tar.gz"))
        (sha256
          (base32
            "163v9ivdwzbj53hdmc4sz0kss1s4824a127jrggvfv1db1i02jq7"))))
    (build-system cargo-build-system)
    (arguments
      `(#:cargo-inputs
        (("rust-approx" ,rust-approx-0.1)
         ("rust-bit-set" ,rust-bit-set-0.4)
         ("rust-bit-vec" ,rust-bit-vec-0.4)
         ("rust-csv" ,rust-csv-0.15)
         ("rust-custom-derive" ,rust-custom-derive-0.1)
         ("rust-itertools" ,rust-itertools-0.5)
         ("rust-itertools-num" ,rust-itertools-num-0.1)
         ("rust-lazy-static" ,rust-lazy-static-0.2)
         ("rust-nalgebra" ,rust-nalgebra-0.10)
         ("rust-newtype-derive" ,rust-newtype-derive-0.1.6)
         ("rust-num-integer" ,rust-num-integer-0.1)
         ("rust-num-traits" ,rust-num-traits-0.1)
         ("rust-ordered-float" ,rust-ordered-float-0.3)
         ("rust-quick-error" ,rust-quick-error-1.1)
         ("rust-regex" ,rust-regex-0.2)
         ("rust-rustc-serialize" ,rust-rustc-serialize-0.3)
         ("rust-serde" ,rust-serde-0.9)
         ("rust-serde-derive" ,rust-serde-derive-0.9)
         ("rust-vec-map" ,rust-vec-map-0.7))))
    (home-page "https://rust-bio.github.io")
    (synopsis
      "A bioinformatics library for Rust. This library provides implementations of many algorithms and data structures that are useful for bioinformatics, but also in other fields.")
    (description
      "This package provides a bioinformatics library for Rust.  This library provides implementations of many algorithms and data structures that are useful for bioinformatics, but also in other fields.")
    (license license:expat)))
