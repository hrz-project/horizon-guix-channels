
(define-module (horizon packages assembly)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages time)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages java)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages parallel)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages tbb)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages vim)
  #:use-module ((guix build utils) #:hide (delete))
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (guix build-system python)
  #:use-module (guix build-system r)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  ;;
  #:use-module (horizon packages development)
  #:use-module (horizon packages r-packages)
  #:use-module (horizon packages python)
  #:use-module (horizon packages alignment))

(define-public python-dgenies
  (package
    (name "python-dgenies")
    (version "1.2.0.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "dgenies" version))
        (sha256
          (base32
            "15vkrl1d1izr39q9k3i8p56qgmmij94a2jacgvr4fxsw3br11i97"))))
    (build-system python-build-system)
    (inputs
     `(("minimap2" ,minimap2)
       ("mashmap" ,mashmap)))
    (propagated-inputs
      `(("python-biopython" ,python-biopython)
        ("python-crontab" ,python-crontab)
        ("python-drmaa" ,python-drmaa)
        ("python-flask" ,python-flask)
        ("python-flask-mail" ,python-flask-mail)
        ("python-intervaltree" ,python-intervaltree)
        ("python-jinja2" ,python-jinja2)
        ("python-markdown" ,python-markdown)
        ("python-matplotlib" ,python-matplotlib)
        ("python-numpy" ,python-numpy)
        ("python-peewee" ,python-peewee)
        ("python-psutil" ,python-psutil)
        ("python-pyyaml" ,python-pyyaml)
        ("python-requests" ,python-requests)
        ("python-tendo" ,python-tendo)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fix-config-path
           (lambda* (#:key outputs #:allow-other-keys)
             (substitute* '("setup.py")
               (("\\/etc\\/dgenies") (string-append (assoc-ref outputs "out") "/shared/etc"))
               (("\\/var\\/www\\/dgenies") (string-append (assoc-ref outputs "out") "/shared/var/www")))))
         (add-before 'build 'replace-embedded-bin
           (lambda* (#:key inputs #:allow-other-keys)
             (let* ((inp (lambda (x) (assoc-ref inputs x)))
                    (get-bin (lambda (x) (string-append (inp x) "/bin/" x))))
               (for-each
                (lambda (f)
                  (let ((tgt (string-append "src/dgenies/bin/" f)))
                    (delete-file tgt)
                    (symlink (get-bin f) tgt)))
                '("minimap2" "mashmap"))
               #t)))
         (add-before 'install 'create-etc-path
           (lambda* (#:key outputs #:allow-other-keys)
             (mkdir-p (string-append (assoc-ref outputs "out") "/shared/var/www"))
             (mkdir-p (string-append (assoc-ref outputs "out") "/shared/etc"))
             #t))
         (delete 'check))))
    (home-page "http://dgenies.toulouse.inra.fr")
    (synopsis
      "Dotplot large Genomes in an Interactive, Efficient and Simple way")
    (description
      "Dotplot large Genomes in an Interactive, Efficient and Simple way")
    (license #f)))

;; (define-public sga
;;   (package
;;     (name "sga")
;;     (version "0.10.15")
;;     ()))
