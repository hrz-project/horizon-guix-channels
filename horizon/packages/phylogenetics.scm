(define-module (horizon packages phylogenetics)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages multiprecision)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public libpll
  (let ((commit "8b545392688495e58c15fee0dba30c8caf204cef")
        (revision "0"))
    (package
      (name "libpll")
      (version (git-version "2" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/xflouris/libpll-2")
               (commit commit)))
         (sha256
          (base32 "1p81zwx8yajb3fxzgkbfmsccj1zipn7sijwdyckg8gz01mr0z3na"))))
      (build-system gnu-build-system)
      (native-inputs
       `(("autoreconf" ,autoconf)
         ("aclocal" ,automake)
         ("libtool" ,libtool)))
      (inputs
       `(("bison" ,bison)
         ("flex" ,flex)
         ("gcc" ,gcc-9)))
      (home-page "https://github.com/xflouris/libpll-2")
      (synopsis "Phylogenetic Likelihood Library 2 - experimental")
      (description "The aim of this project is to implement a versatile
high-performance software library for phylogenetic analysis. The library
should serve as a lower-level interface of PLL (Flouri et al. 2015) and should
have the following properties:

@itemize
@item open source code with an appropriate open source license.
@item 64-bit multi-threaded design that handles very large datasets.
@item easy to use and well-documented.
@item SIMD implementations of time-consuming parts.
@item as fast or faster likelihood computations than RAxML (Stamatakis 2014).
@item fast implementation of the site repeats algorithm (Kobert 2017).
@item functions for tree visualization.
@item bindings for Python.
@item generic and clean design.
@item Linux, Mac, and Microsoft Windows compatibility.
@end itemize")
      (license license:agpl3+))))

(define-public pll-modules
  (let ((commit "3c0a3fbc90aed82c235872ffd2a4d21b65ee7a88")
        (revision "0"))
    (package
      (name "pll-modules")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/ddarriba/pll-modules")
               (commit commit)))
         (sha256
          (base32 "1136n2z48ca00iaij17nfywxcg5ajp53qg4nqka60wlgs48aikma"))))
      (build-system gnu-build-system)
      (native-inputs
       `(("autoreconf" ,autoconf)
         ("aclocal" ,automake)
         ("libtool" ,libtool)))
      (inputs
       `(("bison" ,bison)
         ("flex" ,flex)
         ("gcc" ,gcc-9)
         ("libpll" ,libpll)))
      (arguments
       `(#:configure-flags
         (list
          (string-append "CPPFLAGS=-I" (assoc-ref %build-inputs "libpll") "/include/libpll")
          (string-append "LDFLAGS=-L" (assoc-ref %build-inputs "libpll") "/lib"))))
      (home-page "https://github.com/ddarriba/pll-modules")
      (synopsis "High Level modules for the Low Level Phylogenetic Likelihood Library")
      (description "High Level modules for the Low Level Phylogenetic Likelihood Library")
      (license license:agpl3+))))

(define-public terraphast-one
  (let ((commit "8af2e4c838c82329adf14fa8df2c341b757378af")
        (revision "0"))
    (package
      (name "terraphast-one")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/amkozlov/terraphast-one")
               (commit commit)))
         (sha256
          (base32 "1lcabm9mnvda6qz1ixn7y4qr3bjawlj7cnkjsfgf141dqlgvhfxq"))))
      (build-system cmake-build-system)
      (inputs
       `(("gcc" ,gcc-9)
         ("gmp" ,gmp)))
      (arguments
       `(#:tests?
         #f
         #:phases
         (modify-phases %standard-phases
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (lib (lambda (x) (string-append out "/lib/" x)))
                      (inc (lambda (x) (string-append out "/include/" x))))
                 (mkdir-p (lib "")) (mkdir-p (inc ""))
                 (copy-recursively "../source/include/" (inc ""))
                 (for-each
                  (lambda (f) (copy-file f (lib f)))
                  '("libterraces.a" "libterraces_c.a"))
                 #t))))))
      (home-page "https://github.com/amkozlov/terraphast-one")
      (synopsis "An implementation of the SUPERB algorithm for analysis of phylogenetic terraces")
      (description "An implementation of the SUPERB algorithm for analysis of phylogenetic terraces")
      (license license:gpl3))))

(define-public raxml-ng
  (let ((commit "3a5518f203ebd9548b59fbca299c2c7462dec917")
        (revision "0"))
    (package
      (name "raxml-ng")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/amkozlov/raxml-ng")
               (commit commit)))
         (sha256
          (base32 "0wcnhym574xrm4lxqfcqy91nc6q3g80w3bx93qwsqnq644v61yhd"))))
      (build-system cmake-build-system)
      (inputs
       `(("terraphast-src" ,(package-source terraphast-one))
         ("pll-modules-src" ,(package-source pll-modules))
         ("libpll-src" ,(package-source libpll))
         ("bison" ,bison)
         ("flex" ,flex)
         ("gmp" ,gmp)
         ("gcc" ,gcc-9)))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'add-terraphast-pll-modules
             (lambda* (#:key inputs #:allow-other-keys)
               (let* ((terraphast (assoc-ref inputs "terraphast-src"))
                      (pll-modules (assoc-ref inputs "pll-modules-src"))
                      (libpll (assoc-ref inputs "libpll-src")))
                 (copy-recursively terraphast "libs/terraphast")
                 (copy-recursively pll-modules "libs/pll-modules")
                 (copy-recursively libpll "libs/pll-modules/libs/libpll")))))))
      (home-page "https://github.com/amkozlov/raxml-ng")
      (synopsis "RAxML-NG is a phylogenetic tree inference tool which uses maximum-likelihood (ML) optimality criterion.")
      (description "RAxML-NG is a phylogenetic tree inference tool which uses
maximum-likelihood (ML) optimality criterion. Its search heuristic is based on
iteratively performing a series of Subtree Pruning and Regrafting (SPR) moves,
which allows to quickly navigate to the best-known ML tree. RAxML-NG is
a successor of RAxML (Stamatakis 2014) and leverages the highly optimized
likelihood computation implemented in libpll (Flouri et al. 2014).")
      (license license:agpl3+))))
