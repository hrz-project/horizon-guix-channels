(define-public (horizon packages go)
  ;; packages
  #:use-module (gnu packages golang)
  ;; build system
  #:use-module (guix build-system go)
  ;;
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((guix build utils) #:hide (delete which))
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1))


(define-public go-github-com-biogo-store
  (package
    (name "go-github-com-biogo-store")
    (version "0.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/biogo/store")
             (commit "2c6ad937eb839d6385b0e14497e08450718e7a75")))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "12ksq9wqp919pfkwfqnijr97g7n83mwx40jsnka0rhvhl2ggddqk"))))
    (build-system go-build-system)
    (native-inputs
     `(("go-github-com-kr-pretty" ,go-github-com-kr-pretty)
       ("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)))
    (arguments
     `(#:tests? #f
                #:unpack-path
                "github.com/biogo/store"
                #:import-path
                "github.com/biogo/store"
                #:phases
                (modify-phases %standard-phases
                  (delete 'build))))
    (synopsis "Store provides a number of data store types that are useful for bioinformatic analysis.")
    (description "Store provides a number of data store types that are
useful for bioinformatic analysis.
@itemize
@item Left-Leaning Red-Black tree
@item Interval tree
@item k-d tree
@item Run-length encoding data store
@end itemize")
    (home-page "https://github.com/biogo/store")
    (license license:bsd-3)))

(define (go-github-com-biogo-store-package suffix)
  (package (inherit go-github-com-biogo-store)
           (name (string-append "go-github-com-biogo-store-" suffix))
           (version "0.0.0")
           (arguments
            `(#:unpack-path
              "github.com/biogo/store"
              #:import-path
              ,(string-append "github.com/biogo/store/" suffix)))))

(define-public go-github-com-biogo-store-kdtree
  (package (inherit (go-github-com-biogo-store-package "kdtree"))))

(define-public go-github-com-biogo-store-llrb
  (package (inherit (go-github-com-biogo-store-package "llrb"))))

(define-public go-github-com-biogo-store-step
  (package (inherit (go-github-com-biogo-store-package "step"))))

(define-public go-github-com-biogo-store-interval
  (package (inherit (go-github-com-biogo-store-package "interval"))))

(define-public go-golang-org-x-exp
  (package
    (name "go-golang-org-x-exp")
    (version "0.0.0")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/golang/exp")
                    (commit "94841d0725da6d75db0041374260f8a11035d4c8")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0f6c0xh3kzsd8g1li5fwjn3lywl836d3iy5kj3qk2x8fdshqm4jr"))))
    (build-system go-build-system)
    (arguments
     `(#:tests? #f
                #:import-path
                "golang.org/x/exp"
                #:phases
                (modify-phases %standard-phases
                  (delete 'build))))
    (home-page "https://github.com/golang/exp")
    (synopsis "Experimental golang packages")
    (description "This subrepository holds experimental and
deprecated (in the old directory) packages.")
    (license #f)))                      ; unknown to me



(define-public go-github-com-ulikunitz-xz
  (package
    (name "go-github-com-ulikunitz-xz")
    (version "0.5.6")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/ulikunitz/xz")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1qpk02c0nfgfyg110nmbaiy5x12fpn0pm8gy7h1s8pwns133n831"))))
    (build-system go-build-system)
    (arguments
     `(#:tests? #f
                #:import-path
                "github.com/ulikunitz/xz"
                #:phases
                (modify-phases %standard-phases
                  (delete 'build))))
    (home-page "https://github.com/ulikunitz/xz")
    (synopsis "Reading and writing of xz compressed streams")
    (description "This Go language package supports the reading and
writing of xz compressed streams. It includes also a gxz command for
compressing and decompressing data. The package is completely written
in Go and doesn't have any dependency on any C code.")
    (license license:bsd-3)))

(define-public go-github-com-biogo-hts
  (package
    (name "go-github-com-biogo-hts")
    (version "1.1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/biogo/hts")
             (commit "346de99200e929580bd3ebf3293fd721dd943da6")))
       (file-name (git-file-name name version))
       (sha256 (base32 "0a08fb8xvvbimga95awa69wfip41px29kvh6imy43810plga954i"))))
    (build-system go-build-system)
    (native-inputs
     `(;; ("go-github-com-biogo-boom" ,go-github-com-biogo-boom)
       ("go-github-com-kortschak-utter" ,go-github-com-kortschak-utter)
       ("go-github-com-ulikunitz-xz" ,go-github-com-ulikunitz-xz)
       ("go-golang-org-x-exp" ,go-golang-org-x-exp)
       ("go-github-com-kr-pretty" ,go-github-com-kr-pretty)
       ("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)))
    (arguments
     `(#:tests? #f
                #:unpack-path
                "github.com/biogo/hts"
                #:import-path
                "github.com/biogo/hts"
                #:phases
                (modify-phases %standard-phases
                  (delete 'build))))
    (synopsis "SAM and BAM handling for the Go language.")
    (description "bíogo/hts provides a Go native implementation of
the SAM specification for SAM and BAM alignment formats commonly used
for representation of high throughput genomic data, the BAI, CSI and
tabix indexing formats, and the BGZF blocked compression format. The
bíogo/hts packages perform parallelized read and write operations and
are able to cache recent reads according to user-specified caching
methods. The bíogo/hts APIs have been constructed to provide
a consistent interface to sequence alignment data and the underlying
compression system in order to aid ease of use and tool development.")
    (home-page "https://github.com/biogo/hts")
    (license license:bsd-3)))

(define-public go-github-com-biogo-graph
  (package
    (name "go-github-com-biogo-graph")
    (version "0.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/biogo/graph")
             (commit "057c1989faed86b200716b2605ccac233db73275")))
       (file-name (git-file-name name version))
       (sha256 (base32 "1kpzs5dfd5dsk4mg1g2qjz1prqd84ixhrcxxnf90hq25vxcnk7lh"))))
    (build-system go-build-system)
    (arguments
     `(#:tests? #f
                #:unpack-path
                "github.com/biogo/graph"
                #:import-path
                "github.com/biogo/graph"
                #:phases
                (modify-phases %standard-phases
                  (delete 'build))))
    (home-page "https://github.com/biogo/graph")
    (synopsis "bíogo undirected graph analysis repository")
    (description "bíogo undirected graph analysis repository")
    (license license:bsd-3)))

(define-public go-github-com-biogo-biogo
  (package
    (name
     (string-append "go-github-com-biogo-biogo"))
    (version "1.0.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/biogo/biogo")
             (commit (string-append "v" version))))
       (sha256
        (base32 "01y0pqlv2207w70v1jmivrf761inapbygzs7cagia8l7r9lanlr0"))))
    (build-system go-build-system)
    (arguments
     `(#:import-path
       ,(string-append "github.com/biogo/biogo" )
       #:unpack-path
       "github.com/biogo/biogo"
       #:phases
       (modify-phases %standard-phases
         (add-before 'reset-gzip-timestamps 'make-files-writable
           (lambda* (#:key outputs #:allow-other-keys)
             ;; Make sure .gz files are writable so that the
             ;; 'reset-gzip-timestamps' phase can do its work.
             (let ((out (assoc-ref outputs "out")))
               (for-each make-file-writable
                         (find-files out "\\.gz$"))
               #t)))
         (delete 'build))))
    (native-inputs
     `(("go-github-com-biogo-store" ,go-github-com-biogo-store)
       ("go-github-com-biogo-hts" ,go-github-com-biogo-hts)
       ("go-github-com-biogo-graph" ,go-github-com-biogo-graph)
       ("go-gopkg-in-check-v1" ,go-gopkg-in-check-v1)
       ("go-github-com-kr-text" ,go-github-com-kr-text)
       ("go-github-com-kr-pretty" ,go-github-com-kr-pretty)
       ("go-github-com-creack-pty" ,go-github-com-creack-pty)
       ("go-github-com-kortschak-utter" ,go-github-com-kortschak-utter)))
    (synopsis "@code{biogo} part")
    (description "This package is a part of @code{biogo}")
    (home-page "https://github.com/biogo/biogo")
    (license license:bsd-3)))

;; (define-public go-github-com-bsipost-
;;   ())
