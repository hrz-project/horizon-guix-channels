(define-module (horizon packages python)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages check)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages llvm)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-web)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages time)
  #:use-module (gnu packages xml)
  ;;
  #:use-module (guix build-system python)
  ;;
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public python-argparse
  (package
    (name "python-argparse")
    (version "1.4.0")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "argparse" version))
        (sha256
          (base32
            "1r6nznp64j68ih1k537wms7h57nvppq0szmwsaf99n71bfjqkc32"))))
    (build-system python-build-system)
    (arguments `(#:tests? #f))
    (home-page
      "https://github.com/ThomasWaldmann/argparse/")
    (synopsis "Python command-line parsing library")
    (description
      "Python command-line parsing library")
    (license #f)))

(define-public python-binpacking
  (package
    (name "python-binpacking")
    (version "1.4.3")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "binpacking" version))
        (sha256
          (base32
            "1y5102hwd6rs2lriryasri0s2k0gy372aw45y029sf456zpj53q4"))))
    (build-system python-build-system)
    (native-inputs
     `(("python-pytest" ,python-pytest)
       ("python-pytest-cov" ,python-pytest-cov)
       ("python-pytest-runner" ,python-pytest-runner)))
    (propagated-inputs
      `(("python-future" ,python-future)
        ("python-numpy" ,python-numpy)))
    (arguments `(#:tests? #f))
    (home-page
      "https://www.github.com/benmaier/binpacking")
    (synopsis
      "Heuristic distribution of weighted items to bins (either a fixed number
of bins or a fixed number of volume per bin). Data may be in form of list,
dictionary, list of tuples or csv-file.")
    (description
      "Heuristic distribution of weighted items to bins (either a fixed number
of bins or a fixed number of volume per bin). Data may be in form of list,
dictionary, list of tuples or csv-file.")
    (license license:expat)))

(define-public python-colored
  (package
    (name "python-colored")
    (version "1.4.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "colored" version))
        (sha256
          (base32
            "1vam15kzsafqh0lhqxr3hkwp9hmqs5z8j63ndqlk96z3v44sqvq5"))))
    (build-system python-build-system)
    (arguments `(#:tests? #f))
    (home-page "https://gitlab.com/dslackw/colored")
    (synopsis
      "Simple library for color and formatting to terminal")
    (description
      "Simple library for color and formatting to terminal")
    (license #f)))

(define-public python-crontab
  (package
    (name "python-crontab")
    (version "2.5.1")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "python-crontab" version))
        (sha256
          (base32
            "0cccrqc10r8781ba81x8r2frs3pl2m4hkm599k5358ak0xr7xgjb"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-dateutil" ,python-dateutil)))
    (home-page
     "https://gitlab.com/doctormo/python-crontab/")
    (arguments `(#:tests? #f))
    (synopsis "Python Crontab API")
    (description "Python Crontab API")
    (license #f)))

(define-public python-ete3
  (package
    (name "python-ete3")
    (version "3.1.2")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "ete3" version))
        (sha256
          (base32
            "1sariq57v77adbg8h9vj8aymrjsm2hgi1cgs11k9v219qnw8gjag"))))
    (build-system python-build-system)
    (inputs
     `(("python-six" ,python-six)
       ("python-numpy" ,python-numpy)
       ("python-pyqt" ,python-pyqt)
       ("python-lxml" ,python-lxml)))
    (arguments `(#:tests? #f))
    (home-page "http://etetoolkit.org")
    (synopsis
      "A Python Environment for (phylogenetic) Tree Exploration")
    (description
      "A Python Environment for (phylogenetic) Tree Exploration")
    (license #f)))

(define-public python-flask-mail
  (package
    (name "python-flask-mail")
    (version "0.9.1")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Flask-Mail" version))
       (sha256
        (base32
         "0hazjc351s3gfbhk975j8k65cg4gf31yq404yfy0gx0bjjdfpr92"))))
    (build-system python-build-system)
    (arguments `(#:tests? #f))
    (propagated-inputs
     `(("python-blinker" ,python-blinker)
       ("python-flask" ,python-flask)))
    (home-page
     "https://github.com/rduplain/flask-mail")
    (synopsis "Flask extension for sending email")
    (description "Flask extension for sending email")
    (license license:bsd-3)))

(define-public python-illumina-utils
  (package
    (name "python-illumina-utils")
    (version "2.8")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "illumina-utils" version))
        (sha256
          (base32
            "0lxzy5jc6j7v8a2hcrq69ffs1zg3is7zzm3d6d13yxivi35m9f90"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-levenshtein" ,python-levenshtein)
        ("python-matplotlib" ,python-matplotlib)
        ("python-numpy" ,python-numpy)))
    (home-page
      "https://github.com/meren/illumina-utils")
    (synopsis "A library and collection of scripts to work with Illumina
paired-end data (for CASAVA 1.8+).")
    (description "A library and collection of scripts to work with Illumina
paired-end data (for CASAVA 1.8+).")
    (license #f)))

(define-public python-llvmlite-0.34
  (package
    (inherit python-llvmlite)
    (version "0.34.0")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "llvmlite" version))
       (sha256
        (base32
         "0qqzs6h34002ig2jn31vk08q9hh5kn84lhmv4bljz3yakg8y0gph"))))
    (inputs
     `(("llvm" ,llvm-9)
       ("lld" ,lld)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'fix-fpic-flag
           (lambda _
             (setenv "CXX" "g++ -fPIC"))))))))

(define-public  python-numba-0.51.2
  (package
    (inherit python-numba)
    ;; (name "python-numba")
    (version "0.51.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "numba" version))
       (sha256
        (base32
         "0s0777m8kq4l96i88zj78np7283v1n4878qfc1gvzb8l45bmkg8n"))))
    (build-system python-build-system)
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'disable-proprietary-features
           (lambda _
             (setenv "NUMBA_DISABLE_HSA" "1")
             (setenv "NUMBA_DISABLE_CUDA" "1")
             #t))
         (delete 'check))))
    (inputs
     `(("gcc" ,gcc-9)))
    (propagated-inputs
     `(("python-llvmlite" ,python-llvmlite-0.34)
       ("python-numpy" ,python-numpy)
       ("python-setuptools" ,python-setuptools)))
    (home-page "https://numba.github.com")
    (synopsis "compiling Python code using LLVM")
    (description "compiling Python code using LLVM")
    (license license:bsd-3)))

(define-public python-pyani
  (package
    (name "python-pyani")
    (version "0.2.10")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "pyani" version))
        (sha256
          (base32 "0n9bnbjk32wsr3d7zk0l1aclpfpg1pyzqv6917rknp8ja8f44khm"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-biopython" ,python-biopython)
        ("python-matplotlib" ,python-matplotlib)
        ("python-pandas" ,python-pandas)
        ("python-scipy" ,python-scipy)
        ("python-seaborn" ,python-seaborn)))
    (home-page "http://widdowquinn.github.io/pyani/")
    (synopsis
      "pyani provides a package and script for calculation of genome-scale average nucleotide identity.")
    (description
      "pyani provides a package and script for calculation of genome-scale average nucleotide identity.")
    (license license:expat)))

(define-public python-pylca
  (package
    (name "python-pylca")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/pirovc/pylca")
             (commit (string-append "pylca_v" version))))
       (sha256 (base32 "0v52h8mzaxs7gly2v4h0x3g9yynxcn6qh5ka8xii3mlzfsm17cnn"))))
    (build-system python-build-system)
    (home-page "https://github.com/pirovc/pylca")
    (synopsis "Lowest common ancestor (LCA) algorithm implementation in
Python")
    (description "Adaptation of the the lowest common ancestor (LCA) algorithm
originally developed by D. Eppstein (https://www.ics.uci.edu/~eppstein/)")
    (license license:expat)))

(define-public python-taxsbp
  (package
    (name "python-taxsbp")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/pirovc/taxsbp")
             (commit version)))
       (sha256 (base32 "0dxx1wy4684vb9p683sn1zcndn9r7q0b6ghvjvvc6n9irwx2zvah"))))
    (build-system python-build-system)
    (native-inputs
     `(("python-unittest" ,python-unittest2)
       ("python-discover" ,python-discover)
       ("python-pytest" ,python-pytest)
       ("python-pytest-runner" ,python-pytest-runner)))
    (propagated-inputs
     `(("python" ,python-wrapper)
       ("python-binpacking" ,python-binpacking)
       ("python-pylca" ,python-pylca)
       ("python-pandas" ,python-pandas)))
    ;; (arguments
    ;;  `(#:phases
    ;;    (modify-phases %standard-phases
    ;;      (delete 'build))))
    (home-page "https://github.com/pirovc/taxsbp")
    (synopsis "TaxSBP - Taxonomic structured bin packing implementation")
    (description "Implementation of the approximation algorithm for the hierarchically structured bin packing problem [1] adapted for the NCBI Taxonomy database [2] (uses LCA script from [3]).
---
[1] Codenotti, B., De Marco, G., Leoncini, M., Montangero, M., & Santini, M. (2004). Approximation algorithms for a hierarchically structured bin packing problem. Information Processing Letters, 89(5), 215–221. http://doi.org/10.1016/j.ipl.2003.12.001
[2] Federhen, S. (2012). The NCBI Taxonomy database. Nucleic Acids Research, 40(D1), D136–D143. http://doi.org/10.1093/nar/gkr1178
[3] https://www.ics.uci.edu/~eppstein/ in the package https://github.com/pirovc/pylca")
    (license license:expat)))

(define-public python-tendo
  (package
    (name "python-tendo")
    (version "0.2.15")
    (source
      (origin
        (method url-fetch)
        (uri (pypi-uri "tendo" version))
        (sha256
          (base32
            "1i5yxa9xjrqvxbihx3xaxwpjk8ci8xy1vwlpgnb2h8wf3badfncs"))))
    (build-system python-build-system)
    (propagated-inputs
      `(("python-pbr" ,python-pbr)
        ("python-pip" ,python-pip)
        ("python-setuptools" ,python-setuptools)
        ("python-six" ,python-six)))
    (home-page "https://github.com/pycontribs/tendo")
    (arguments `(#:tests? #f))
    (synopsis
      "A Python library that extends some core functionality")
    (description
      "A Python library that extends some core functionality")
    (license license:bsd-3)))
