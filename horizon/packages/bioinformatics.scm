;; GNU Guix for Horizon

;;; Copyright © 2020 Samuel Barreto <samuel.barreto8@gmail.com>

;;; MODULES ------------------------------------------------------------------
(define-module (horizon packages bioinformatics)
  ;;
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages cmake)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages cran)
  #:use-module (gnu packages crates-io)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages datastructures)
  #:use-module (gnu packages gawk)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages golang)
  #:use-module (gnu packages java)
  #:use-module (gnu packages jemalloc)
  #:use-module (gnu packages libevent)
  #:use-module (gnu packages libunwind)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages logging)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages networking)
  #:use-module (gnu packages parallel)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages popt)
  #:use-module (gnu packages pretty-print)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages serialization)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages statistics)
  #:use-module (gnu packages tbb)
  #:use-module (gnu packages time)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages vim)
  ;;
  #:use-module (guix build-system cargo)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system go)
  #:use-module (guix build-system python)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system r)
  ;;
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((guix build utils) #:hide (delete which))
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (srfi srfi-1)
  ;;
  #:use-module (horizon packages rust)
  #:use-module (horizon packages development)
  #:use-module (horizon packages bioperl)
  #:use-module (horizon packages r-packages))


;;; UTILITIES ----------------------------------------------------------------

;; FIXME WIP: quast full requires external tools such as blast and
;; all. [2020-09-23 17:12] not functionnal yet; I mostly need the
;; simpler quast version, without all external tools; staying private
;; for now.
(define-public (replace-thirdparty inputs source target)
  (let ((from (assoc-ref inputs source)))
    (delete-file-recursively target)
    (copy-recursively from target)))

(define horizon-perl-deps
  `(("perl-file-path", perl-file-path)
    ("perl-file-temp" ,perl-file-temp)
    ("perl-getopt-long" ,perl-getopt-long)
    ("perl-time-hires" ,perl-time-hires)))

;;; PACKAGES -----------------------------------------------------------------

(define-public barrnap
  (package
    (name "barrnap")
    (version "0.9")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/tseemann/barrnap")
             (commit version )))
       (sha256
        (base32
         "0ahalqgb5n6ygykhl5r4x8c488w4gzj6162vh8dg9p8a1zdci623"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(("bin/barrnap" "bin/barrnap")
         ("db"  "db"))))
    (propagated-inputs
     `(("hmmer" ,hmmer)
       ("bedtools" ,bedtools)))
    (inputs
     `(("perl" ,perl)))
    (home-page "https://github.com/tseemann/barrnap")
    (synopsis "Bacterial ribosomal RNA predictor")
    (description "Barrnap predicts the location of ribosomal RNA genes in genomes. It supports bacteria (5S,23S,16S), archaea (5S,5.8S,23S,16S), metazoan mitochondria (12S,16S) and eukaryotes (5S,5.8S,28S,18S).

It takes FASTA DNA sequence as input, and write GFF3 as output. It uses the new nhmmer tool that comes with HMMER 3.1 for HMM searching in RNA:DNA style. Multithreading is supported and one can expect roughly linear speed-ups with more CPUs.")
    (license license:gpl3)))


(define-public bcalm
  (package
    (name "bcalm")
    (version "2.2.3")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/GATB/bcalm")
             (commit (string-append "v" version))))
       (sha256
        (base32
         "10xwp2xry05dzr5pv69n8vri5yrhak7abfjn92x2040j83bn98la"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("perl" ,perl)                   ;for generating H5pubgen.h
       ("gatb-core-src" ,(package-source gatb-core))))
    (inputs
     `(("zlib" ,zlib)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'unpack-gatb-sources
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((gatb-src (assoc-ref inputs "gatb-core-src")))
               (delete-file-recursively "gatb-core")
               (copy-recursively gatb-src "./gatb-core")))))))
    (home-page "https://github.com/GATB/bcalm")
    (synopsis "BCALM 2 is a bioinformatics tool for constructing the
compacted de Bruijn graph from sequencing data.")
    (description "BCALM 2 outputs the set of unitigs of the de Bruijn
graph. A unitig is the sequence of a non-branching path. Unitigs that
are connected by an edge in the graph overlap by exactly (k-1)
nucleotides.")
    (license license:expat)))

(define-public bcool
  (package
    (name "bcool")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Malfoy/BCOOL")
             (commit version)))
       (sha256
        (base32 "0l2k7mq0jxp9aivy7wrr7am39qql6vh8481di0k65j0sw1jvx8z7"))))
    (build-system copy-build-system)
    (inputs
     `(("python" ,python-3)
       ("bcalm" ,bcalm)
       ("bgreat" ,bgreat)
       ("btrim" ,btrim)
       ("ntcard" ,ntcard)))
    (arguments
     `(#:install-plan
       '(("Bcool" "bin/Bcool"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'patch-path-to-bins
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((bin (lambda (x) (string-append "\"" (assoc-ref inputs x) "/bin/" x))))
               (substitute* '("Bcool")
                 (("\\/home\\/malfoy\\/dev\\/BCOOL\\/bin") "directories handled by GNU Guix magic ...")
                 (("BCOOL_INSTDIR \\+ \"\\/bcalm") (bin "bcalm"))
                 ;; remove memory limit; bcalm will detect it
                 (("-max-memory 10000") "")
                 (("BCOOL_INSTDIR \\+ \"\\/btrim") (bin "btrim"))
                 (("BCOOL_INSTDIR \\+ \"\\/ntcard") (bin "ntcard"))
                 (("BCOOL_INSTDIR \\+ \"\\/bgreat") (bin "bgreat"))))
             #t)))))
    (home-page "https://github.com/Malfoy/BCOOL")
    (synopsis "de Bruijn graph cOrrectiOn from graph aLignment")
    (description "De Bruijn Graph correction from graph alignment")
    (license license:agpl3)))

(define-public bifrost
  (package
    (name "bifrost")
    (version "1.0.5")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/pmelsted/bifrost")
             (commit (string-append "v" version))))
       (sha256
        (base32 "1rx30zhqfyqm951vm1a8xzfrbp2pgdwz9yyx6051ry8cfp0j0vkj"))))
    (build-system cmake-build-system)
    (inputs
     `(("zlib" ,zlib)
       ("gcc" ,gcc-9)))
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/pmelsted/bifrost")
    (synopsis "Bifrost: Highly parallel construction and indexing of
colored and compacted de Bruijn graphs")
    (description "Parallel construction, indexing and querying of
colored and compacted de Bruijn graphs

@itemize
@item Build, index, color and query the compacted de Bruijn graph
@item No need to build the uncompacted de Bruijn graph
@item Reads or assembled genomes as input
@item Output graph in GFA (can be visualized with Bandage)
@item Graph cleaning: short tip clipping, etc.
@item No disk usage (adapted for cluster architectures)
@item Multi-threaded and SIMD optimized
@item No parameters to estimate with other tools
@item Inexact k-mer search of queries
@item C++ API available:
@itemize
@item Associate your data with vertices
@item Add or remove (sub-)sequences / k-mers / colors
@item Find unitigs containing queried k-mers
@end itemize
@end itemize
")
    (license license:bsd-2)))


(define-public bgreat
  (package
    (name "bgreat")
    (version "1.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/Malfoy/BGREAT2/archive/" version ".tar.gz"))
       (sha256 (base32 "1k3pp5p7fsy0p96442vk0dgl2y980q4pazgk6ic74nr0ldayfq5w"))))
    (build-system gnu-build-system)
    (inputs
     `(("gcc" ,gcc-9)
       ("zlib" ,zlib)))
    (arguments
     `(#:make-flags
       (list
        (string-append "CC=" (assoc-ref %build-inputs "gcc") "/bin/g++"))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'bootstrap)
         (delete 'check)
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (inst (lambda (x) (install-file x bin))))
               (for-each inst '("bgreat" "numbersToSequences" "sortPaths"))
               #t))))))
    (home-page "https://github.com/Malfoy/BGREAT2")
    (synopsis "Paired reads mapping on de Bruijn graph")
    (description "Bgreat2 is intended to map reads or read pairs on de Bruijn
graphs in an efficient manner. The de Bruijn graph should be represented as
a set of unitigs. We advise the use of Bcalm in order to do so. A mapping can
be represented by a path in the graph (list of nodes) or by the actual
sequences of the graph. This last behavior have been shown able to correct
a set a of reads (eg BCOOL).")
    (license license:agpl3)))


(define-public biogrok
  (let ((commit "cc68a8ddc9ea0ab9839fa0dfec86f267b67cd504")
        (revision "0"))
    (package
      (name "biogrok")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/noporpoise/biogrok")
               (commit commit)))
         (sha256
          (base32 "100nzkg3g903wnrjdsqfv529d8309mzkicvg5x90930xql0bjg9h"))))
      (build-system copy-build-system)
      (inputs
       `(("perl" ,perl)
         ("bash" ,bash)
         ("awk" ,gawk)
         ("grep" ,grep)
         ("samtools" ,samtools)))
      (arguments
       `(#:install-plan
         '(("./" "bin"))))
      (home-page "https://github.com/noporpoise/biogrok")
      (synopsis "A collection of scripts for simple bioinformatic tasks")
      (description "A collection of scripts for simple bioinformatic tasks
Most tools use only standard linux tools bash, grep, awk and perl.
Some tools require samtools.")
      (license license:expat))))

(define-public bioinf-perl
  (let ((commit "badab487fa21867191a51bff5a17c87a140d9fff")
        (revision "0"))
    (package
      (name "bioinf-perl")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/noporpoise/bioinf-perl/")
               (commit commit)))
         (sha256
          (base32 "1cd70p1waj8r7vmin8l79ymaprbjcsvkajw28v0577alq3pyyg9f"))))
      (inputs
       `(("perl" ,perl)))
      (build-system copy-build-system)
      (arguments
       `(#:install-plan
         '(("lib" "lib")
           ("blast_scripts/" "bin/")
           ("cortex_scripts/" "bin/")
           ("de_bruijn/" "bin/")
           ("fastn_scripts/" "bin/")
           ("genetics/" "bin/")
           ("sam_scripts/" "bin/")
           ("sim_mutations/" "bin/")
           ("utilities/" "bin/")
           ("vcf_scripts/" "bin/"))))
      (home-page "https://github.com/noporpoise/bioinf-perl")
      (synopsis "Perl libraries and scripts for everyday bioinformatics tasks")
      (description "Perl libraries and scripts for everyday
bioinformatics tasks, including VCF, FASTA & FASTQ handling")
      (license #f))))

(define-public blastfrost
  (let ((commit "2c2470309baa8d1b946a0a9fa04fc59f6a3f1660")
        (revision "0"))
    (package
      (name "blastfrost")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/nluhmann/BlastFrost/")
               (commit commit)))
         (sha256
          (base32 "0y6prdsy52j0ycdznahzawi0dwmyf9pzlk35r08f065g5v97h84k"))))
      (build-system cmake-build-system)
      (native-inputs
       `(("zlib" ,zlib)))
      (inputs
       `(("bifrost" ,(package-source bifrost))
         ("gcc" ,gcc-9)))
      (arguments
       `(#:tests?
         #f
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'copy-bifrost-src
             (lambda* (#:key inputs #:allow-other-keys)
               (let ((bifrost (assoc-ref inputs "bifrost")))
                 (copy-recursively bifrost "bifrost"))
               #t))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let ((bin (string-append (assoc-ref outputs "out") "/bin")))
                 (mkdir-p bin)
                 (copy-file "BlastFrost" (string-append bin "/BlastFrost"))
                 #t))))))
      (home-page "https://github.com/nluhmann/BlastFrost/")
      (synopsis "BlastFrost is a highly efficient method for querying
100,000s of genome assemblies.")
      (description "BlastFrost is a highly efficient method for querying
100,000s of genome assemblies. BlastFrost builds on the recently
developed Bifrost, which generates a dynamic data structure for
compacted and colored de Bruijn graphs from bacterial genomes.
BlastFrost queries a Bifrost data structure for sequences of interest,
and extracts local subgraphs, thereby enabling the efficient
identification of the presence or absence of individual genes or
single nucleotide sequence variants.")
      (license license:gpl3))))

(define-public blight
  (package
    (name "blight")
    (version "0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/Malfoy/Blight")
             (commit "f38c20b8ed792fd4ed5279577c2949fadc3d1499")))
       (sha256
        (base32
         "1hw1gdcwf3603s8zk1qfn65c0mdf7zal0nfzc30ciasbmka08x41"))))
    (build-system gnu-build-system)
    (inputs
     `(("zlib" ,zlib)
       ("lz4" ,lz4)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((bin (string-append (assoc-ref outputs "out") "/bin")))
               (mkdir-p bin)
               (install-file "bench_blight" bin))
             #t)))))
    (home-page "https://github.com/Malfoy/Blight")
    (synopsis "de Bruijn graph index with light memory usage")
    (description "Blight is an index structure able to index the kmers
of a de Bruijn graph and to associate an unique identifier to each
kmer. The index can be used as a set that will return true when
a query kmer is in the set false otherwise. The index can also be used
as an associative data structure that will return a unique
identifier (between 1 and N where N is the number of indexed kmer) to
each kmer in the set and -1 to kmer that are not indexed. This index
can therefore be used to associate information to kmers in a efficient
and exact way with the use of a simple array of value.")
    (license license:agpl3+)))

(define-public bloocoo
  ;; FIXME probably not the most clever way to have bloocoo recompile
  ;; gatb-core and all its dependencies, but knowledge of CMake
  ;; witchcraft is beyond me. somehow bloocoo depends on gatb-core via
  ;; an `include (GatbCore)` in CMakeLists.txt that messes up with the
  ;; search path because of dynamic scoping of CMake's include, from
  ;; what I can guess.
  (package
    (name "bloocoo")
    (version "1.0.7")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             (format #f
                     "https://github.com/GATB/bloocoo/releases/download/v~a/Bloocoo-v~a-Source.tar.gz"
                     version version)))
       (sha256
        (base32
         "0lp6qvy7fz3kghx25mfycki783lxy8bk4jn08mqv56qbszb0gjv5"))))
    (build-system cmake-build-system)
    (propagated-inputs
     ;; for scripts
     `(("r-minimal" ,r-minimal)
       ("python-wrapper" ,python-wrapper)
       ("python-future" ,python-future)))
    (native-inputs
     `(("zlib" ,zlib)))
    (arguments
     `(#:tests? #f))
    (home-page "https://gatb.inria.fr/software/bloocoo/")
    (synopsis "Bloocoo: read error corrector")
    (description "Bloocoo is a k-mer spectrum-based read error
corrector, designed to correct large datasets with a very low memory
footprint.")
    (license license:agpl3+)))


(define-public btrim
  (let ((commit "7d5dc9a39083f3011ba1310a9858a5527ca16600")
        (revision "1.0.1"))
    (package
      (name "btrim")
      (version (git-version revision "0" commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url  "https://github.com/Malfoy/BTRIM")
               (commit commit)))
         (sha256 (base32 "00486w1lh1ghxjj23gsi261pbm4jdfrdn1rrpzg6b46w6i2fz1j7"))))
      (build-system gnu-build-system)
      (inputs
       `(("gcc" ,gcc-9)
         ("zlib" ,zlib)))
      (arguments
       `(#:make-flags
         (list
          (string-append "CC=" (assoc-ref %build-inputs "gcc") "/bin/g++")
          "CFLAGS=-O3 -std=c++14 -pthread -pipe -fopenmp")
         #:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (delete 'bootstrap)
           (delete 'check)
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin"))
                      (inst (lambda (x) (install-file x bin))))
                 (for-each inst '("btrim"))
                 #t))))))
      (home-page "https://github.com/Malfoy/BTRIM")
      (synopsis "de Bruijn graph TRIMming tool")
      (description "This tool is made to remove \"tips\" (short dead ends) from a compacted de Bruijn graph.")
      (license license:agpl3))))


;; (define-public seqkit
;;   (package
;;     (name "seqkit")
;;     (version "0.13.2")
;;     (source
;;      (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/shenwei356/seqkit")
;;              (commit (string-append "v" version))))
;;        (sha256
;;         (base32
;;          "082862yvdqnnbj516f14xf8lxi9yq96ka2pz3lh6fcjy7s7p7xgk"))))
;;     (build-system go-build-system)
;;     (propagated-inputs
;;      `(("perl" ,perl)
;;        ("r-minimal" ,r-minimal)))
;;     (arguments
;;      `(#:unpack-path
;;        "github.com/shenwei356/seqkit"
;;        #:import-path
;;        "github.com/shenwei356/seqkit/seqkit"
;;        #:phases
;;        (modify-phases %standard-phases
;;          (replace 'build
;;            (lambda* (#:key inputs outputs #:allow-other-keys)
;;              (invoke "go get -u github.com/shenwei356/seqkit/seqkit"))))))
;;     (home-page "https://bioinf.shenwei.me/seqkit/")
;;     (synopsis "SeqKit - a cross-platform and ultrafast toolkit for FASTA/Q file manipulation")
;;     (description "This project describes a cross-platform ultrafast
;; comprehensive toolkit for FASTA/Q processing. SeqKit provides
;; executable binary files for all major operating systems, including
;; Windows, Linux, and Mac OS X, and can be directly used without any
;; dependencies or pre-configurations. SeqKit demonstrates competitive
;; performance in execution time and memory usage compared to similar
;; tools. The efficiency and usability of SeqKit enable researchers to
;; rapidly accomplish common FASTA/Q file manipulations")
;;     (license #f)))                      ; MIT license

(define-public bubbz
  (package
    (name "bubbz")
    (version "1.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/medvedevgroup/BubbZ")
             (commit (string-append "v" version))))
       (sha256
        (base32 "11d0fxai4vn94yjf9vxajlj99k09s78zaf88dx79mgrkh0642d68"))))
    (build-system cmake-build-system)
    (inputs
     `(("twopaco" ,(package-source twopaco))
       ("tbb" ,tbb)
       ("gcc" ,gcc-9)))
    (propagated-inputs
     `(("twopaco-bin" ,twopaco)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fetch-twopaco
           (lambda* (#:key inputs #:allow-other-keys)
             (copy-recursively (assoc-ref inputs "twopaco") "TwoPaCo")
             #t))
         (add-after 'unpack 'fix-twopaco-path
           (lambda* (#:key inputs #:allow-other-keys)
             (substitute* '("BubbZ/bubbz")
               (("\\$DIR\\/twopaco") (string-append (assoc-ref inputs "twopaco-bin") "/bin/twopaco")))
             #t))
         (add-after 'install 'delete-twopaco-derived-bins
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((bin (lambda (x) (string-append (assoc-ref outputs "out") "/bin/" x))))
               (delete-file (bin "twopaco"))
               (delete-file (bin "graphdump")))
             #t)))))
    (home-page "https://github.com/medvedevgroup/BubbZ")
    (synopsis "BubbZ is a whole-genome homology mapping pipeline.")
    (description "BubbZ is a whole-genome homology mapping pipeline.
BubbZ works best in the case when the user needs to find all (possibly
overalpping) pairwise mappings in a collection of genomes. The
mappings are output in GFF format.")
    (license license:bsd-3)))


(define-public maf2synteny
  (package
    (name "maf2synteny")
    (version "1.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/fenderglass/maf2synteny")
             (commit version)))
       (sha256 (base32 "1d6dysw6rzr7xjrpn6ccl7b5di1f79316ak1fb2fy9159ryz73b7"))))
    (build-system gnu-build-system)
    (inputs
     `(("gcc" ,gcc-9)))
    (arguments
     `(#:make-flags
       (list (string-append "CXX=" (assoc-ref %build-inputs "gcc") "/bin/g++")
             (string-append "BIN_DIR=" (assoc-ref %outputs "out") "/bin"))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'check)
         (delete 'install)
         (add-before 'build 'create-bin-dir
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((bin (string-append (assoc-ref outputs "out") "/bin")))
               (mkdir-p bin)))))))
    (home-page "https://github.com/fenderglass/maf2synteny")
    (synopsis "A tool for recovering synteny blocks from multiple alignment")
    (description "A tool that postprocesses whole genome alignment (for two or
more genomes) and produces coarse-grained synteny blocks. Currently, either
Cactus or SibeliaZ alignments are supported.")
    (license license:bsd-3)))

;; stolen from ekg guix-genomics channel <https://github.com/ekg/guix-genomics/blob/master/shasta.scm>
(define-public spoa
  (let ((version "3.0.1")
        (commit "9dbcd7aa223c1e7fa789530c39fcd143d3886d3b")
        (package-revision "4"))
    (package
     (name "spoa")
     (version (string-append version "+" (string-take commit 7) "-" package-revision))
     (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/rvaser/spoa.git")
                    (commit commit)
                    (recursive? #t)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1h90s9nqwp4lr3wl8mhcd2yd66z58d2q3zrn3n7fnwxs71ncwfw6"))))
     (build-system cmake-build-system)
     (arguments
      `(#:configure-flags
        (list
         "-Dspoa_build_executable=ON"
         "-DCMAKE_BUILD_TYPE=Release"
         "-DCMAKE_CXX_FLAGS=-O3 -fPIC"
         "-DCMAKE_C_FLAGS=-O3 -fPIC")
        #:phases
        (modify-phases
         %standard-phases
         (delete 'check))))
     (native-inputs
      `(("gcc" ,gcc-9)
        ("cmake" ,cmake)))
     (synopsis "c++ implementation of the partial order alignment (POA) algorithm")
     (description
"Spoa (SIMD POA) is a c++ implementation of the partial order alignment (POA)
algorithm (as described in 10.1093/bioinformatics/18.3.452) which is used to
generate consensus sequences (as described in 10.1093/bioinformatics/btg109). It
supports three alignment modes: local (Smith-Waterman), global
(Needleman-Wunsch) and semi-global alignment (overlap), and three gap modes:
linear, affine and convex (piecewise affine). It supports Intel SSE4.1+ and AVX2
vectorization (marginally faster due to high latency shifts).")
     (home-page "https://github.com/rvaser/spoa.git")
     (license license:expat))))

(define-public sibeliaz
  (let ((commit "16fc5477799a53a7547cad593402545d05c9f95a")
        (revision "0"))
    (package
      (name "sibeliaz")
      (version (git-version "1.2.2" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/medvedevgroup/SibeliaZ")
               (commit commit)))
         (sha256
          (base32 "0gyx72vcyxdp9cd2mw8rl73zs9415k44fkyqx0fpcarsk6xs1jk4"))))
      (build-system cmake-build-system)
      (native-inputs
       `(("sed" ,sed)))
      (inputs
       `(("time" ,time)
         ("tbb" ,tbb)
         ("gcc" ,gcc-9)))
      (propagated-inputs
       `(("python" ,python-2)
         ("python-numpy" ,python2-numpy)
         ("python-biopython" ,python2-biopython)
         ("twopaco" ,twopaco)
         ("spoa" ,spoa)
         ("maf2synteny" ,maf2synteny)))
      (arguments
       `(#:tests?
         #f
         #:configure-flags
         (list "-DNO_DEPENDENCIES=1")
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'fix-bin-path
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* '("SibeliaZ-LCB/sibeliaz")
                 (("/usr/bin/time") (string-append (assoc-ref inputs "time") "/bin/time"))
                 (("\\$\\{DIR\\}twopaco") (string-append (assoc-ref inputs "twopaco") "/bin/twopaco"))
                 (("\\$\\{DIR\\}spoa") (string-append (assoc-ref inputs "spoa") "/bin/spoa")))
               #t))
           (add-after 'install 'install-python-scripts
             (lambda* (#:key outputs inputs #:allow-other-keys)
               (let ((bin (string-append (assoc-ref outputs "out") "/bin")))
                 (with-directory-excursion "../source"
                   (for-each (lambda (script)
                               ;; add shebang
                               (invoke "sed" "-i" (string-append "1i#!" (assoc-ref inputs "python") "/bin/python") script)
                               (chmod script #o555)
                               (install-file script bin)
                               (wrap-program (string-append bin "/" (basename script))
                                 `("PYTHONPATH" ":" prefix (,(getenv "PYTHONPATH")))))
                             '("SibeliaZ-LCB/maf_to_gfa1.py"
                               "SibeliaZ-LCB/maf_to_xmfa.py")))))))))
      (home-page "https://github.com/medvedevgroup/BubbZ")
      (synopsis "A fast whole-genome aligner based on de Bruijn graphs")
      (description "SibeliaZ is a whole-genome alignment and locally-coliinear
blocks construction pipeline. The blocks coordinates are output in GFF format
and the alignment is in MAF.

SibeliaZ was designed for the inputs consisting of multiple similar genomes,
like different strains of the same species. The tool works best for the
datasets with the distance from a leaf to the most recent common ancestor not
exceeding 0.09 substitutions per site, or 9 PAM units.

Currently SibeliaZ does not support chromosomes in the input longer than
4294967296 bp, this will be fixed in the future releases.")
      (license license:bsd-4))))


(define-public busco-5
  (package
    (name "busco")
    (version "5.beta")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://gitlab.com/ezlab/busco/")
             (commit (string-append "v" version))))
       (sha256
        (base32 "1wxvwdmd62gbraxsj03knj16w4i4099cnixx4580yv009v663901"))))
    (build-system python-build-system)
    (native-inputs
     `(("sed" ,sed)))
    (propagated-inputs
     `(("python-biopython" ,python-biopython)
       ("python-numpy" ,python-numpy)
       ("prodigal" ,prodigal)
       ("tblastn" ,blast+)
       ("makeblastdb" ,blast+)
       ("hmmsearch" ,hmmer)
       ("run_sepp.py" ,sepp)
       ("metaeuk" ,metaeuk)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'build 'replace-config-path
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let ((get-bin (lambda (name) (string-append (assoc-ref inputs name) "/bin/"))))
               (invoke "sed" "-i" (string-append "70s|/usr/local/bin/|" (get-bin "hmmsearch") "|")
                       "config/config.ini")
               (invoke "sed" "-i" (string-append "78s|/usr/local/bin/|" (get-bin "prodigal") "|")
                       "config/config.ini")
               (substitute* '("config/config.ini")
                 (("\\/ncbi-blast-2\\.10\\.1\\+\\/bin\\/") (get-bin "tblastn"))
                 (("\\/metaeuk\\/build\\/bin\\/") (get-bin "metaeuk"))
                 (("\\/home\\/biodocker\\/sepp\\/") (get-bin "run_sepp.py")))
               (substitute* '("scripts/busco_configurator.py")
                 (("config\\.ini\\.default")
                  (string-append (assoc-ref outputs "out") "/share/config/config.ini.default"))))
             #t))
         (add-after 'install 'add-busco-scripts-and-wrap
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin/"))
                    (cfg (string-append out "/share/config/config.ini.default")))
               (mkdir-p (string-append out  "/share/config"))
               (copy-file "config/config.ini" cfg)
               (copy-recursively "scripts/" bin))
             #t))
         )))
    (home-page "https://busco.ezlab.org/")
    (synopsis "BUSCO: Assessing Genome Assembly and Annotation Completeness")
    (description "Based on evolutionarily-informed expectations of
gene content of near-universal single-copy orthologs, BUSCO metric is
complementary to technical metrics like N50.")
    (license license:expat)))

;; MCCORTEX ----------------------------------------------------------

(define-public busco-coverage
  (package
    (name "busco-coverage")
    (version "1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/hrz-project/busco-coverage")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1kmqw4hdvnw72ma2k3p6i0bn9kygldca27dmq8b2i1byl6y1vncm"))))
    (build-system copy-build-system)
    (native-inputs
     `(("make" ,gnu-make)))
    (propagated-inputs
     `(("perl" ,perl)
       ,@horizon-perl-deps
       ("parallel" ,parallel)
       ("samtools-dedup" ,samtools-dedup)
       ("bedtools" ,bedtools)
       ("diamond" ,diamond)
       ("bwa-mem2" ,bwa-mem2)
       ("htslib" ,htslib-1.10)               ;for bgzip
       ("samtools" ,samtools-1.10)
       ("r-minimal" ,r-minimal)
       ("r-data-table" ,r-data-table)))
    (arguments
     `(#:install-plan
       '(("bin/busco-coverage.pl" "bin/busco-coverage"))))
    (home-page "https://gitlab.com/hrz-project/busco-coverage")
    (synopsis "Compute coverage at BUSCO locations")
    (description "This software uses the read coverage measured on
genes identified as being universal single-copy orthologs, as used by
the commonly used BUSCO pipeline to estimate genome size.")
    (license license:gpl3)))

;; CANCEL: for now I don't know how you can build singularity with
;; guix, using go and its gopath tricks.

;; (define-public singularity-3.6
;;   (package
;;     (inherit singularity)
;;     (version "3.6.2")
;;     (source
;;      (origin
;;        (method url-fetch)
;;        ;; https://github.com/hpcng/singularity/releases/download/v3.6.2/singularity-3.6.2.tar.gz
;;        (uri (format #f "https://github.com/hpcng/singularity/releases/download/v~a/singularity-~a.tar.gz"
;;                     version version))
;;        (sha256
;;         (base32
;;          "16sd08bfa2b1qgpnd3q6k7glw0w1wyrqyf47fz2220yafrryrmyz"))))
;;     (home-page "https://sylabs.io/singularity/")
;;     (license license:bsd-3)))



(define-public bwa-mem2
  (package
    (name "bwa-mem2")
    (version "2.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/bwa-mem2/bwa-mem2")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "1y8v1cgn0m3dwlxbj95xf4rk9mbansxk0sxqsnqz7nifjlpcakqg"))))
    (build-system gnu-build-system)
    (inputs `(("zlib" ,zlib)
              ("safestringlib" ,safestringlib)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (replace 'configure
           (lambda* (#:key inputs #:allow-other-keys)
             (let* ((safestringlib (assoc-ref inputs "safestringlib"))
                    (lib-static  (string-append safestringlib "/lib/libsafestring.a"))
                    (lib-headers (string-append "-I" safestringlib "/include")))
               (substitute* "Makefile"
                 (("-Iext/safestringlib/include") lib-headers)
                 (("SAFE_STR_LIB=.*$") (string-append "SAFE_STR_LIB=" lib-static))
                 (("-Lext/safestringlib") (string-append "-L" safestringlib))))
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin")))
               (mkdir-p bin)
               (install-file "bwa-mem2" bin)))))))
    (home-page "https://github.com/bwa-mem2/bwa-mem2")
    (synopsis "Fast read mapping using next version of BWA algorithm")
    (description "Bwa-mem2 is the next version of the bwa-mem
algorithm in bwa. It produces alignment identical to bwa and is
~1.3-3.1x faster depending on the use-case, dataset and the running
machine.")
    (license license:expat)))

;; VARIATION GRAPH ---------------------------------------------------

(define-public edyeet
  (let ((commit "0de36908b0e66256a7f73df2a4b8ee593050f180")
        (revision "0"))
    (package
      (name "edyeet")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/ekg/edyeet")
               (commit commit)))
         (sha256
          (base32 "166l1dqs6sag3bc1c64lzcfdygmcmrafyx14wvxnmp9kxfam700r"))))
      (native-inputs
       `(("autoconf" ,autoconf)))
      (inputs
       `(("zlib" ,zlib)
         ("gsl" ,gsl)
         ("perl" ,perl)))
      (build-system gnu-build-system)
      (arguments
       `(#:tests? #f))
      (home-page "https://github.com/ekg/edyeet")
      (synopsis "base-accurate DNA sequence alignments using edlib and mashmap2")
      (description "edyeet is a fork of MashMap that implements
base-level alignment using edlib. It completes an alignment module in
MashMap and extends it to enable multithreaded operation. A single
command-line interface simplfies usage. The PAF output format is
harmonized and made equivalent to that in minimap2, and has been
validated as input to seqwish.")
      (license license:public-domain))))

(define-public fastp-0.20.1
  (package
    (inherit fastp)
    (name "fastp")
    (version "0.20.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/OpenGene/fastp")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0ly8mxdvrcy23jwxyppysx3dhb1lwsqhfbgpyvargxhfk6k700x4"))))))
(define-public gatb-core
  ;; FIXME should probably use hdf5 from guix rather than thirdparty
  ;; downloaded with git.
  (package
    (name "gatb-core")
    (version "1.4.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/GATB/gatb-core")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0xrw4fbaabynranrqcg7cz1193zrbgcfdf4rcklyywcfq9rxl30l"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("perl" ,perl)                   ;for generating H5Pubgen.h
       ("gcc" ,gcc)
       ("boost" ,boost)
       ("hdf5" ,hdf5)))
    (inputs
     `(("zlib" ,zlib)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'enter-source (lambda _ (chdir "gatb-core") #t)))))
    (home-page "https://gatb.inria.fr")
    (synopsis "GATB: The Genome Analysis Toolbox with de-Bruijn graph")
    (description "The Genome Analysis Toolbox with de-Bruijn graph (GATB)
provides a set of highly efficient algorithms to analyse NGS data
sets. These methods enable the analysis of data sets of any size on
multi-core desktop computers, including very huge amount of reads data
coming from any kind of organisms such as bacteria, plants, animals
and even complex samples (e.g. metagenomes).")
    (license license:agpl3+)))

(define-public glimmer
  (package
    (name "glimmer")
    (version "3.02")
    (source
     (origin
       (method url-fetch)
       (uri "http://ccb.jhu.edu/software/glimmer/glimmer302b.tar.gz")
       (sha256
        (base32 "1xib3ryzmnffazk774l8xb1zb4wry6klr8qc6vbswxd6s01qxwpc"))))
    (build-system gnu-build-system)
    (propagated-inputs
     `(("gawk" ,gawk)
       ("tcsh" ,tcsh)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-before 'build 'change-to-src-directory
           (lambda _ (chdir "src") #t))
         (add-before 'install 'change-to-build-dir
           (lambda _ (chdir "..") #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (scripts (string-append out "/share/scripts")))
               (mkdir-p bin)
               (mkdir-p scripts)
               (copy-recursively "bin" bin)
               (copy-recursively "scripts" scripts)
               (copy-file "docs/notes.pdf" (string-append out "/share/doc")))
             #t)))))
    (home-page "http://ccb.jhu.edu/software/glimmer/index.shtml")
    (synopsis "Glimmer is a system for finding genes in microbial DNA")
    (description "Glimmer is a system for finding genes in microbial
DNA, especially the genomes of bacteria, archaea, and viruses.
Glimmer (Gene Locator and Interpolated Markov ModelER) uses
interpolated Markov models (IMMs) to identify the coding regions and
distinguish them from noncoding DNA. The IMM approach is described in
our original Nucleic Acids Research paper on Glimmer 1.0 and in our
subsequent paper on Glimmer 2.0. The IMM is a combination of Markov
models from 1st through 8th-order, where the order used is determined
by the amount of data available to train the model. In addition, the
positions used as context for the model need not immediately precede
the predicted postion but are determined by a decision procedure based
on the predictive power of each position in the training data
set (which we term an Interpolated Context Model or ICM). The models
for coding sequence are 3-periodic nonhomogenous Markov models.
Improvements made in version 3 of Glimmer are described in the third
Glimmer paper.")
    (license #f)))

(define-public glimmer-hmm
  (package
    (name "glimmer-hmm")
    (version "3.0.4")
    (source
     (origin
       (method url-fetch)
       (uri "http://ccb.jhu.edu/software/glimmerhmm/dl/GlimmerHMM-3.0.4.tar.gz")
       (sha256
        (base32 "09q7cp8ccdyczdi5r7nbvis7fx7vs7fyijslh7bs6jcz5dwj3qs3"))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'bootstrap)
         (replace 'build
           (lambda _
             (with-directory-excursion "sources"
               (invoke "make" "-j" (number->string (parallel-job-count))))
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (trained (string-append out "/trained_dir")))
               (mkdir-p bin)
               (copy-file "sources/glimmerhmm"
                          (string-append bin "/glimmerhmm"))
               (copy-recursively "trained_dir" trained))
             #t)))))
    (home-page "http://ccb.jhu.edu/software/glimmerhmm/")
    (synopsis "Eukaryotic Gene-Finding System")
    (description "GlimmerHMM is a new gene finder based on a Generalized Hidden Markov Model (GHMM). Although the gene finder conforms to the overall mathematical framework of a GHMM, additionally it incorporates splice site models adapted from the GeneSplicer program and a decision tree adapted from GlimmerM. It also utilizes Interpolated Markov Models for the coding and noncoding models . Currently, GlimmerHMM's GHMM structure includes introns of each phase, intergenic regions, and four types of exons (initial, internal, final, and single)")
    (license license:artistic2.0)))

(define-public htslib-1.10
  (package
    (inherit htslib)
    ;; (name "htslib-1.10")
    (version "1.10.2")
    (source (origin
              (method url-fetch)
              (uri
               (format #f "https://github.com/samtools/htslib/releases/download/~a/htslib-~a.tar.bz2"
                       version     version))
              (sha256
               (base32 "0f8rglbvf4aaw41i2sxlpq7pvhly93sjqiz0l4q3hwki5zg47dg3"))))
    (home-page "https://www.htslib.org/")))

(define-public kmc
  (package
    (name "kmc")
    (version "3.1.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/refresh-bio/KMC")
             (commit "cf669139d9ecc08646a3aaf18a3f732476269ba7")))
       (sha256
        (base32 "00kpd992kklsk31qlac6b2ng731lrx9djmj0rw0wv492q45alsxf"))))
    (build-system gnu-build-system)
    (inputs
     `(("zlib" ,zlib)
       ("bzip2" ,bzip2)
       ("python-wrapper" ,python-wrapper)
       ("pybind11" ,pybind11)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'check)
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin")))
               (mkdir-p bin)
               (copy-recursively "bin" bin)
               (let ((pylib (car (find-files (string-append bin "/") "py_kmc_api.*"))))
                 (wrap-program pylib
                   `("PYTHONPATH" ":" = (,pylib ,(getenv "PYTHONPATH")))))))))))
    (home-page "http://sun.aei.polsl.pl/kmc/")
    (synopsis "KMC is a disk-based programm for counting k-mers from (possibly gzipped) FASTQ/FASTA files")
    (description "KMC—K-mer Counter is a utility designed for counting k-mers (sequences of consecutive k symbols) in a set of reads from genome sequencing projects. K-mer counting is important for many bioinformatics applications, e.g., developing de Bruijn graph assemblers. Building de Bruijn graphs is a commonly used approach for genome assembly with data from second-generation sequencer. Unfortunately, sequencing errors (frequent in practice) results in huge memory requirements for de Bruijn graphs, as well as long build time. One of the popular approaches to handle this problem is filtering the input reads in such a way that unique k-mers (very likely obtained as a result of an error) are discarded.")
    (license license:gpl3)))

(define-public kronatools
  (package
    (name "kronatools")
    (version "2.7.1")
    (source
     (origin
       (method url-fetch)
       (uri "https://github.com/marbl/Krona/releases/download/v2.7.1/KronaTools-2.7.1.tar")
       (file-name (string-append "kronatools-" version))
       (sha256
        (base32 "06kagrz2rdpxf3k0km0x6rq59sambg0clbr0mvb7rbc541s5xcwg"))))
    (build-system copy-build-system)
    (arguments
     `(#:install-plan
       '(("src" "src")
         ("data" "data")
         ("img" "img")
         ("lib" "lib")
         ("scripts" "scripts")
         ("taxonomy" "taxonomy"))
       #:phases
       (modify-phases %standard-phases
         (add-after 'install 'link-kt
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((scrdir (string-append (assoc-ref outputs "out") "/scripts"))
                   (bin (string-append (assoc-ref outputs "out") "/bin")))
               (mkdir-p bin)
               (for-each (lambda (script)
                           (let ((from (string-append scrdir "/" script ".pl"))
                                 (to (string-append bin "/kt" script)))
                             (symlink from to)))
                         '("ClassifyHits" "ClassifyBLAST"
                           "GetContigMagnitudes" "GetLCA" "GetLibPath" "GetTaxIDFromAcc"
                           "GetTaxInfo" "ImportBLAST" "ImportDiskUsage" "ImportEC" "ImportFCP"
                           "ImportGalaxy" "ImportHits" "ImportKrona" "ImportMETAREP-BLAST"
                           "ImportMETAREP-EC" "ImportMGRAST" "ImportPhymmBL" "ImportRDP"
                           "ImportRDPComparison" "ImportTaxonomy" "ImportText" "ImportXML"))))))))
    (propagated-inputs
     `(("perl" ,perl)))
    (home-page "https://github.com/marbl/Krona/wiki")
    (synopsis "Krona allows hierarchical data to be explored with zooming, multi-layered pie charts")
    (description "Krona allows hierarchical data to be explored with zooming, multi-layered pie charts. Krona charts can be created using an Excel template or KronaTools, which includes support for several bioinformatics tools and raw data formats. The interactive charts are self-contained and can be viewed with any modern web browser.")
    (license #f)))


(define-public mash
  (package
    (name "mash")
    (version "2.2")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/marbl/mash")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "135r7xm15yfvk9ygvsazv53impbj1nlwfb4zgy9snl68jwpi47y5"))
              (modules '((guix build utils)))
              (snippet
               '(begin
                  ;; Delete bundled kseq.
                  ;; TODO: Also delete bundled murmurhash and open bloom filter.
                  (delete-file "src/mash/kseq.h")
                  #t))))
    (build-system gnu-build-system)
    (arguments
     `(#:tests? #f ; No tests.
                #:configure-flags
                (list
                 (string-append "--with-capnp=" (assoc-ref %build-inputs "capnproto"))
                 (string-append "--with-gsl=" (assoc-ref %build-inputs "gsl")))
                #:make-flags (list "CC=gcc")
                #:phases
                (modify-phases %standard-phases
                  (add-after 'unpack 'fix-includes
                    (lambda _
                      (substitute* '("src/mash/Sketch.cpp"
                                     "src/mash/CommandFind.cpp"
                                     "src/mash/CommandScreen.cpp")
                        (("^#include \"kseq\\.h\"")
                         "#include \"htslib/kseq.h\""))
                      #t))
                  (add-after 'fix-includes 'use-c++14
                    (lambda _
                      ;; capnproto 0.7 requires c++14 to build
                      (substitute* "configure.ac"
                        (("c\\+\\+11") "c++14"))
                      (substitute* "Makefile.in"
                        (("c\\+\\+11") "c++14"))
                      #t)))))
    (native-inputs
     `(("autoconf" ,autoconf-wrapper)
       ("automake" ,automake)
       ;; Capnproto and htslib are statically embedded in the final
       ;; application. Therefore we also list their licenses, below.
       ("capnproto" ,capnproto)
       ("htslib" ,htslib)))
    (inputs
     `(("gsl" ,gsl)
       ("zlib" ,zlib)))
    (supported-systems '("x86_64-linux"))
    (home-page "https://mash.readthedocs.io")
    (synopsis "Fast genome and metagenome distance estimation using MinHash")
    (description "Mash is a fast sequence distance estimator that uses the
MinHash algorithm and is designed to work with genomes and metagenomes in the
form of assemblies or reads.")
    (license (list license:bsd-3          ; Mash
                   license:expat          ; HTSlib and capnproto
                   license:public-domain  ; MurmurHash 3
                   license:cpl1.0))))     ; Open Bloom Filter


(define-public mashtree
  (package
    (name "mashtree")
    (version "1.2.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/L/LS/LSKATZ/Mashtree-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0bis822r347lj78jrvxwlhz02lbpjfynbm0gdhw4fcr7lln687sg"))))
    (build-system perl-build-system)
    (native-inputs
     `(("sed" ,sed)))
    (inputs
     `(("ross" ,(origin
                  (method url-fetch)
                  (uri (format #f "https://github.com/lskatz/ROSS/archive/v0.3.tar.gz"))
                  (sha256 (base32 "0j42vbc2g62c5szkcv7ws5926y8iqb48cbbzpc3a1gfhxg4rg7yy"))))))
    (propagated-inputs
     `(("mash" ,mash)
       ("quicktree" ,quicktree)
       ("perl-bio-kmer" ,perl-bio-kmer)
       ("perl-bio-sketch-mash" ,perl-bio-sketch-mash)
       ("bioperl" ,bioperl-minimal)
       ("perl-dbd-sqlite" ,perl-dbd-sqlite)
       ("perl-dbi" ,perl-dbi)
       ("perl-file-which" ,perl-file-which)
       ("perl-json" ,perl-json)
       ("perl-list-moreutils" ,perl-list-moreutils)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'copy-ross
           (lambda* (#:key inputs #:allow-other-keys)
             (copy-file (assoc-ref inputs "ross") "bin/ROSS-v0.3.tar.gz")
             (invoke "tar" "xvzf" "./bin/ROSS-v0.3.tar.gz" "--directory" "./bin")
             #t))
         (add-after 'unpack 'remove-ross-fetch
           (lambda _
             (invoke "sed" "-i" "20,27d;41,43d" "Makefile.PL")
             #t)))))
    (home-page
     "https://metacpan.org/release/Mashtree")
    (synopsis "Create a tree using Mash distances.")
    (description "Create a tree using Mash distances.")
    (license license:gpl3)))

(define-public mccortex
  (package
    (name "mccortex")
    (version "1.0.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/mcveanlab/mccortex")
             (commit (string-append "mccortex-" version))
             (recursive? #t)))
       (sha256
        (base32 "00l5vd6iipd8scc0ng83c03p5c43v2m22mp6y00n6km0lcvc62c3"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("gcc" ,gcc-9)
       ("sed" ,sed)
       ("autoconf" ,autoconf)
       ("automake" ,automake)))
    (inputs
     `(("zlib" ,zlib)
       ("bzip2" ,bzip2)
       ("curses" ,ncurses)              ;for embedded htslib
       ("bioinf-perl" ,bioinf-perl)
       ("biogrok" ,biogrok)
       ("vcf-slim" ,vcf-slim)
       ("htslib" ,htslib)
       ("bwa" ,bwa)
       ("curl" ,curl)
       ("xz" ,xz)
       ("libcrypto" ,openssl)
       ("bcftools" ,bcftools)))
    (propagated-inputs
     `(("make" ,gnu-make)
       ("perl" ,perl)
       ("python-wrapper" ,python-wrapper)
       ("python2.7" ,python-2.7)
       ("luajit" ,luajit)
       ("r-minimal" ,r-minimal)))
    ;; mccortex scripts comes with all kind of scripts; makes sense to
    ;; install them to another directory
    (outputs '("out" "scripts"))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fix-cc-compiler
           (lambda _ (setenv "CC" (which "gcc")) #t))
         (delete 'bootstrap)
         (delete 'configure)
         (add-after 'unpack 'use-system-htslib
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((htslib (lambda (x) (string-append (assoc-ref inputs "htslib") "/" x))))
               (substitute* '("libs/Makefile")
                 (("CORE=htslib ") "CORE="))
               (substitute* '("Makefile")
                 (("IDIR_HTS=.*$") (string-append "IDIR_HTS=" (htslib "include/htslib") "\n"))
                 (("LIB_HTS=.*$")  (string-append "LIB_HTS=" (htslib "lib/libhts.a") "\n"))))
             #t))
         (add-after 'unpack 'fix-paths
           ;; really ugly stuff to fix path to input
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((gbin (lambda (x) (lambda (y) (string-append (assoc-ref inputs x) "/bin/" y))))
                    (hts  (lambda (x) (string-append (assoc-ref inputs "htslib") x)))
                    (bgk  (gbin "biogrok"))
                    (vsl  (gbin "vcf-slim"))
                    (ctx-scripts
                     (lambda (x) (string-append (assoc-ref outputs "scripts") "/bin/" x)))
                    (sed
                     (lambda (file pattern replace)
                       (invoke "sed" "-i"
                               (string-append "s|^" pattern ".*$|" pattern "=" replace "|") file))))
               (for-each
                (lambda (file)
                  (sed file "BCFTOOLS" ((gbin "bcftools") "bcftools"))
                  (sed file "BGZIP" ((gbin "htslib") "bgzip"))
                  (sed file "BWA" ((gbin "bwa") "bwa"))
                  (sed file "CTXFLANKS" (ctx-scripts "cortex_print_flanks.sh"))
                  (sed file "CTXKCOV" (ctx-scripts "mccortex-kcovg.pl"))
                  (sed file "HRUNANNOT" (vsl "vcfhp"))
                  (sed file "SAM2VCF" (vsl "sam-name-to-vcf.sh"))
                  (sed file "SAMCMP" (ctx-scripts "analysis/haploid-sam-compare.py"))
                  (sed file "VCFALLELES" (bgk "vcf-count-alleles"))
                  (sed file "VCFCONTIGS" (vsl "vcfcontigs"))
                  (sed file "VCFNLINES" (vsl "vcf-count"))
                  (sed file "VCFNLINES" (vsl "vcf-count"))
                  (sed file "VCFRENAME" (bgk "vcf-rename"))
                  (sed file "VCFSORT" (bgk "vcf-sort"))
                  (sed file "VCF_SELECT_ID" (bgk "vcf-select-id")))
                (append (find-files "scripts" "\\.pl$")
                        (find-files "scripts" "\\.sh$")))
               (for-each
                (lambda (file)
                  ;; remove bioinf perl, handled by wrap script below
                  (invoke "sed" "-i" "s|^.*bioinf-perl/lib.*$||" file)
                  ;; lookup for mccortex perl modules in current directory
                  ;; (flattened directory by installation)
                  (invoke "sed" "-i" "s|^use lib \\$FindBin.*perl.*$|use lib \\$FindBin::Bin;|" file))
                (find-files "scripts" "\\.pl$"))
               (for-each
                (lambda (file)
                  (substitute* `(,file)
                    (("-I \\$\\(HTSLIB\\)") (string-append "-I" (hts "/include/htslib/")))
                    (("\\$\\(HTSLIB\\)\\/libhts.a") (hts "/lib/libhts.a  -lcurl -lcrypto -lbz2 -llzma"))))
                (find-files "libs/" "Makefile"))
               (invoke "sed" "-i"
                       (string-append "s|#include \"htslib/version.h\"|"
                                      "#define HTS_VERSION \"" ,(package-version htslib) "\""
                                      "|")
                       "src/global/global.h")
               (sed "Makefile" "LINK" "-lpthread -lz -lm -lcurl -lcrypto -lbz2 -llzma"))
             #t))
         (replace 'build
           (lambda _
             (invoke "make" "-j" (number->string (parallel-job-count))
                     "MAXK=63" "mccortex")
             (invoke "make" "-j" (number->string (parallel-job-count))
                     "MAXK=31" "mccortex")
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (scripts (assoc-ref outputs "scripts"))
                    (share   (string-append scripts "/bin/")))
               (mkdir-p bin)
               (mkdir-p share)
               (for-each
                (lambda (f) (copy-file (string-append "bin/" f)
                                       (string-append bin "/" f)))
                '("mccortex" "mccortex31" "mccortex63"))
               (for-each
                (lambda (f) (copy-file f (string-append share (basename f))))
                (find-files "scripts")))
             #t))
         (add-after 'install 'wrap-scripts
           ;; make sure that cortex scripts find the bioinf-perl modules
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (scr (assoc-ref outputs "scripts"))
                    (bin (string-append out "/bin/"))
                    (wrapper (string-append bin "mccortex-scripts"))
                    (share (string-append scr "/bin/"))
                    (bfperl (string-append (assoc-ref inputs "bioinf-perl") "/lib/")))
               (for-each
                (lambda (file) (wrap-program file `("PERL5LIB" ":" prefix (,bfperl))))
                (find-files share "\\.pl$"))
               (with-output-to-file wrapper
                 (lambda _
                   (display
                    (string-append
                     "#!" (which "sh") "\n"
                     "usage () {
cat <<EOF
mccortex-scripts [script] <args>

Run the mccortex scripts SCRIPT with ARGS passed to it.
Run mccortex-scripts without argument to get a list of available
scripts.

----------------------------------------------------------------------
                           MCCORTEX SCRIPTS
----------------------------------------------------------------------

EOF
}
"
                     "if [ -z \"$1\" ]; then usage && (cd " share " && ls -R .) && exit 1; fi" "\n"
                     "prog=\"$1\"" "\n"
                     "shift" "\n"
                     share "/${prog} \"$@\""))))
               (chmod wrapper #o555))
             #t)))))
    (home-page "https://github.com/mcveanlab/mccortex/wiki")
    (synopsis "McCortex is a genome assembler and variant caller.")
    (description "McCortex is a genome assembler and variant caller.
It has a rich set of features for representing and manipulating
genomes as graphs including:

@itemize
@item correcting reads
@item genome assembly
@item calling variants
@item genotyping variants
@end itemize

It can run with and without a reference genome. It is a re-write of
the original Cortex Assembler.")
    (license license:expat)))


;; BubbZ -------------------------------------------------------------

(define-public megahit
  (package
    (name "megahit")
    (version "1.2.9")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/voutcn/megahit")
                    (commit (string-append "v" version))))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "1r5d9nkdmgjsbrpj43q9hy3s8jwsabaz3ji561v18hy47v58923c"))))
    (build-system cmake-build-system)
    (arguments
     '(#:tests? #f ; no "check" target
                #:phases
                (modify-phases %standard-phases
                  (add-after 'unpack 'remove-native-compilation
                    (lambda _
                      (substitute* "CMakeLists.txt" (("-march=native") ""))
                      #t))
                  (replace 'install
                    (lambda* (#:key outputs #:allow-other-keys)
                      (let* ((out (assoc-ref outputs "out"))
                             (bin (string-append out "/bin")))
                        (install-file "megahit_toolkit" bin)
                        (install-file "megahit_core" bin)
                        (install-file "megahit_core_no_hw_accel" bin)
                        (install-file "megahit_core_popcnt" bin)
                        (install-file "megahit" bin))
                      #t)))))
    (native-inputs `(("zlib" ,zlib)))
    (inputs `(("gzip" ,gzip)
              ("bzip2" ,bzip2)))
    (propagated-inputs `(("python3" ,python-3)
                         ("python" ,python-wrapper)
                         ("python-future" ,python-future)))
    (home-page "https://github.com/voutctn/megahit")
    (synopsis "ultra-fast and memory-efficient NGS assembler")
    (description
     "MEGAHIT is an ultra-fast and memory-efficient NGS assembler.
It is optimized for metagenomes, but also works well on
generic single genome assembly (small or mammalian size)
and single-cell assembly.")
    (license license:gpl3+)))

(define-public metagraph
  (let ((commit "c6b5762835c6a323ea051d5b4471198149201636")
        (revision "1"))
    (package
      (name "metagraph")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/ratschlab/metagraph")
               (commit commit)
               (recursive? #t)))
         (sha256 (base32 "13n84qlpdrh0h54na6wainl4pb5c5zd821pc6gx6rmqm6x9mx00r"))))
      (build-system cmake-build-system)
      (native-inputs
       `(("sed" ,sed)
         ("autoconf" ,autoconf)
         ("automake" ,automake)
         ("cmake" ,cmake)
         ("make" ,gnu-make)
         ("libtool" ,libtool)
         ("python" ,python-wrapper)
         ("python2" ,python-2)
         ("pkg-config" ,pkg-config)))
      (inputs
       `(("bash" ,bash)
         ("binutils" ,binutils)
         ("gcc" ,gcc-9)
         ("libunwind" ,libunwind)
         ("perl" ,perl)))
      (propagated-inputs
       `(("asio" ,asio)
         ("boost" ,boost)
         ("bzip2" ,bzip2)
         ("curl" ,curl)
         ("double-conversion" ,double-conversion)
         ("jsoncpp" ,jsoncpp)
         ("fmt" ,fmt)
         ("folly" ,folly)
         ("gflags" ,gflags)
         ("glog" ,glog)
         ("htslib" ,htslib)
         ("jemalloc" ,jemalloc)
         ("kmc" ,kmc)
         ("libevent" ,libevent)
         ("libiberty" ,libiberty)
         ("libmaus2" ,libmaus2)
         ("lz4" ,lz4)
         ("lzma" ,xz)
         ("openssl" ,openssl)
         ("sdsl" ,sdsl-lite-hmusta)
         ("snappy" ,snappy)
         ("zlib" ,zlib)))
      (arguments
       `(#:tests?
         #f
         #:phases
         (modify-phases %standard-phases
           (add-before 'configure 'set-cmake-rpath
             (lambda _
               (invoke "sed" "-i"
                       ;; this is a way to tell cmake to use the link path as
                       ;; rpath during the install process. at this point one
                       ;; has to wonder what justified the switch from an ugly
                       ;; but working autotools to this ugly but unutterable
                       ;; mess that is cmake.
                       "380iset_target_properties\\(metagraph PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE\\)"
                       "metagraph/CMakeLists.txt")
               #t))
           (add-after 'unpack 'set-libs
             (lambda* (#:key inputs #:allow-other-keys)
               (let* ((inc      (lambda (x) (string-append (assoc-ref inputs x) "/include/" x)))
                      (lib      (lambda (x) (string-append (assoc-ref inputs x) "/lib")))
                      (libmaus2 (lambda (x) (string-append (assoc-ref inputs "libmaus2") "/" x)))
                      (sdsl     (lambda (x) (string-append (assoc-ref inputs "sdsl") "/" x))))
                 (for-each
                  (lambda (file) (substitute* `(,file)
                              (("enable_testing\\(\\)") "")
                              (("external-libraries/sdsl-lite/include") (inc "sdsl"))
                              (("external-libraries/Simple-Web-Server")
                               (string-append
                                "external-libraries/Simple-Web-Server" "\n  "
                                (inc "htslib") "\n  "
                                (inc "boost") "\n  "
                                (inc "glog") "\n  "
                                (inc "openssl") "\n  "
                                (inc "gflags") "\n  "
                                (inc "double-conversion") "\n  "
                                (string-append (assoc-ref inputs "bzip2") "/include") "\n  "))
                              (("external-libraries/sdsl-lite/lib")
                               (string-append
                                (lib "sdsl") "\n  "
                                (lib "htslib") "\n  "
                                (lib "boost") "\n  "
                                (lib "glog") "\n  "
                                (lib "openssl") "\n  "
                                (lib "gflags") "\n  "
                                (lib "double-conversion") "\n  "
                                (lib "bzip2")))
                              (("external-libraries/libmaus2/include") (libmaus2 "include"))
                              (("external-libraries/libmaus2/lib") (libmaus2 "lib"))))
                  (find-files "metagraph" "CMakeLists.txt")))
               #t))
           (add-before 'configure 'set-correct-directory
             (lambda _ (chdir "metagraph") #t))
           (replace 'configure
             (lambda* (#:key outputs inputs #:allow-other-keys)
               (mkdir "../build") (chdir "../build")
               (let* ((libs (map
                             (lambda (x) (string-append (assoc-ref inputs x) "/lib"))
                             '("double-conversion" "htslib" "jsoncpp" "boost" "openssl"
                               "gflags" "glog" "bzip2" "xz" "curl")))
                      (out (assoc-ref outputs "out"))
                      (args `("../metagraph" ; sourcedir
                              "-DCMAKE_BUILD_TYPE=Release"
                              ,(string-append "-DCMAKE_INSTALL_PREFIX=" out)
                              "-DCMAKE_INSTALL_LIBDIR=lib"
                              ;; enable verbose output from builds
                              "-DCMAKE_VERBOSE_MAKEFILE=ON"
                              ;; add input libraries to rpath
                              "-DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE"
                              ;; add (other) libraries of the project itself to rpath
                              ,(string-append "-DCMAKE_INSTALL_RPATH="
                                              (string-append out "/lib;" (format #f "~{~a;~}" libs)))
                              ,(format #f "-DMETALIBS=~{-L~a }" libs)
                              "-DBUILD_KMC=OFF"
                              "-DLINK_OPT=OFF"
                              "-DBUILD_STATIC=OFF")))
                      (format #t "running `cmake' with arguments ~s~%" args)
                      (apply invoke "cmake" args)))))))
      (home-page "metagraph.ethz.ch")
      (synopsis "Scalable annotated de Bruijn graphs for DNA indexing,
sequence search, alignment, and assembly.")
      (description "Scalable annotated de Bruijn graphs for DNA indexing,
sequence search, alignment, and assembly")
      (license license:gpl3))))

(define-public minigraph
  (package
    (name "minigraph")
    (version "0.11")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/lh3/minigraph")
             (commit (string-append "v" version))))
       (sha256
        (base32 "1kdijw4akk35c0f44dxzb4150wphmiyv47c2wqpk2hxx8iljfdgy"))))
    (build-system gnu-build-system)
    (inputs `(("zlib" ,zlib)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'bootstrap)
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (lambda (x) (string-append out "/bin/" x))))
               (mkdir-p (bin ""))
               (copy-file "minigraph" (bin "minigraph"))
               #t))))))
    (home-page "https://github.com/lh3/minigraph")
    (synopsis "Minigraph is a proof-of-concept sequence-to-graph mapper and
graph constructor.")
    (description "Minigraph is a proof-of-concept sequence-to-graph mapper and
graph constructor. It finds approximate locations of a query sequence in
a sequence graph and incrementally augments an existing graph with long query
subsequences diverged from the graph. The figure on the right briefly explains
the procedure.")
    (license license:expat)))


;; this one is a huge behemoth I spended more than two days trying to
;; compile. RUNPATH was tricky, which is why there is that much propagated
;; inputs; maybe it has to do with the `folly` library linking. Compilation on
;; a virtual machine needs >8GB RAM, without any more than 2 cpus, otherwise
;; the compilation process would be killed by low memory
;; availability. Compilation can be quite long (expect ~0.5 to 1H for
;; compilation only, fetching the dependencies with git clone --recursive
;; takes usually about 10 minutes.)
;;
;; FIXME this should probably use guix derived libraries rather than cloning
;; recursively all dependencies.

(define-public mmseqs2
  (package
    (name "mmseqs2")
    (version "12-113e3")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://github.com/soedinglab/MMseqs2")
                    (commit version)))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "164awhz19qda77rrjfam7qxzkj0gp0aim1la2zqi1sghs7k2yw3b"))))
    (build-system cmake-build-system)
    (native-inputs
     `(("gcc" ,gcc)))
    (inputs
     `(("xxd" ,xxd)
       ("perl" ,perl)
       ("zlib" ,zlib)))
    (propagated-inputs
     `(("gawk" ,gawk)))
    (arguments
     `(#:tests? #f))
    (home-page "https://mmseqs.com/")
    (synopsis "MMseqs2: ultra fast and sensitive search and clustering suite")
    (description "MMseqs2 (Many-against-Many sequence searching) is
a software suite to search and cluster huge protein and nucleotide
sequence sets. MMseqs2 is open source GPL-licensed software
implemented in C++ for Linux, MacOS, and (as beta version, via cygwin)
Windows. The software is designed to run on multiple cores and servers
and exhibits very good scalability. MMseqs2 can run 10000 times faster
than BLAST. At 100 times its speed it achieves almost the same
sensitivity. It can perform profile searches with the same sensitivity
as PSI-BLAST at over 400 times its speed.")
    (license license:gpl2)))

;; CONTERMINATOR -------------------------------------------------------------------

;; NOTE conterminator uses a specific mmseqs2 version, probably to
;; avoid having to update conterminator at each mmseqs2 upgrade.
(define-public conterminator
  (let ((commit "570993be7f5f31ee357183c9118bf3aa75575870")
        (revision "0"))
    (package
      (inherit mmseqs2)
      (name "conterminator")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/martin-steinegger/conterminator")
               (commit commit)))
         (sha256
          (base32 "0n53d5wc43aknppspzkn41j4hyf5w3nf6jwl74pkj27vivdsjiqq"))))
      (build-system cmake-build-system)
      (arguments
       `(#:tests? #f))
      (home-page "https://github.com/martin-steinegger/conterminator")
      (synopsis "Detection of incorrectly labeled sequences across kingdoms")
      (description "Conterminator is an efficient method for detecting
incorrectly labeled sequences across kingdoms by an exhaustive
all-against-all sequence comparison. It is a free open-source
GPLv3-licensed software for Linux and macOS, and is developed on top
of modules provided by MMseqs2.")
      (license license:gpl3))))

(define-public metaeuk
  (package
    (inherit mmseqs2)
    (name "metaeuk")
    (version "2-ddf2742")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/soedinglab/metaeuk")
             (commit "3bb93d6610f3a10b3f33268f0a3b3cfb8a35df65")))
       (sha256
        (base32 "110nyhz0cy0lyvalhcpq0g7x9h4r6sywwwf836dalxc1lpz2rc8h"))))
    (build-system cmake-build-system)
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/soedinglab/metaeuk")
    (synopsis "MetaEuk - sensitive, high-throughput gene discovery and annotation for large-scale eukaryotic metagenomics")
    (description "MetaEuk is a modular toolkit designed for
large-scale gene discovery and annotation in eukaryotic metagenomic
contigs. Metaeuk combines the fast and sensitive homology search
capabilities of MMseqs2 with a dynamic programming procedure to
recover optimal exons sets. It reduces redundancies in multiple
discoveries of the same gene and resolves conflicting gene predictions
on the same strand. MetaEuk is GPL-licensed open source software that
is implemented in C++ and available for Linux and macOS. The software
is designed to run on multiple cores.")
    (license license:gpl3)))

(define-public ntcard
  (package
    (name "ntcard")
    (version "1.2.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/bcgsc/ntCard")
             (commit version)))
       (sha256
        (base32 "0xrv2bx8ciwsylmalwa16gcy76d808dkpajqvpflmqfvwqi5v3xk"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("autoconf" ,autoconf)
       ("automake" ,automake)
       ("make" ,gnu-make)))
    (inputs
     `(("gcc" ,gcc-9)))
    (arguments
     `(#:configure-flags
       (list
        (string-append "CC=" (assoc-ref %build-inputs "gcc") "/bin/gcc")
        (string-append "CXX=" (assoc-ref %build-inputs "gcc") "/bin/g++"))
       #:tests? #f))
    (home-page "https://www.bcgsc.ca/resources/software/ntcard")
    (synopsis "Estimating k-mer coverage histogram of genomics data")
    (description "ntCard is a streaming algorithm for cardinality estimation
in genomics datasets. As input it takes file(s) in fasta, fastq, sam, or bam
formats and computes the total number of distinct k-mers, F0, and also the
k-mer coverage frequency histogram, fi, i>=1.")
    (license license:expat)))

(define-public odgi
  (let ((commit "210db8a9d9830b04fbb07d052f252459117a2388")
        (revision "1"))
    (package
      (name "odgi")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/vgteam/odgi")
               (commit commit)
               (recursive? #t)))
         (sha256
          (base32 "1ws07vkpfn5fa74y93na3r05pdwjvxa0i1ddhzrz9zw2ddxxbfv0"))))
      (build-system cmake-build-system)
      (inputs
       `(("python" ,python-wrapper)
         ("gcc" ,gcc-9)))
      (arguments
       `(#:tests?
         #f
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'generate-git-version
             (lambda _
               (mkdir-p "include")
               (with-output-to-file "include/odgi_git_version.hpp"
                 (lambda ()
                   (display (string-append "#define ODGI_GIT_VERSION \""
                                           ,(git-version "0" revision commit)
                                           "\"")))))))))
      (home-page "https://github.com/vgteam/odgi")
      (synopsis "optimized dynamic genome/graph implementation")
      (description "odgi follows the dynamic GBWT in developing a byte-packed
version of the graph and paths through it. Each node is represented by a byte
array into which we write variable length integers to represent, 1) the node
sequence, 2) its edges, and 3) the paths crossing the node.")
      (license license:expat))))


(define-public pggb
  (let ((commit "247485a485f8a39d0366b5a4ab9eecb575b9aa79")
        (revision "1"))
    (package
      (name "pggb")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/pangenome/pggb")
               (commit commit)))
         (sha256
          (base32 "0l66xr1laq08w860jyn6jx8a6vwijvg41gyfd43yfirz1rhd2c0g"))))
      (build-system copy-build-system)
      (propagated-inputs
       `(("getopt" ,util-linux)
         ("time" ,time)
         ("wfmash" ,wfmash)
         ("edyeet" ,edyeet)
         ("seqwish" ,seqwish)
         ("smoothxg" ,smoothxg)
         ("odgi" ,odgi)))
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'fix-time-path
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* '("pggb")
                 (("/usr/bin/time") (string-append (assoc-ref inputs "time") "/bin/time"))))))
         #:install-plan
         '(("pggb" "bin/"))))
      (home-page "https://github.com/pangenome/pggb")
      (synopsis "The pangenome graph builder")
      (description "This pangenome graph construction pipeline renders
a collection of sequences into a pangenome graph (in the variation graph
model). Its goal is to build a graph that is locally directed and acyclic
while preserving large-scale variation. Maintaining local linearity is
important for the interpretation, visualization, and reuse of pangenome
variation graphs.")
      (license license:expat))))

(define-public pyvolve
  (package
    (name "python-pyvolve")
    (version "1.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (pypi-uri "Pyvolve" version))
       (sha256
        (base32
         "1wwspidqc91npbja4r4is358h9dywgna03zx1hcmf3dqd32rj6b7"))))
    (build-system python-build-system)
    (propagated-inputs
     `(("python" ,python-wrapper)
       ("python-biopython" ,python-biopython)
       ("python-numpy" ,python-numpy)
       ("python-scipy" ,python-scipy)))
    (arguments `(#:tests? #f))
    (home-page
     "https://github.com/sjspielman/pyvolve")
    (synopsis
     "Sequence simulation along phylogenies according to continuous-time Markov models")
    (description
     "Sequence simulation along phylogenies according to continuous-time Markov models")
    (license license:bsd-2)))


;; FIXME won't compile for some reason. maybe it has to do with the version of gcc
;; (define-public dream-yara
;;   (let ((commit "e1bf6b5bbbeb74659379d54b9d3623660f316751")
;;         (revision "0"))
;;     (package
;;       (name "dream-yara")
;;       (version (git-version "1" revision commit))
;;       (source
;;        (origin
;;          (method git-fetch)
;;          (uri (git-reference
;;                (url "https://github.com/temehi/dream_yara")
;;                (commit commit)
;;                (recursive? #t)))
;;          (sha256 (base32 "16qy0khggj43n4h58ii51xh14rdl70svgnrxxiw9cwz7lcabmsqi"))))
;;       (build-system cmake-build-system)
;;       (native-inputs
;;        `(("gcc" ,gcc-9)))
;;       (inputs
;;        `(("zlib" ,zlib)
;;          ("bzip2" ,bzip2)))
;;       (arguments
;;        `(#:tests?
;;          #f
;;          #:configure-flags
;;          (list "-DCMAKE_BUILD_TYPE=Release")))
;;       (home-page "https://github.com/temehi/dream_yara")
;;       (synopsis "An exact read mapper for very large databases with short update time")
;;       (description "Yara is an exact tool for aligning DNA sequencing reads to
;; reference genomes. DREAM-Yara is an extension of Yara to support distributed
;; read mapping. It works by spliting a given reference database in to smaller
;; manageble partitions and this allows faster indexing and super fast updating
;; time. DREAM-Yara can quickly exclude reads for parts of the databases where
;; they cannot match. This allows us to keep the databases in several indices
;; which can be easily rebuilt if parts are updated while maintaining a fast
;; search time. Both Yara and DREAM-Yara are fully sensitive read mappers.")
;;       (license license:bsd-2))))


(define-public quast
  (package
    (name "quast")
    (version "5.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (format #f
                    "https://github.com/ablab/quast/releases/download/quast_~a/quast-~a.tar.gz"
                    version version))
       (sha256
        (base32
         "13ml8qywbb4cc7wf2x7z5mz1rjqg51ab8wkizwcg4f6c40zgif6d"))))
    (build-system python-build-system)
    (arguments
     `(#:python
       ,python-3
       #:phases
       (modify-phases %standard-phases
         (delete 'check)
         (add-after 'unpack 'patch-jsontemplate
           ;; NOTE: this is a simple hack to avoid fetching quast from
           ;; github repository (700Mb) and prefer the tar.gz from
           ;; quast releases. cgi.escape is not available in python 3.
           ;; this has been fixed since 5.0.2 release, but no release
           ;; has been made yet.
           (lambda _
             (substitute* '("quast_libs/site_packages/jsontemplate/jsontemplate.py")
               (("import cgi")
                "
try:
    from html import escape as escape_html
except:
    from cgi import escape as escape_html")
               (("cgi\\.escape") "escape_html"))
             #t))
         (add-before 'build 'replace-bundled-libs
           (lambda* (#:key inputs #:allow-other-keys)
             (let* ((get-bin (lambda (name)
                               (string-append (assoc-ref inputs name) "/bin/" name)))
                    (replace-bin (lambda (name)
                                   (let ((from (string-append "quast_libs/" name))
                                         (dir (string-append "quast_libs/" name "/bin"))
                                         (to (string-append "quast_libs/" name "/bin/" name)))
                                     (delete-file-recursively from)
                                     (mkdir-p dir)
                                     (symlink (get-bin name) to)))))
               (replace-bin "barrnap")
               (replace-bin "bedtools")
               ;; deal with sambamba: replace with installed sambamba
               (delete-file "quast_libs/sambamba/sambamba_linux")
               (symlink (get-bin "sambamba") "quast_libs/sambamba/sambamba_linux")
               ;; glimmerhmm
               (symlink (get-bin "glimmerhmm") "quast_libs/glimmer/glimmerhmm")
               ;; bwa
               (symlink (get-bin "bwa") "quast_libs/bwa/bwa")
               ;; minimap2
               (symlink (get-bin "minimap2") "quast_libs/minimap2/minimap2")
               ;; kronatools
               (delete-file-recursively "quast_libs/kronatools")
               (copy-recursively (assoc-ref inputs "kronatools") "quast_libs/kronatools")
               ;; delete genemark gene finder (closed source)
               (delete-file-recursively "quast_libs/genemark")
               (delete-file-recursively "quast_libs/genemark-es")
               ;; kmc
               ;; (delete-file-recursively "external_tools/KMC")
               ;; (mkdir-p "external_tools/KMC/linux_64/")
               ;; (symlink (string-append (assoc-ref inputs "kmc") "/bin/kmc") "external_tools/KMC/linux_64/kmc")
               ;; (symlink (string-append (assoc-ref inputs "kmc") "/bin/kmc_tools") "external_tools/KMC/linux_64/kmc_tools")
               ;; ;; blast
               ;; (delete-file-recursively "external_tools/blast")
               ;; (mkdir-p "external_tools/blast/linux_64/")
               ;; (for-each (lambda (f) (symlink (string-append (assoc-ref inputs "blast+") "/bin/" f)
               ;;                           (string-append "external_tools/blast/linux_64/" f)))
               ;;           '("blastn" "tblastn" "makeblastdb"))
               ;; ;; red
               ;; (delete-file-recursively "external_tools/red")
               ;; (mkdir-p "external_tools/red/linux_64")
               ;; (symlink (get-bin "Red") "external_tools/red/linux_64/")
               ;; TODO busco
               (delete-file "quast_libs/busco/hmmsearch")
               (symlink (string-append (assoc-ref inputs "hmmer") "/bin/hmmsearch")
                        "quast_libs/busco/hmmsearch"))
             #t)))))
    (inputs
     `(("zlib" ,zlib)
       ("gcc" ,gcc)
       ("perl" ,perl)
       ("bwa" ,bwa)
       ("barrnap" ,barrnap)           ;DONE
       ("bedtools" ,bedtools)
       ("hmmer" ,hmmer)
       ("glimmerhmm" ,glimmer-hmm)    ;DONE
       ("minimap2" ,minimap2)
       ("kronatools" ,kronatools)     ;DONE
       ("sambamba" ,sambamba)
       ("kmc" ,kmc)                   ;DONE
       ("blast+" ,blast+)
       ;; ("gridss" ,gridss)             ;WIP  TODO
       ("Red" ,red)                   ;DONE
       ("python-wrapper" ,python-wrapper)
       ("python-simplejson" ,python-simplejson)
       ("python-matplotlib" ,python-matplotlib)
       ("python-joblib" ,python-joblib)
       ("perl-time-hires" ,perl-time-hires)
       ("r-minimal" ,r-minimal)
       ("gnu-make" ,gnu-make)))
    (home-page "http://quast.sf.net/")
    (synopsis "Genome assembly evaluation tool")
    (description "QUAST stands for QUality ASsessment Tool. It
evaluates genome/metagenome assemblies by computing various
metrics.")
    (license license:gpl2)))

(define quast-full
  (package
    (name "quast-full")
    (version "5.0.2")
    (source
     (origin
       (method url-fetch)
       (uri (format #f
                    "https://github.com/ablab/quast/releases/download/quast_~a/quast-~a.tar.gz"
                    version version))
       (sha256
        (base32
         "13ml8qywbb4cc7wf2x7z5mz1rjqg51ab8wkizwcg4f6c40zgif6d"))))
    (build-system python-build-system)
    (arguments
     `(#:python
       ,python-3
       #:phases
       (modify-phases %standard-phases
         (delete 'check)
         (add-after 'unpack 'replace-bundled-libs
           (lambda* (#:key inputs #:allow-other-keys)
             (let* ((get-bin (lambda (name)
                               (string-append (assoc-ref inputs name) "/bin/" name)))
                    (replace-bin (lambda (name)
                                   (let ((from (string-append "quast_libs/" name))
                                         (dir (string-append "quast_libs/" name "bin"))
                                         (to (string-append "quast_libs/" name "/bin/" name)))
                                     (delete-file-recursively from)
                                     (mkdir-p dir)
                                     (symlink (get-bin name) to)))))
               (replace-bin "barrnap")
               (replace-bin "bedtools")
               ;; deal with sambamba: replace with installed sambamba
               (delete-file "quast_libs/sambamba/sambamba_linux")
               (symlink (get-bin "sambamba") "quast_libs/sambamba/sambamba_linux")
               ;; glimmerhmm
               (symlink (get-bin "glimmerhmm") "quast_libs/glimmer/glimmerhmm")
               ;; bwa
               (symlink (get-bin "bwa") "quast_libs/bwa/bwa")
               ;; minimap2
               (symlink (get-bin "minimap2") "quast_libs/minimap2/minimap2")
               ;; kronatools
               (delete-file-recursively "quast_libs/kronatools")
               (copy-recursively (assoc-ref inputs "kronatools") "quast_libs/kronatools")
               ;; kmc
               ;; (delete-file-recursively "external_tools/KMC")
               ;; (mkdir-p "external_tools/KMC/linux_64/")
               ;; (symlink (string-append (assoc-ref inputs "kmc") "/bin/kmc") "external_tools/KMC/linux_64/kmc")
               ;; (symlink (string-append (assoc-ref inputs "kmc") "/bin/kmc_tools") "external_tools/KMC/linux_64/kmc_tools")
               ;; ;; blast
               ;; (delete-file-recursively "external_tools/blast")
               ;; (mkdir-p "external_tools/blast/linux_64/")
               ;; (for-each (lambda (f) (symlink (string-append (assoc-ref inputs "blast+") "/bin/" f)
               ;;                           (string-append "external_tools/blast/linux_64/" f)))
               ;;           '("blastn" "tblastn" "makeblastdb"))
               ;; ;; red
               ;; (delete-file-recursively "external_tools/red")
               ;; (mkdir-p "external_tools/red/linux_64")
               ;; (symlink (get-bin "Red") "external_tools/red/linux_64/")
               ;; TODO busco
               )
             #t)))))
    (inputs
     `(("zlib" ,zlib)
       ("gcc" ,gcc)
       ("perl" ,perl)
       ("bwa" ,bwa)
       ("barrnap" ,barrnap)           ;DONE
       ("bedtools" ,bedtools)
       ;; ("busco" ,busco)               ;TODO
       ("glimmerhmm" ,glimmer-hmm)    ;DONE
       ("minimap2" ,minimap2)
       ("kronatools" ,kronatools)     ;DONE
       ("sambamba" ,sambamba)
       ("kmc" ,kmc)                   ;DONE
       ("blast+" ,blast+)
       ;; ("gridss" ,gridss)             ;WIP  TODO
       ("Red" ,red)                   ;DONE
       ("python-wrapper" ,python-wrapper)
       ("python-simplejson" ,python-simplejson)
       ("python-matplotlib" ,python-matplotlib)
       ("python-joblib" ,python-joblib)
       ("perl-time-hires" ,perl-time-hires)
       ("r-minimal" ,r-minimal)
       ("gnu-make" ,gnu-make)
       ;; ("openjdk14" ,openjdk14)
       ))
    (home-page "http://quast.sf.net/")
    (synopsis "Genome assembly evaluation tool")
    (description "QUAST stands for QUality ASsessment Tool. It
evaluates genome/metagenome assemblies by computing various
metrics.")
    (license license:gpl2)))

(define-public quicktree
  (package
    (name "quicktree")
    (version "2.5")
    (source
     (origin
       (method url-fetch)
       (uri (format #f "https://github.com/khowe/quicktree/archive/v~a.tar.gz" version))
       (sha256 (base32 "0j62275vvx7dgz2dv8yncphgqy2zyx6vbwhdpm2hc7rzrr2sh6kk"))))
    (build-system gnu-build-system)
    (inputs
     `(("gcc" ,gcc-9)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'bootstrap)
         (delete 'configure)
         (delete 'check)
         (replace 'build
           (lambda _
             (invoke "make" "all" (string-append "CC=" (which "gcc")))))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin")))
               (mkdir-p bin)
               (copy-file "quicktree" (string-append bin "/quicktree"))))))))
    (home-page "https://github.com/khowe/quicktree")
    (synopsis "Fast implementation of the neighbour-joining phylogenetic inference method")
    (description "QuickTree is an efficient implementation of the
Neighbor-Joining algorithm (PMID: 3447015), capable of reconstructing
phylogenies from huge alignments in time less than the age of the
universe.")
    (license license:asl2.0)))

(define-public raptor
  (package
    (name "raptor")
    (version "1.0.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/seqan/raptor")
             (commit (string-append "raptor-v" version))
             (recursive? #t)))
       (sha256 (base32 "0hlldha6dv3gva37dqq8xs5010gnw3p7wcrdjg4hq56gjsbzj8qn"))))
    (build-system cmake-build-system)
    (inputs
     `(("bzip2" ,bzip2)
       ("zlib" ,zlib)
       ("git" ,git)
       ("gcc" ,gcc-10)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (format #f "~a/bin" out))
                    (lib (format #f "~a/lib" out)))
               (copy-recursively "bin" bin)
               (copy-recursively "lib" lib)))))))
    (home-page "https://github.com/seqan/raptor")
    (synopsis "A fast and space-efficient pre-filter for querying very large
collections of nucleotide sequences")
    (description "A fast and space-efficient pre-filter for querying very
large collections of nucleotide sequences")
    (license license:bsd-3)))

(define-public red
  (package
    (name "red")
    (version "1.0")
    (source
     (origin (method url-fetch)
             (uri "http://toolsmith.ens.utulsa.edu/red/data/DataSet1Src.tar.gz")
             (sha256
              (base32 "0mndrayf6zz0gmjgr4zmbrrd7kdrck8c8rvja9wsvpf93nm73krk"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("gnu-make" ,gnu-make)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'bootstrap)
         (delete 'check)
         (add-after 'unpack 'replace-build-dir
           (lambda _
             (substitute* '("Makefile")
               (("\\.\\./bin") "bin")
               (("/usr/bin/g\\+\\+") "g++"))
             #t))
         (replace 'build
           (lambda _
             (invoke "make" "-B" "bin")
             (invoke "make" "-j" (number->string (parallel-job-count)))
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((bin (string-append (assoc-ref outputs "out") "/bin")))
               (mkdir-p bin)
               (copy-file "bin/Red" (string-append bin "/Red")))
             #t)))))
    (home-page "http://toolsmith.ens.utulsa.edu/")
    (synopsis "Intelligent, rapid, accurate tool for detecting repeats de-novo on the genomic scale")
    (description "Red: an intelligent, rapid, accurate tool for
detecting repeats de-novo on the genomic scale")
    (license #f)))

;; ;; FIXME: work in progress: gridss source depends on
;; ;; maven-shade-plugin, which depends on jdependency, which depends on
;; ;; asm, which is a PITA because it depends on gradle, which itself is
;; ;; a PITA to install from source.
;; (define gridss
;;   (package
;;     (name "gridss")
;;     (version "2.9.4")
;;     (source
;;      (origin
;;        (method git-fetch)
;;        (uri (git-reference
;;              (url "https://github.com/PapenfussLab/GRIDSS")
;;              (commit (string-append "v" version))))
;;        (sha256
;;         (base32 "0yhjvvdngsqdx4r23cbmj3dkz3lkczmhqnmvmgvqrknbn1kyfs8l"))))
;;     (build-system maven-build-system)
;;     (native-inputs
;;      `(("maven-shade-plugin" ,maven-shade-plugin)))
;;     (inputs
;;      `(("r-minimal" ,r-minimal)
;;        ("samtools" ,samtools)
;;        ("bwa" ,bwa)))
;;     (arguments
;;      `(#:phases
;;        (modify-phases %standard-phases
;;          (delete 'configure)
;;          (replace 'build
;;            (lambda* (#:key outputs #:allow-other-keys)
;;              (invoke "mvn" "clean" "package"
;;                      "-o"
;;                      (string-append "-Duser.home=" (getenv "HOME")))
;;              #t)))))
;;     (home-page "https://github.com/PapenfussLab/GRIDSS")
;;     (synopsis "GRIDSS: the Genomic Rearrangement IDentification Software Suite")
;;     (description "GRIDSS is a module software suite containing tools
;; useful for the detection of genomic rearrangements. GRIDSS includes
;; a genome-wide break-end assembler, as well as a structural variation
;; caller for Illumina sequencing data. GRIDSS calls variants based on
;; alignment-guided positional de Bruijn graph genome-wide break-end
;; assembly, split read, and read pair evidence.")
;;     (license license:gpl3+)))

(define-public samtools-1.10
  (package
    (inherit samtools)
    (version "1.10")
    (source (origin
              (method url-fetch)
              (uri
               (format #f "https://github.com/samtools/samtools/releases/download/~a/samtools-~a.tar.bz2"
                       version     version))
              (sha256
               (base32 "119ms0dpydw8dkh3zc4yyw9zhdzgv12px4l2kayigv31bpqcb7kv"))))
    (inputs
     `(("htslib" ,htslib-1.10)
       ("ncurses" ,ncurses)
       ("perl" ,perl)
       ("python" ,python)
       ("zlib" ,zlib)))
    (home-page "https://www.htslib.org/")))

(define-public samtools-dedup
  (package
    (name "samtools-dedup")
    (version "1.1")
    (source (origin
              (method git-fetch)
              (uri (git-reference
                    (url "https://gitlab.com/hrz-project/samtools-dedup")
                    (commit "554fdef274420e798b431aa44260dc7011ce6ddc")))
              (file-name (git-file-name name version))
              (sha256
               (base32
                "0k0as36g54vab492yxg8j5j9apsdrbnphxzrhwz742qgnnxgx3ax"))))
    (build-system copy-build-system)
    (native-inputs
     `(("make" ,gnu-make)))
    (propagated-inputs
     `(("perl" ,perl)
       ,@horizon-perl-deps
       ("samtools" ,samtools-1.10)))
    (arguments
     `(#:install-plan
       '(("bin/samtools-dedup.pl" "bin/samtools-dedup"))))
    (home-page "https://gitlab.com/hrz-project/samtools-dedup")
    (synopsis "Deduplicate reads based on samtools version 1.10")
    (description "samtools-dedup mark duplicates reads based on read
mapping coordinates")
    (license license:gpl3)))

(define-public sepp
  (package
    (name "sepp")
    (version "4.3.10")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/smirarab/sepp")
             (commit "5e961bee2f3fcdd85f84aec2bc3f2873253d9449")))
       (sha256
        (base32 "0crzvc5fd0rcsz87cfsjcr34kq8pmp4c73xykvhzvqkqc1hg0ivm"))))
    (native-inputs
     `(("java" ,icedtea)
       ("python-setuptools" ,python-setuptools)))
    (inputs
     `(("python-wrapper" ,python-wrapper)
       ("python-dendrody" ,python-dendropy)
       ;; will skip this step for now as pplacer is kinda difficult to
       ;; build on guix, because of ocaml requirements
       ;;
       ;; ("pplacer" ,pplacer)
       ;; ("guppy" ,pplacer)
       ("hmmsearch" ,hmmer)
       ("hmmalign" ,hmmer)
       ("hmmbuild" ,hmmer)))
    (build-system python-build-system)

    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-before 'build 'sepp-dont-use-home
           (lambda _
             (invoke "python" "setup.py" "config" "-c")
             #t))
         (add-after 'install 'copy-bundled-libs-and-config
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (tgt (string-append out "/.sepp"))
                    (pth (string-append out "/lib/python3.8/site-packages/home.path")))
               (mkdir-p tgt)
               (copy-recursively ".sepp" tgt)
               (with-output-to-file pth
                 (lambda () (display (string-append out "/.sepp"))))
               (copy-file "default.main.config" (string-append out "/.sepp/main.config")))
             #t)))
       ;; UPDATE: was going to replace the hmmer binaries with guix
       ;; derived ones, but issue #76 on github says that sepp needs some
       ;; specific hmmer version because hmmer changed some output
       ;; formating ... leaving it as it is for now.
       ;;
       ;; (add-before 'build 'replace-bundled-libs
       ;;   ;; replace hmmer binaries with guix-derived hmmer binaries
       ;;   (lambda* (#:key outputs inputs #:allow-other-keys)
       ;;     (for-each
       ;;      (lambda (ibin)
       ;;        (let ((path (string-append
       ;;                     (assoc-ref inputs ibin)
       ;;                     "/bin/" ibin))
       ;;              (tgt (string-append "tools/bundled/Linux/" ibin)))
       ;;          (delete-file tgt)
       ;;          (symlink path tgt)))
       ;;      '("hmmsearch" "hmmalign" "hmmbuild"))
       ;;     #t)))
       ))
    (home-page "https://github.com/smirarab/sepp")
    (synopsis "Ensemble of HMM methods (SEPP, TIPP, UPP)")
    (description "This repository includes code for SEPP, TIPP, UPP,
HIPPI. The three methods use ensembles of Hidden Markov Models (HMMs)
in different ways, each focusing on a different problem.")
    (license license:gpl3+)))

(define-public seqwish
  (let ((commit "9bbfa70145e79777fc12cff6d0bad65d545f39fd")
        (revision "0"))
    (package
      (name "seqwish")
      (version "0.6")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/ekg/seqwish")
               (commit commit)
               (recursive? #t)))
         (sha256
          (base32 "1ifi3n3xcbsq18w36b1x03scl3z2nrn1hyp1w28908cdm2v0570z"))))
      (build-system cmake-build-system)
      (inputs
       `(("gcc-9" ,gcc-9)
         ("zlib" ,zlib)))
      (arguments
       `(#:tests? #f))
      (home-page "https://github.com/ekg/seqwish")
      (synopsis "alignment to variation graph inducer")
      (description "seqwish implements a lossless conversion from pairwise
alignments between sequences to a variation graph encoding the sequences and
their alignments. As input we typically take all-versus-all alignments, but
the exact structure of the alignment set may be defined in an application
specific way. This algorithm uses a series of disk-backed sorts and passes
over the alignment and sequence inputs to allow the graph to be constructed
from very large inputs that are commonly encountered when working with large
numbers of noisy input sequences. Memory usage during construction and
traversal is limited by the use of sorted disk-backed arrays and succinct
rank/select dictionaries to record a queryable version of the graph.")
      (license license:expat))))

(define-public smoothxg
  (let ((commit "0a5afb269680a99977fcbba7e22c666851367ffc")
        (revision "2"))
    (package
      (name "smoothxg")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/ekg/smoothxg")
               (commit commit)
               (recursive? #t)))
         (sha256
          (base32 "0axvizh2xs52hq5g0p5px8v3l8ga4qmw57z5f21n1yjzba9fjiqm"))))
      (build-system cmake-build-system)
      (inputs
       `(("python" ,python-wrapper)
         ("gcc" ,gcc-9)
         ("zlib" ,zlib)))
      (arguments
       `(#:make-flags (list (string-append "CC=" (assoc-ref %build-inputs "gcc") "/bin/gcc")
                                 (string-append "CXX=" (assoc-ref %build-inputs "gcc") "/bin/g++"))
         #:tests? #f))
      (home-page "https://github.com/ekg/smoothxg")
      (synopsis "linearize and simplify variation graphs using blocked partial
order alignment")
      (description "Pangenome graphs built from raw sets of alignments may
have complex local structures generated by common patterns of genome
variation. These local nonlinearities can introduce difficulty in downstream
analyses, visualization, and interpretation of variation graphs.

smoothxg finds blocks of paths that are collinear within a variation graph. It
applies partial order alignment to each block, yielding an acyclic variation
graph. Then, to yield a @emph{smoothed} graph, it walks the original paths to lace
these subgraphs together. The resulting graph only contains cyclic or
inverting structures larger than the chosen block size, and is otherwise
manifold linear. In addition to providing a linear structure to the graph,
smoothxg can be used to extract the consensus pangenome graph by applying the
heaviest bundle algorithm to each chain.

To find blocks, smoothxg applies a greedy algorithm that assumes that the
graph nodes are sorted according to their occurence in the graph's embedded
paths. The path-guided stochastic gradient descent based 1D sort implemented
in odgi sort -Y is designed to provide this kind of sort.")
      (license license:expat))))

(define reindeer
  ;; FIXME same as gatb-core; probably not the good way to have git
  ;; clone recursively, especially regarding the huuuge gatb-core
  ;; library
  (package
    (name "reindeer")
    (version "1.0.2")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/kamimrcht/REINDEER")
             (commit "7b957ae41130095dc7f5057c176a48602a9b369b")))
       (sha256
        (base32
         "0lzmbgyxf82ag7857539jlxm3kl3w8a9zzp2qpcvf6x8p1ygnidf"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("cmake" ,cmake)
       ("blight-src" ,(package-source blight))))
    (inputs
     `(("zlib" ,zlib)
       ("bcalm" ,bcalm)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'replace-sources
           (lambda* (#:key inputs #:allow-other-keys)
             (delete-file-recursively "blight")
             (copy-recursively (assoc-ref inputs "blight-src") "./blight")
             #t))
         (add-after 'unpack 'fix-bcalm-path
           (lambda* (#:key inputs #:allow-other-keys)
             (let ((bcalm-bin (string-append (assoc-ref inputs "bcalm") "/bin/bcalm")))
               ;; fix number of parallel build process
               (substitute* "src/launch_bcalm.cpp"
                 (("./bin/bcalm") bcalm-bin))
               #t)))
         (delete 'bootstrap)
         (delete 'configure)
         (replace 'build
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((nproc (number->string (parallel-job-count))))
               (mkdir-p "bin")
               (invoke "make -j" nproc))
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (install-file "Reindeer" (string-append (assoc-ref outputs "out") "/bin"))
             #t)))))
    (synopsis "Efficient indexing of k-mer presence and abundance in sequencing datasets")
    (description "REINDEER builds a data-structure that indexes k-mers
and their abundances in a collection of datasets (raw RNA-seq or
metagenomic reads for instance). Then, a sequence (FASTA) can be
queried for its presence and abundance in each indexed dataset. While
other tools (e.g. SBT, BIGSI) were also designed for large-scale k-mer
presence/absence queries, retrieving abundances was so far
unsupported (except for single datasets, e.g. using some k-mer
counters like KMC, Jellyfish). REINDEER combines fast queries, small
index size, and low memory footprint during indexing and queries. We
showed it allows to index 2585 RNA-seq datasets (~4 billions k-mers)
using less than 60GB of RAM and a final index size lower than 60GB on
the disk. Then, a REINDEER index can either be queried on
disk (experimental feature, low RAM usage) or be loaded in RAM for
faster queries.")
    (home-page "https://github.com/kamimrcht/REINDEER")
    (license license:agpl3+)))

(define-public twopaco
  (package
    (name "twopaco")
    (version "0.9.4")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/medvedevgroup/TwoPaCo/")
             (commit version)))
       (sha256
        (base32 "1z6c1ryxy4z27xfbg1lrv9hfrpj4s11dkhv0m8c785msvfkrhf6i"))))
    (build-system cmake-build-system)
    (inputs
     `(("tbb" ,tbb)
       ("gcc" ,gcc-9)))
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'set-src-dir
           (lambda _ (chdir "src") #t)))))
    (home-page "https://github.com/medvedevgroup/TwoPaCo/")
    (synopsis "A fast constructor of the compressed de Bruijn graph from many genomes")
    (description "It is an implementation of the algorithm described
in the paper

@quotation
TwoPaCo: An efficient algorithm to build the compacted de
Bruijn graph from many complete genomes
@end quotation")
    (license license:bsd-3)))

(define-public vcf-slim
  (let ((commit "238616adb0b745207d97d58d511f012459c007b9")
        (revision "0"))
    (package
      (name "vcf-slim")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/noporpoise/vcf-slim")
               (commit commit)))
         (sha256
          (base32 "0ah92d12y7bix31mfadzphpsjkayyrrdky4dbm48razw315psm60"))))
      (build-system gnu-build-system)
      (inputs
       `(("zlib" ,zlib)
         ("xz" ,xz)
         ("bzip2" ,bzip2)
         ("curl" ,curl)
         ("libcrypto" ,openssl)
         ("htslib" ,htslib)))
      (arguments
       `(#:tests?
         #f
         #:phases
         (modify-phases %standard-phases
           (delete 'bootstrap)
           (delete 'configure)
           (add-after 'unpack 'fix-cc-compiler
             (lambda _ (setenv "CC" (which "gcc")) #t))
           (add-after 'unpack 'fix-htslib-path
             (lambda* (#:key inputs #:allow-other-keys)
               (substitute* '("Makefile")
                 (("\\$\\(HTSLIB\\)\\/htslib")
                  (string-append (assoc-ref inputs "htslib") "/include/htslib"))
                 (("LIBS=-lz -lm -lpthread") "LIBS=-lz -lm -lpthread -lcurl -llzma -lcrypto -lbz2"))
               #t))
           (replace 'build
             (lambda* (#:key inputs #:allow-other-keys)
               (invoke "make"
                       (string-append "HTSLIB=" (assoc-ref inputs "htslib") "/lib/")
                       "all")

               #t))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin")))
                 (mkdir-p bin)
                 (copy-recursively "bin" bin)
                 (copy-recursively "script" bin))
               #t)))))
      (home-page "https://github.com/noporpoise/vcf-slim")
      (synopsis "Light weight VCF annotation and filtering")
      (description "Light weight VCF annotation and filtering")
      (license license:expat))))

(define-public wfa
  (let ((commit "61a93bb3f1845e0409b04a7e178e5c6226f68f45")
        (revision "0"))
    (package
      (name "wfa")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/smarco/WFA")
               (commit commit)))
         (sha256
          (base32 "1c6wry8shxgs51mw4dlhhsryhyvvl31g7d4dl2340rzdlr3p8y1x"))))
      (build-system gnu-build-system)
      (arguments
       `(#:tests?
         #f
         #:phases
         (modify-phases %standard-phases
           (delete 'bootstrap)
           (delete 'configure)
           (add-before 'build 'suppress-benchmark-compilation
             (lambda _
               (substitute* '("Makefile")
                 (("SUBDIRS=benchmark") "SUBDIRS=")
                 (("all: \\$\\(SUBDIRS\\) tools ") "all: $(SUBDIRS) "))
               (delete-file-recursively "benchmark")
               #t))
           ;; (replace 'build
           ;;   (lambda _
           ;;     (invoke "make" "-j" (number->string (parallel-job-count))
           ;;             "build/libwfa.a")))
           (replace 'install
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (inc (lambda (x) (string-append out "/include/" x)))
                      (lib (lambda (x) (string-append out "/lib/" x))))
                 (mkdir-p (lib ""))
                 (mkdir-p (inc ""))
                 (copy-file "build/libwfa.a" (lib "libwfa.a"))
                 (for-each
                  (lambda (dir)
                    (mkdir-p (inc dir))
                    (for-each
                     (lambda (file) (copy-file file (inc (string-append dir "/" (basename file)))))
                     (find-files dir "\\.h$")))
                  '("edit" "gap_affine" "gap_lineal" "system" "utils")))
               #t)))))
      (home-page "https://github.com/smarco/WFA")
      (synopsis "Wavefront alignment algorithm (WFA): Fast and exact
gap-affine pairwise alignment")
      (description "The wavefront alignment (WFA) algorithm is an exact
gap-affine algorithm that takes advantage of homologous regions
between the sequences to accelerate the alignment process. As opposed
to traditional dynamic programming algorithms that run in quadratic
time, the WFA runs in time O(ns), proportional to the read length
n and the alignment score s, using O(s^2) memory. Moreover, the WFA
exhibits simple data dependencies that can be easily vectorized, even
by the automatic features of modern compilers, for different
architectures, without the need to adapt the code. This library
implements the WFA and the WFA-Adapt algorithms for gap-affine
penalties. It also provides support functions to display and verify
the results.")
      (license license:expat))))

(define-public wfmash
  (let ((commit "5c6ae5d37f82f7e5a96e7d1c4d56cfc0124c9477")
        (revision "1"))
    (package
      (name "wfmash")
      (version (git-version "0" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/ekg/wfmash")
               (commit commit)))
         (sha256
          (base32 "1n84k1a8hzcrk02nyx2dh2wamanhcrs74g6ibm0yqfwm0qslsj23"))))
      (build-system gnu-build-system)
      (native-inputs
       `(("autoconf" ,autoconf)
         ("sed" ,sed)))
      (inputs
       `(("zlib" ,zlib)
         ("gsl" ,gsl)
         ("perl" ,perl)))
      (arguments
       `(#:tests?
         #f
         #:phases
         (modify-phases %standard-phases
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (lambda (x) (string-append out "/bin/" x))))
                 (mkdir-p (bin ""))
                 (copy-file "wfmash" (bin "wfmash")))
               #t)))))
      (home-page "https://github.com/ekg/wfmash")
      (synopsis "base-accurate DNA sequence alignments using WFA and mashmap2")
      (description "A DNA sequence read mapper based on mash distances
and the wavefront alignment algorithm. wfmash is a fork of MashMap
that implements base-level alignment using the wavefront alignment
algorithm WFA. It completes an alignment module in MashMap and extends
it to enable multithreaded operation. A single command-line interface
simplfies usage. The PAF output format is harmonized and made
equivalent to that in minimap2, and has been validated as input to
seqwish. wfmash is also a fork of edyeet, which uses edlib to obtain
an edit-distance based alignment, which is fast but may not be
appropriate for many biological applications.")
      (license license:public-domain))))

(define-public rust-overlaps
  (let ((commit "57cc460ecd2d182bd8c212ddad374d8916791032")
        (revision "0"))
    (package
      (name "rust-overlaps")
      (home-page "https://github.com/jbaaijens/rust-overlaps")
      (version (git-version "0.1.1" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url home-page)
                      (commit commit)))
                (sha256
                 (base32 "136vkvwszpg4mlr63khlmq83kljcrzbfg0wvxl1ndy9k8cxhvf15"))))
      (build-system cargo-build-system)
      (arguments
       `(#:tests?
         #f                             ; some permission aren't granted by
                                        ; chrooted package build
         #:cargo-inputs
         (("rust-bio" ,rust-bio-0.13)
          ("rust-rand" ,rust-rand-0.7)
          ("rust-clap" ,rust-clap-2)
          ("rust-bidir-map" ,rust-bidir-map-0.3.2)
          ("rust-csv" ,rust-csv-1.1)
          ("rust-cue" ,rust-cue-0.1.0)
          ("rust-num-cpus" ,rust-num-cpus-1)
          ("rust-stacker" ,rust-stacker-0.1))))
      (synopsis "approximate suffix-prefix overlap problem solver")
      (description synopsis)
      (license license:expat))))

(define-public quick-cliques
  (let ((commit "7f7d0b7d1534a0fefae8e710a8deb9486f597149")
        (revision "0"))
    (package
      (name "quick-cliques")
      (home-page "https://github.com/darrenstrash/quick-cliques")
      (version (git-version "1" revision commit))
      (source
       (origin (method git-fetch)
               (uri (git-reference
                     (url home-page)
                     (commit commit)))
               (sha256 (base32 "1da1d1l6qkmm32xc3maq5rckvbfadafmrwmrskygb9kirfhjb7k4"))))
      (build-system gnu-build-system)
      (inputs
       `(("g++" ,gcc-9)))
      (arguments
       `(#:make-flags
         (list (string-append "CXX=" (assoc-ref %build-inputs "g++") "/bin/g++"))
         #:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin")))
                 (install-file "bin/qc" bin))))
           (delete 'check))))
      (synopsis "Quickly compute all maximal cliques of a graph")
      (description synopsis)
      (license license:gpl3))))

(define-public haplo-conduct
  (let ((commit "24e4d9fc35ef6a93e336949a9a8f98ed4ceef362")
        (revision "0"))
    (package
      (name "haplo-conduct")
      (home-page "https://github.com/HaploConduct/HaploConduct")
      (version (git-version "0.2.1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url home-page)
               (commit commit)))
         (sha256
          (base32 "03ndcid7lzssc5ybsr2frhi72yysjjl5lqhnaviyqh0hng0162a8"))))
      (build-system gnu-build-system)
      (native-inputs
       `(("sed" ,sed)))
      (inputs
       `(("gcc" ,gcc-9)
         ("boost" ,boost)
         ("quick-cliques" ,quick-cliques)))
      (propagated-inputs
       `(("rust-overlaps" ,rust-overlaps)
         ("bwa" ,bwa)
         ("samtools" ,samtools)
         ("python" ,python-2)
         ("python3" ,python-3)
         ("python-scipy" ,python2-scipy)))
      (arguments
       `(#:make-flags
         (list (string-append "CC=" (assoc-ref %build-inputs "gcc") "/bin/g++"))
         #:phases
         (modify-phases %standard-phases
           (add-after 'unpack 'delete-quick-cliques
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (delete-file-recursively "quick-cliques")
               (invoke "sed" "-i" (string-append "84s|base_path|" "'" (assoc-ref outputs "out") "'|") "savage.py")
               (invoke "sed" "-i" (string-append "46s|base_path|" "'" (assoc-ref outputs "out") "'|") "haploconduct.py")
               (invoke "sed" "-i" (string-append "85s|base_path|" "'" (assoc-ref outputs "out") "'|") "polyte.py")
               (invoke "sed" "-i" (string-append "678s|selfpath|" "'" (assoc-ref outputs "out") "'|") "polyte.py")
               (invoke "sed" "-i" (string-append "86s|base_path|" "'" (assoc-ref outputs "out") "'|") "polyte-split.py")
               (invoke "sed" "-i" (string-append "22s|selfpath|" "'" (assoc-ref outputs "out") "'|") "scripts/pipeline_per_stage.py")
               (invoke "sed" "-i" "20d" "makefile")
               (invoke "sed" "-i" (string-append "400s|program_settings.base_path \\+ \"/quick-cliques/" "|\"" (assoc-ref inputs "quick-cliques") "|") "src/ViralQuasispecies.cpp")
               (invoke "sed" "-i" (string-append "115s|" "base_path \\+ \"/quick-cliques" "|\"" (assoc-ref inputs "quick-cliques") "|") "estimate_strain_count.py")
               #t))
           (delete 'configure)
           (delete 'check)
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin"))
                      (inst! (lambda (x) (install-file x bin))))
                 (for-each inst! '("bin/ViralQuasispecies" "haploconduct.py"
                                   "polyte.py" "polyte-split.py" "savage.py"))))))))
      (synopsis "Haplotype-aware genome assembly toolkit")
      (description "HaploConduct is a package designed for reconstruction of
individual haplotypes from next generation sequencing data, in particular
Illumina. Currently, HaploConduct consists of two methods: SAVAGE and
POLYTE.")
      (license license:gpl3))))
