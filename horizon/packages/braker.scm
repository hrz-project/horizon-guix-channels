(define-module (horizon packages braker)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((guix build utils) #:hide (delete which))
  #:use-module (guix build-system copy)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system perl)
  #:use-module (guix build-system python)
  #:use-module (guix build-system r)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  ;;
  #:use-module (gnu packages base)
  #:use-module (gnu packages bioinformatics)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages databases)
  #:use-module (gnu packages image)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages regex)
  #:use-module (gnu packages rsync)
  #:use-module (gnu packages ruby)
  #:use-module (gnu packages shells)
  #:use-module (gnu packages sqlite)
  #:use-module (gnu packages tcl)
  #:use-module (gnu packages tls)
  #:use-module (gnu packages xml)
  ;;
  #:use-module (horizon packages bioinformatics)
  #:use-module (horizon packages perl)
  #:use-module (horizon packages lua))

(define-public kent-utils
  (package
    (name "kent-utils")
    (version "404")                     ;lol
    (source
     (origin
       (method url-fetch)
       (uri (format #f "http://hgdownload.soe.ucsc.edu/admin/exe/userApps.v~a.src.tgz" version))
       (sha256 (base32 "1vfh89d2gwzs71x0k1w8ssxmwvgrzynj6ja81a3pd2v9xlkb60kp"))))
    (build-system gnu-build-system)
    (native-inputs
     `(("rsync" ,rsync)))
    (inputs
     `(("mysql" ,mysql)
       ("libpng" ,libpng)
       ("libssl" ,openssl)
       ("perl" ,perl)
       ("python" ,python-3)
       ("python2" ,python-2.7)
       ("tcsh" ,tcsh)
       ("tclsh" ,tcl)
       ("libuuid" ,util-linux "lib")
       ("gcc" ,gcc-9)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'bootstrap)
         (delete 'configure)
         (delete 'check)
         (add-before 'build 'set-cc
           (lambda _ (setenv "CC" (which "gcc")) #t))
         (add-before 'patch-source-shebangs 'patch-perl5
           (lambda _
             (let ((perl5-scripts (find-files "kent" "\\.pl$")))
               (substitute* `(,@perl5-scripts)
                 (("/usr/local/bin/perl5") (which "perl"))))
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin")))
               (mkdir-p bin)
               (copy-recursively "bin" bin))
             #t)))))
    (home-page "http://hgdownload.soe.ucsc.edu/admin/exe/")
    (synopsis "UCSC genome browser 'kent' bioinformatic utilities")
    (description "These are only the command line bioinformatic utilities
from the kent source tree.")
    (license #f)))

(define-public augustus
  (package
    (name "augustus")
    (version "3.3.3")
    (source
     (origin
       (method url-fetch)
       (uri (format #f "http://bioinf.uni-greifswald.de/augustus/binaries/augustus-~a.tar.gz" version))
       (sha256 (base32 "0h3idgk5nx9wsfsazy3pharq607qpv4sgfiyhmzqp2mifhhd7i2c"))))
    (outputs '("out" "scripts"))
    (build-system gnu-build-system)
    (inputs
     `(("python" ,python-3)
       ("perl" ,perl)
       ("htslib" ,htslib)
       ("samtools" ,samtools)
       ("bcftools" ,bcftools)
       ("bamtools" ,bamtools)
       ("mysql" ,mysql)
       ("boost" ,boost)
       ("zlib" ,zlib)
       ("sqlite3" ,sqlite)
       ("xz" ,xz)
       ("bzip2" ,bzip2)
       ("curl" ,curl)
       ("openssl" ,openssl)
       ("gsl" ,gsl)
       ("curses" ,ncurses)
       ("suitesparse" ,suitesparse)
       ("lpsolve" ,lpsolve)
       ("gcc" ,gcc-9)))
    (propagated-inputs
     `(("kent-utils" ,kent-utils)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (delete 'check)
         (add-after 'unpack 'delete-doc-and-bin
           ;; doc is >100Mb; bin will be recompiled
           (lambda _ (for-each delete-file-recursively '("docs" "examples" "bin"))))
         (add-after 'unpack 'chmod-scripts
           (lambda _
             (define (unexe x) (chmod (string-append "scripts/" x) #o664))
             (for-each unexe '("gbrowse.conf" "gff2ps_mycustom" "helpMod.pm" "SplicedAlignment.pm"))
             #t))
         (add-after 'unpack 'enable-compgenpred-sqlite
           (lambda _
             (substitute* '("common.mk")
               (("^#COMPGENEPRED") "COMPGENEPRED")
               (("^#SQLITE") "SQLITE"))
             #t))
         (add-after 'unpack 'fix-inputs-path
           (lambda* (#:key inputs #:allow-other-keys)
             (define (inp in dir)
               (apply string-append `(,(assoc-ref inputs in) ,dir)))
             (define (inc x) (string-append "-I" x))
             (define (link x) (string-append "-L" x))
             (for-each
              (lambda (file)
                (substitute* `(,file)
                  (("g\\+\\+") (which "g++"))
                  (("/usr/include/bamtools") (inp "bamtools" "/include/bamtools"))
                  (("/usr/include/lpsolve") (inp "lpsolve" "/include/lpsolve"))
                  (("/usr/include/mysql") (inp "mysql" "/include"))
                  (("/usr/include/samtools") (inp "samtools" "/include/samtools"))
                  (("/usr/include/boost") (inp "boost" "/include/boost"))
                  (("-DSQLITE") (format #f "~a -DSQLITE" (link (inp "sqlite3" "/lib"))))
                  (("-I\\$\\(HTSLIB\\)") (inc (inp "htslib" "/include/htslib")))
                  (("-I\\$\\(SAMTOOLS\\)") (inc (inp "samtools" "/include/samtools")))
                  (("-I\\$\\(BCFTOOLS\\)") "")
                  (("\\$\\(HTSLIB\\)/libhts.a") (inp "htslib" "/lib/libhts.a"))
                  (("\\$\\(SAMTOOLS\\)/libbam.a") (inp "samtools" "/lib/libbam.a"))
                  (("-lcurses") (format #f "~a ~a" (link (inp "curses" "/lib")) "-lncurses"))))
              (append
               (find-files "auxprogs/utrrnaseq" "\\.mk$")
               (find-files "." "[Mm]akefile")))
             #t))
         (add-before 'build 'set-cc
           (lambda _ (setenv "CC" (which "gcc")) #t))
         (replace 'build
           (lambda _
             (invoke "make" "all")
             (with-directory-excursion "auxprogs/aln2wig"
               (invoke "make"))
             (copy-file "auxprogs/utrrnaseq/Debug/utrrnaseq" "bin/utrrnaseq")
             #t))
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((bin (lambda (x dir) (string-append (assoc-ref outputs x) dir))))
               (mkdir-p (bin "out" "/bin"))
               (mkdir-p (bin "out" "/config"))
               (mkdir-p (bin "scripts" "/bin"))
               (copy-recursively "bin" (bin "out" "/bin"))
               (copy-recursively "config" (bin "out" "/config"))
               (copy-recursively "scripts" (bin "scripts" "/bin")))
             #t))
         (add-after 'install 'wrap-scripts
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((scripts (find-files (string-append (assoc-ref outputs "scripts") "/bin")))
                   (config (string-append (assoc-ref outputs "out") "/config")))
               (define (wrap file)
                 (when (and (executable-file? file)
                            (not (string-suffix? "pm" file)))
                   (wrap-program file `("AUGUSTUS_CONFIG_PATH" "" = (,file ,config)))))
               (for-each wrap scripts))
             #t)))))
    (home-page "http://bioinf.uni-greifswald.de/augustus/")
    (synopsis "AUGUSTUS is a program that predicts genes in eukaryotic genomic sequences.")
    (description "AUGUSTUS is a gene prediction program written by Mario
Stanke, Oliver Keller, Stefanie König, Lizzy Gerischer and Katharina Hoff. It
can be used as an ab initio program, which means it bases its prediction
purely on the sequence. AUGUSTUS may also incorporate hints on the gene
structure coming from extrinsic sources such as EST, MS/MS, protein alignments
and syntenic genomic alignments. Since version 3.0 AUGUSTUS can also predict
the genes simultaneously in several aligned genomes.")
    (license license:artistic2.0)))


(define-public spaln
  (package
    (name "spaln")
    (version "2.4.03")
    (source
     (origin
       (method url-fetch)
       (uri (format #f "https://github.com/ogotoh/spaln/archive/Ver.~a.tar.gz" version))
       (sha256 (base32 "08sb6qhdrm7s1knahjsf5gwk7vfn0n9xvbxv24bpp0l54hwijzbb"))))
    (build-system gnu-build-system)
    (inputs
     `(("gcc" ,gcc-9)
       ("zlib" ,zlib)
       ("perl" ,perl)))
    (outputs '("out" "scripts"))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'bootstrap)
         (delete 'check)
         (add-before 'configure 'set-dir (lambda _ (chdir "src") #t))
         (replace 'configure
           (lambda* (#:key outputs #:allow-other-keys)
             (setenv "CXX" (which "g++"))
             (setenv "exec_prefix"  (string-append (assoc-ref outputs "out") "/bin"))
             (setenv "alndbs_dir" (string-append (assoc-ref outputs "out") "/seqdb"))
             (setenv "table_dir" (string-append (assoc-ref outputs "out") "/table"))
             (invoke "./configure" "--use_zlib=1")))
         (add-after 'install 'copy-scripts
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((scripts (assoc-ref outputs "scripts"))
                    (bin (string-append scripts "/bin")))
               (mkdir-p bin)
               (chdir "..")
               (copy-recursively "perl" bin)
               #t))))))
    (home-page "https://github.com/ogotoh/spaln/")
    (synopsis "Genome mapping and spliced alignment of cDNA or amino acid sequences")
    (description "Spaln (space-efficient spliced alignment) is a stand-alone
program that maps and aligns a set of cDNA or protein sequences onto a whole
genomic sequence in a single job. Spaln also performs spliced or ordinary
alignment after rapid similarity search against a protein sequence database,
if a genomic segment or an amino acid sequence is given as a query. From
Version 1.4, spaln supports a combination of protein sequence database and
a given genomic segment. From Version 2.2, spaln also performs rapid
similarity search and (semi-)global alignment of a set of protein sequence
queries against a protein sequence database.")
    (license license:gpl2)))

(define-public spaln-boundary-scorer
  (let ((commit "d42373657b486641c98b5b6a4d1e328d5e25868b")
        (revision "0"))
    (package
      (name "spaln-boundary-scorer")
      (version (git-version "1" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/gatech-genemark/spaln-boundary-scorer")
               (commit commit)))
         (sha256 (base32 "0h17cz7rbrw0bw3vf5xnzp1z38r86wq2y5x7zgjvygchv45zwpxg"))))
      (build-system gnu-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (delete 'bootstrap)
           (delete 'check)
           (add-after 'unpack 'delete-example-files
             (lambda _ (delete-file-recursively "test/test_files") #t))
           (replace 'build
             (lambda _
               (invoke
                "make" "all" (string-append "CC=" (which "g++")))
               #t))
           (replace 'install
             (lambda* (#:key outputs #:allow-other-keys)
               (let* ((out (assoc-ref outputs "out"))
                      (bin (string-append out "/bin")))
                 (mkdir-p bin)
                 (copy-file "spaln_boundary_scorer" (string-append bin "/spaln_boundary_scorer"))))))))
      (home-page "https://github.com/gatech-genemark/spaln-boundary-scorer")
      (synopsis "Spaln boundary scorer parses introns, starts, stops and exons from Spaln's alignment output and scores them based on local alignment quality")
      (description "Spaln boundary scorer parses introns, starts, stops and
exons from Spaln's alignment output and scores them. Introns, starts and stops
are scored based on local alignment quality around their boundaries. Detailed
description of how the scores are computed is available in
https://academic.oup.com/nargab/article/2/2/lqaa026/5836691.")
      (license #f))))

;; (define-public genemark-hmm-euk
;;   (package
;;     (name "genemark-hmm-euk")
;;     (version "1.0")
;;     (source
;;      (origin
;;        (method url-fetch)
;;        (uri "http://topaz.gatech.edu/GeneMark/tmp/GMtool_hCL2o/genemark_hmm_euk_linux_64.tar.gz")
;;        (sha256 (base32 "17waq3i2zcs7q8083142j6ijqbnl108zsgmbbc9z75ryq6v2gzsa"))))
;;     ))

(define-public genemark-es
  (package
    (name "genemark-es")
    (version "4.61")
    (source
     (origin
       (method url-fetch)
       ;; 24 MiB
       (uri "http://topaz.gatech.edu/GeneMark/tmp/GMtool_gSPFB/gmes_linux_64.tar.gz")
       (sha256 (base32 "029py72n9ymyrp2xwcxmhsiynxcdffmpplj6afhk5sgb9s49b6kd"))))
    (build-system perl-build-system)
    (propagated-inputs
     `(("perl" ,perl)
       ("python" ,python-wrapper)
       ("perl-yaml" ,perl-yaml)
       ("perl-hash-merge" ,perl-hash-merge)
       ("perl-logger-simple" ,perl-logger-simple)
       ("perl-parallel-forkmanager" ,perl-parallel-forkmanager)
       ("perl-mce" ,perl-mce)))
    (arguments
     `(#:phases
       (modify-phases %standard-phases
         (delete 'bootstrap)
         (delete 'check)
         (delete 'build)
         (delete 'configure)
         (replace 'install
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (heu (string-append out "/heu_dir"))
                    (bin (string-append out "/bin"))
                    (lib (string-append bin "/lib"))
                    (share (string-append out "/share")))
               (for-each mkdir-p (list bin share lib heu))
               (for-each
                (lambda (file) (copy-file file (string-append bin "/" (basename file))))
                (append (find-files "." "\\.pl$")
                        (find-files "." "\\.py$")
                        `("check_install.bash"
                          "Gibbs3"
                          "gmhmme3"
                          "probuild"
                          ,@(find-files "other"))))
               (copy-recursively "lib" lib)
               (copy-recursively "heu_dir" heu)
               (copy-file (car (find-files "." "gm_key")) (string-append out "/share/gm_key")))))
         (add-after 'install 'patch-and-copy-config
           (lambda* (#:key outputs #:allow-other-keys)
             (substitute* '("gmes.cfg")
               (("heu_dir:  heu_dir") (string-append "heu_dir: " (assoc-ref outputs "out") "/heu_dir")))
             (copy-file "gmes.cfg" (string-append (assoc-ref outputs "out") "/share/gmes.cfg"))
             #t))
         (add-after 'install 'create-helper-scripts
           (lambda* (#:key outputs #:allow-other-keys)
             (let* ((out (assoc-ref outputs "out"))
                    (bin (string-append out "/bin"))
                    (script (string-append bin "/prepare-gmes")))
               (with-output-to-file script
                 (lambda ()
                   (display
                    (string-append
                     "#!" (which "bash") "\n"
                     "set -x \n"
                     "echo '[INFO]	Copying [gm_key] to ~/.gm_key and configuration file [gmes.cfg] to .' 1>&2" "\n"
                     "cp -f " out "/share/gm_key" " $HOME/.gm_key" "\n"
                     "cp -f " out "/share/gmes.cfg" " . " "\n"))))
               (chmod script #o555))
             #t)))))
    (home-page "http://exon.gatech.edu/GeneMark/index.html")
    (synopsis "Eukaryotic gene prediction suite")
    (description "Eukaryotic gene prediction suite. Use the 'prepare-gmes' embedded script for setup with guix.")
    (license (license:non-copyleft "file://LICENSE"
                                   "See LICENSE in the distribution"))))

(define-public genometools
  (package
    (name "genometools")
    (version "1.6.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/genometools/genometools/archive/v"
                                  version
                                  ".tar.gz"))
              (sha256 (base32 "0gw65lpas3ag8cpdh9iml89jj08qalgflq2dc7w2mm7ilx1s332j"))))
    (build-system gnu-build-system)
    (inputs
     `(("zlib" ,zlib)
       ("bzip2" ,bzip2)
       ("lua" ,lua-5.1)
       ("lua-filesystem" ,lua5.1-filesystem)
       ("lua-lpeg" ,lua5.1-lpeg)
       ("md5" ,openssl)
       ("lua-md5" ,lua5.1-md5)
       ("libexpat" ,expat)
       ("libtre" ,tre)
       ("libcairo" ,cairo)
       ("libpango" ,pango)
       ("sqlite3" ,sqlite)
       ("libbam" ,htslib)
       ("gcc" ,gcc-9)))
    (propagated-inputs
     `(("ruby" ,ruby)
       ("perl" ,perl)
       ("python" ,python-wrapper)))
    (native-inputs
     `(("pkg-config" ,pkg-config)))
    (arguments
     `(#:make-flags
       (list "useshared=no"
             "verbose=yes"
             (string-append "CC=" (assoc-ref %build-inputs "gcc") "/bin/gcc")
             (string-append "prefix=" (assoc-ref %outputs "out")))
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))
       #:tests? #f))
    (home-page "http://genometools.org/")
    (synopsis "GenomeTools genome analysis system.")
    (description "The GenomeTools genome analysis system is a free collection
of bioinformatics tools (in the realm of genome informatics) combined into
a single binary named gt. It is based on a C library named libgenometools
which contains a wide variety of classes for efficient and convenient
implementation of sequence and annotation processing software.")
    (license license:isc)))

(define-public vstree
  (package
    (name "vstree")
    (version "2.3.1")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/genometools/vstree/archive/v"
                                  version
                                  ".tar.gz"))
              (sha256 (base32 "0xdcl6l9bfnzswwxg5kkwsrv0yzgc7yfw2fznxw5dd2ddbdyrd6q"))))
    (build-system gnu-build-system)
    (home-page "http://vmatch.de/")
    (synopsis "Vmatch large scale sequence analysis software.")
    (description "Versatile software tool for eﬃciently solving large scale
sequence matching tasks")
    (license license:isc)))

;; WARNING not working
(define-public genomethreader
  (let ((commit "f909ad9eec53e6516c601c3dfcf70b8cce0a2ac2")
        (revision "0"))
    (package
      (name "genomethreader")
      (version (git-version "1.7.3" revision commit))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/genometools/genomethreader")
               (commit commit)))
         (sha256 (base32 "0yvczsfpjyskff8cqm0xb11f7n85amipg5405wbcxlwlykd7hqc5"))))
      (build-system gnu-build-system)
      (inputs
       `(("genometools-src" ,(package-source genometools))
         ("gcc" ,gcc-9)))
      (arguments
       `(#:make-flags
         (list (string-append "prefix=" (assoc-ref %outputs "out"))
               (string-append "CC=" (assoc-ref %build-inputs "gcc") "/bin/gcc")
               (string-append "CXX=" (assoc-ref %build-inputs "gcc") "/bin/g++")
               (string-append "64bit=" "yes")
               (string-append "licensemanager=" "no")
               (string-append "opt=" "no"))
         #:phases
         (modify-phases %standard-phases
           (add-before 'unpack 'copy-gt
             (lambda* (#:key inputs #:allow-other-keys)
               (copy-recursively (assoc-ref inputs "genometools-src") "genometools.tar.gz")
               (invoke "tar" "xvf" "genometools.tar.gz" "--one-top-level=genometools" "--strip-components=1")
               (with-directory-excursion "genometools"
                 (invoke "make" "64bit=yes" "cairo=no" "opt=no" (string-append "CC=" (which "gcc"))))))
           (delete 'configure)
           (replace 'install
             (lambda () #t)))))
      (home-page "http://genomethreader.org/")
      (synopsis "GenomeThreader gene prediction software.")
      (description "GenomeThreader is a software tool to compute gene
structure predictions. The gene structure predictions are calculated using
a similarity-based approach where additional cDNA/EST and/or protein sequences
are used to predict gene structures via spliced alignments. GenomeThreader was
motivated by disabling limitations in GeneSeqer, a popular gene prediction
program which is widely used for plant genome annotation.")
      (license license:isc))))

;; (define-public prothint
;;   (package
;;     (name "prothint")
;;     (version "2.5.0")
;;     (inputs
;;      `(("perl" ,perl)
;;        ("python" ,python-3)
;;        ("perl-mce-mutex" ,perl-mce)
;;        ("perl-threads" ,perl-threads)
;;        ("perl-yaml" ,perl-yaml)
;;        ("perl-math-utils" ,perl-math-utils)))
;;     (propagated-inputs
;;      `(("diamond" ,diamond)
;;        ("spaln" ,spaln)
;;        ("spaln-boundary-scorer" ,spaln-boundary-scorer)
;;        ("genemark-es" ,genemark-es)))
;;     (home-page "https://github.com/gatech-genemark/ProtHint")
;;     (license (license:non-copyleft "file://LICENSE"
;;                                    "See LICENSE file in the distribution"))))
