;; GNU Guix for Horizon

;;; Copyright © 2020 Samuel Barreto <samuel.barreto8@gmail.com>

(define-module (horizon packages java)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages maven)
  #:use-module (gnu packages java)
  #:use-module (gnu packages xml)
  #:use-module (guix build-system ant)
  #:use-module (guix build-system maven)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public maven-jdependency
  (package
    (name "maven-jdependency")
    (version "2.4.0")
    (source (origin
              (method git-fetch)
              (uri
               (git-reference
                (url "https://github.com/tcurdt/jdependency")
                (commit (string-append "jdependency-" version))))
              (sha256
               (base32
                "1hy3mfvh6midxwzyd0c6bkwli6m7wsayxwmkiahsxwamqriafski"))))
    (build-system maven-build-system)
    (propagated-inputs
     `(("java-junit" ,java-junit)
       ("java-commons-io" ,java-commons-io)
       ("java-asm" ,java-asm)))
    (arguments
     `(#:tests? #f))
    (home-page "https://github.com/tcurdt/jdependency")
    (synopsis "jdependency is small library that helps you analyze class level dependencies, clashes and missing classes.")
    (description "Provides an API to analyse and modify class
dependencies. It provides the core to the maven shade plugin for
removing unused classes.")
    (license license:asl2.0)))

(define-public maven-shade-plugin
  (package
    (name "maven-shade-plugin")
    (version "3.2.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/apache/"
                                  "maven-shade-plugin/archive/"
                                  "maven-shade-plugin-" version ".tar.gz"))
              (sha256
               (base32
                "06zkrzpjii7rn2n2n8q03cjmgarbp3plwb5sbc3vka4xp8mwn4zb"))))
    (build-system ant-build-system)
    (arguments
     `(#:jar-name "maven-shade-plugin.jar"
                  #:source-dir "src/main/java"
                  #:tests? #f; test depends on maven-plugin-test-harness
                  #:phases
                  (modify-phases %standard-phases
                    ;; (add-before 'build 'generate-plugin.xml
                    ;;   (generate-plugin.xml "pom.xml"
                    ;;                        "jar"
                    ;;                        "src/main/java/org/apache/maven/plugins/shade"
                    ;;                        (list
                    ;;                         (list "AbstractJarMojo.java" "JarMojo.java")
                    ;;                         (list "AbstractJarMojo.java" "TestJarMojo.java"))))
                    (replace 'install
                      (install-from-pom "pom.xml")))))
    (propagated-inputs
     `(("java-asm" ,java-asm)
       ("java-commons-io" ,java-commons-io)
       ("java-commons-lang3" ,java-commons-lang3)
       ("java-guava" ,java-guava)
       ("java-jdom2" ,java-jdom2)
       ("java-junit" ,java-junit)
       ("java-mockito-core" ,java-mockito-1)
       ("java-plexus-archiver" ,java-plexus-archiver)
       ("java-plexus-component-annotations" ,java-plexus-component-annotations)
       ("java-plexus-utils" ,java-plexus-utils)
       ("java-xmlunit" ,java-xmlunit)
       ("maven-jdependency" ,maven-jdependency)
       ("maven-artifact-transfer" ,maven-artifact-transfer)
       ("maven-core" ,maven-3.0-core)
       ("maven-dependency-tree" ,maven-dependency-tree)
       ("maven-file-management" ,maven-file-management)
       ("maven-model" ,maven-model)
       ("maven-plugin-annotations" ,maven-plugin-annotations)
       ("maven-plugin-api" ,maven-3.0-plugin-api)
       ("maven-shared-utils" ,maven-shared-utils)
       ("maven-artifact" ,maven-3.0-artifact)))
    (home-page "https://maven.apache.org/plugins/maven-shade-plugin")
    (synopsis "Package the artifact in an uber-jar")
    (description "This plugin provides the capability to package the
artifact in an uber-jar, including its dependencies and to shade -
i.e. rename - the packages of some of the dependencies.")
    (license license:asl2.0)))
