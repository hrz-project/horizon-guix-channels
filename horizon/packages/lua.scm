(define-module (horizon packages lua)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages gcc)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system trivial)
  #:use-module (gnu packages))

(define-public lua5.1-md5
  (package
    (name "lua-md5")
    (version "1.3")
    (source (origin
              (method url-fetch)
              (uri (string-append "https://github.com/keplerproject/md5/archive/"
                                  version
                                  ".tar.gz"))
              (sha256
               (base32 "193dsjgnzrnykpmx68njkv72fxh2gb3llqgx2lgbgnf5i66shiq7"))))
    (build-system gnu-build-system)
    (inputs
     `(("lua" ,lua-5.1)
       ("gcc" ,gcc-9)))
    (arguments
     `(#:make-flags
       (let ((out (assoc-ref %outputs "out"))
             (lua-version ,(version-major+minor (package-version lua-5.1))))
         (list
          (string-append "CC=" (assoc-ref %build-inputs "gcc") "/bin/gcc")
          (string-append "PREFIX=" out)
          (string-append "LUA_LIBDIR=" out "/lib/lua/" lua-version)))
       #:tests? #f
       #:phases
       (modify-phases %standard-phases
         (delete 'configure)
         (add-before 'install 'create-dirs
           (lambda* (#:key outputs #:allow-other-keys)
             (let ((out (assoc-ref outputs "out"))
                   (lua-version ,(version-major+minor (package-version lua-5.1))))
               (mkdir-p (string-append out "/lib/lua/" lua-version))))))))
    (home-page "https://keplerproject.github.io/md5/")
    (synopsis "Checksum facilities for Lua 5.X")
    (description "MD5 offers checksum facilities for Lua 5.X: a hash (digest)
function, a pair crypt/decrypt based on MD5 and CFB, and a pair crypt/decrypt
based on DES with 56-bit keys.  Note that although MD5 was designed to be used
as a cryptographic hash function, it was found that key collisions could be
calculated in a few seconds, thus it should not be used where a cryptographic
hash is required.")
    (license license:expat)))
