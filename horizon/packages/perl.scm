(define-module (horizon packages perl)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((guix build utils) #:hide (delete which))
  #:use-module (guix build-system perl)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  ;;
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gcc)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages perl-check)
  ;;
  #:use-module (horizon packages compression))

(define-public perl-logger-simple
  (package
    (name "perl-logger-simple")
    (version "2.0")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/T/TS/TSTANLEY/Logger-Simple-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0b8v55nzy28r5vjdyddgvxlf9gj27fqbz89b2c15jnvp10szsqrf"))))
    (build-system perl-build-system)
    (propagated-inputs
     `(("perl-object-insideout" ,perl-object-insideout)
       ("perl-test-pod" ,perl-test-pod)))
    (home-page
     "https://metacpan.org/release/Logger-Simple")
    (synopsis
     "Implementation of the Simran-Log-Log and Simran-Error-Error modules")
    (description "Implementation of the Simran-Log-Log and Simran-Error-Error
modules")
    (license #f)))

(define-public perl-math-utils
  (package
  (name "perl-math-utils")
  (version "1.14")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "mirror://cpan/authors/id/J/JG/JGAMBLE/Math-Utils-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "1596bwij103w4w5m7102c7bj9x4sjr8a7cibp5qjcqkafgh0m8l8"))))
  (build-system perl-build-system)
  (native-inputs
    `(("perl-module-build" ,perl-module-build)))
  (home-page
    "https://metacpan.org/release/Math-Utils")
  (synopsis
    "Useful mathematical functions not in Perl")
  (description "Useful mathematical functions not in Perl")
  (license license:perl-license)))

(define-public perl-mce
  (package
    (name "perl-mce")
    (version "1.874")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/M/MA/MARIOROY/MCE-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1l6khsmwzfr88xb81kdvmdskxgz3pm4yz2ybxkbml4bmhh0y62fq"))))
    (build-system perl-build-system)
    (propagated-inputs
     `(("perl-sereal-decoder" ,perl-sereal-decoder)
       ("perl-sereal-encoder" ,perl-sereal-encoder)))
    (home-page "https://metacpan.org/release/MCE")
    (synopsis
     "Many-Core Engine for Perl providing parallel processing capabilities")
    (description "MCE spawns a pool of workers and therefore does not fork a new
process per each element of data. Instead, MCE follows a bank queuing
model. Imagine the line being the data and bank-tellers the parallel
workers. MCE enhances that model by adding the ability to chunk the next
n elements from the input stream to the next available
worker.")
    (license license:perl-license)))

(define-public perl-object-insideout
  (package
    (name "perl-object-insideout")
    (version "4.05")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/J/JD/JDHEDDEN/Object-InsideOut-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1i6aif37ji91nsyncp5d0d3q29clf009sxdn1rz38917hai6rzcx"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-module-build" ,perl-module-build)))
    (propagated-inputs
     `(("perl-exception-class" ,perl-exception-class)))
    (home-page
     "https://metacpan.org/release/Object-InsideOut")
    (synopsis
     "Comprehensive inside-out object support module")
    (description "Comprehensive inside-out object support module")
    (license license:perl-license)))

(define-public perl-sereal
  (package
    (name "perl-sereal")
    (version "4.018")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/Y/YV/YVES/Sereal-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0pqygrl88jp2w73jd9cw4k22fhvh5vcwqbiwl9wpxm67ql95cwwa"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-sereal-decoder" ,perl-sereal-decoder)
       ("perl-sereal-encoder" ,perl-sereal-encoder)
       ("perl-test-longstring" ,perl-test-longstring)
       ("perl-test-warn" ,perl-test-warn)))
    (propagated-inputs
     `(("perl-sereal-decoder" ,perl-sereal-decoder)
       ("perl-sereal-encoder" ,perl-sereal-encoder)))
    (home-page "https://metacpan.org/release/Sereal")
    (synopsis "Fast, compact, powerful binary (de-)serialization")
    (description "Sereal is an efficient, compact-output, binary and feature-rich serialization
protocol. The Perl encoder is implemented as the Sereal::Encoder module, the
Perl decoder correspondingly as Sereal::Decoder. They are distributed
separately to allow for safe upgrading without downtime.")
    (license license:perl-license)))

(define-public perl-sereal-decoder
  (package
    (name "perl-sereal-decoder")
    (version "4.018")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/Y/YV/YVES/Sereal-Decoder-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0wfdixpm3p94mnng474l0nh9mjiy8q8hbrbh2af4vwn2hmazr91f"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-test-deep" ,perl-test-deep)
       ("perl-test-differences" ,perl-test-differences)
       ("perl-test-longstring" ,perl-test-longstring)
       ("perl-test-warn" ,perl-test-warn)
       ("gcc" ,gcc-9)))
    (propagated-inputs
     `(("csnappy" ,csnappy)
       ("zstandard" ,zstd)
       ("zstandard-lib" ,zstd "lib")
       ("miniz" ,miniz)
       ("zlib" ,zlib)))
    (arguments
     `(#:make-maker-flags
       (map
        (lambda (x) (format #f "~a=~a" (car x) (cadr x)))
        `(("INC" ,(format #f "~{-I~a ~}"
                          (let ((inp (lambda (in rest) (string-append (assoc-ref %build-inputs in) rest))))
                            (list
                             (inp "zstandard-lib" "/include")
                             (inp "csnappy" "/include")
                             (inp "miniz" "/include/miniz")))))
          ("LIBS" ,(string-append
                    (format #f "~{-L~a ~}"
                            (let ((lib (lambda (in rest) (string-append (assoc-ref %build-inputs in) rest))))
                              (list
                               (lib "zstandard-lib" "/lib")
                               (lib "csnappy" "/lib")
                               (lib "miniz" "/lib"))))
                    (format #f "~{-l~a~}" '("miniz " "csnappy " "zstd"))))))
       #:phases
       (modify-phases %standard-phases
         (add-before 'configure 'setenv
           (lambda _ (setenv "SEREAL_USE_BUNDLED_LIBS" "0") #t)))))
    (home-page
     "https://metacpan.org/release/Sereal-Decoder")
    (synopsis
     "Fast, compact, powerful binary deserialization")
    (description "Fast, compact, powerful binary deserialization")
    (license license:perl-license)))

(define-public perl-sereal-encoder
  (package
    (inherit perl-sereal-decoder)
    (name "perl-sereal-encoder")
    (version "4.018")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/Y/YV/YVES/Sereal-Encoder-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0z9dbkr8ggjqb5g1sikxhy1a359bg08gs3vfg9icqm6xx4gjsv6p"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-sereal-decoder" ,perl-sereal-decoder)
       ("perl-test-deep" ,perl-test-deep)
       ("perl-test-differences" ,perl-test-differences)
       ("perl-test-longstring" ,perl-test-longstring)
       ("perl-test-warn" ,perl-test-warn)
       ("gcc" ,gcc-9)))
    (home-page
     "https://metacpan.org/release/Sereal-Encoder")
    (synopsis
     "Fast, compact, powerful binary serialization")
    (description "Fast, compact, powerful binary serialization")
    (license license:perl-license)))
