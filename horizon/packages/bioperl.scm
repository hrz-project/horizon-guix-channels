(define-module (horizon packages bioperl)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module ((guix build utils) #:hide (delete which))
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system perl)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  ;;
  #:use-module (gnu packages perl)
  #:use-module (gnu packages perl-web)
  #:use-module (gnu packages perl-check)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages bioinformatics))

;; NOTE
;; this is sorted alphabetically, as it should be.

;; (define-public bioperl-minimal
;;   (let ((commit "d98d5ad3ceb47e672cf891df0c9ee6a62b059efd")
;;         (revision "0"))
;;     (package
;;       (inherit bioperl-minimal)
;;       (name "bioperl-minimal")
;;       (version (git-version "1.7.7" revision commit))
;;       (source (origin
;;                 (method git-fetch)
;;                 (uri (git-reference
;;                       (url "https://github.com/bioperl/bioperl-live")
;;                       (commit commit)))
;;                 (file-name (git-file-name name version))
;;                 (sha256
;;                  (base32
;;                   "1h9iw39zsajrv482i22y6xj9ml02imgzznwddr52pz0mqrqp9in2")))))))

(define-public perl-bio-asn1-entrezgene
  (package
    (name "perl-bio-asn1-entrezgene")
    (version "1.73")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/C/CJ/CJFIELDS/Bio-ASN1-EntrezGene-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0jvaziwmw7sanyhkl98lvkglwr0b96l3i3krs9dc7rawf3dpirzr"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-test-most" ,perl-test-most)
       ("perl-bioperl" ,bioperl-minimal)))
    (propagated-inputs
     `(("perl-bio-cluster" ,perl-bio-cluster)
       ("perl-bioperl" ,bioperl-minimal)))
    (home-page
     "https://metacpan.org/release/Bio-ASN1-EntrezGene")
    (synopsis
     "Regular expression-based Perl Parser for NCBI Entrez Gene.")
    (description "Regular expression-based Perl Parser for NCBI Entrez Gene.")
    (license license:perl-license)))

(define-public perl-bio-cluster
  (package
    (name "perl-bio-cluster")
    (version "1.7.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/C/CJ/CJFIELDS/Bio-Cluster-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1fqz36v21lklgdgwm5hsgd1gihvg0zpn9jznbddj8bxrk4wgnrqr"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-test-most" ,perl-test-most)
       ("perl-bioperl" ,bioperl-minimal)))
    (propagated-inputs
     `(("perl-bioperl" ,bioperl-minimal)
       ("perl-bio-variation" ,perl-bio-variation)
       ("perl-bioperl" ,bioperl-minimal)
       ("perl-xml-sax" ,perl-xml-sax)))
    (home-page
     "https://metacpan.org/release/Bio-Cluster")
    (synopsis "BioPerl cluster modules")
    (description "BioPerl cluster modules")
    (license license:perl-license)))

(define-public perl-bio-db-embl
  (package
    (name "perl-bio-db-embl")
    (version "1.7.4")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/C/CJ/CJFIELDS/Bio-DB-EMBL-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "09qsgspa6aglhynpfzxyigpqc2qdf0lhvkyhkx9cqyyvng281q7w"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-test-most" ,perl-test-most)
       ("perl-http-request-common" ,perl-http-message)
       ("perl-test-requiresinternet" ,perl-test-requiresinternet)))
    (propagated-inputs
     `(("perl-bioperl" ,bioperl-minimal)
       ("perl-libwww" ,perl-libwww)
       ("perl-io-string" ,perl-io-string)))
    (arguments `(#:tests? #f))
    (home-page
     "https://metacpan.org/release/Bio-DB-EMBL")
    (synopsis
     "Database object interface for EMBL entry retrieval")
    (description "Database object interface for EMBL entry retrieval")
    (license license:perl-license)))

(define-public perl-bio-db-ncbihelper
  (package
    (name "perl-bio-db-ncbihelper")
    (version "1.7.6")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/C/CJ/CJFIELDS/Bio-DB-NCBIHelper-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1ixwwcqwm09z5kbbmrwjxq5ppgj9c8xgxd2jqgz9352y8zwgwxb4"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-test-exception" ,perl-test-exception)
       ("perl-test-most" ,perl-test-most)
       ("perl-test-requiresinternet"
        ,perl-test-requiresinternet)))
    (propagated-inputs
     `(("perl-bio-asn1-entrezgene" ,perl-bio-asn1-entrezgene)
       ("perl-bioperl" ,bioperl-minimal)
       ("perl-cache-cache" ,perl-cache-cache)
       ("perl-cgi" ,perl-cgi)
       ("perl-http-message" ,perl-http-message)
       ("perl-libwww" ,perl-libwww)
       ("perl-lwp-protocol-https" ,perl-lwp-protocol-https)
       ("perl-uri" ,perl-uri)
       ("perl-xml-twig" ,perl-xml-twig)))
    (arguments `(#:tests? #f))
    (home-page
     "https://metacpan.org/release/Bio-DB-NCBIHelper")
    (synopsis
     "A collection of routines useful for queries to NCBI databases.")
    (description "A collection of routines useful for queries to NCBI databases.")
    (license license:perl-license)))

(define-public perl-bio-db-refseq
  (package
    (name "perl-bio-db-refseq")
    (version "1.7.4")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/C/CJ/CJFIELDS/Bio-DB-RefSeq-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1azy80vc461s1kf2wzb4z3kzsla98wskkil64jfaz1nc4x4f9x7k"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-test-needs" ,perl-test-needs)
       ("perl-test-requiresinternet" ,perl-test-requiresinternet)))
    (propagated-inputs
     `(("perl-libwww" ,perl-libwww)
       ("perl-io-string" ,perl-io-string)
       ("perl-bioperl" ,bioperl-minimal)))
    (home-page
     "https://metacpan.org/release/Bio-DB-RefSeq")
    (synopsis "Database object interface for RefSeq retrieval")
    (description "Database object interface for RefSeq retrieval")
    (license license:perl-license)))

(define-public perl-bio-db-swissprot
  (package
    (name "perl-bio-db-swissprot")
    (version "1.7.4")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/C/CJ/CJFIELDS/Bio-DB-SwissProt-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0w5iv57adll8hmywfwx8rixyz7jmc49im5iyvfr8k8srxlvy8ic2"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-test-needs" ,perl-test-needs)
       ("perl-test-requiresinternet"
        ,perl-test-requiresinternet)))
    (propagated-inputs
     `(("perl-bioperl" ,bioperl-minimal)
       ("perl-libwww" ,perl-libwww)
       ("perl-io-string" ,perl-io-string)
       ("perl-http-message" ,perl-http-message)))
    (home-page
     "https://metacpan.org/release/Bio-DB-SwissProt")
    (synopsis
     "Database object interface to SwissProt retrieval")
    (description "Database object interface to SwissProt retrieval")
    (license license:perl-license)))

(define-public perl-bio-kmer
  (package
    (name "perl-bio-kmer")
    (version "0.41")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/L/LS/LSKATZ/Bio-Kmer-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0hzrfz2gdv6q13mf7a5z5lhvlri45jnh9hv1ba3knlg966360ff6"))))
    (build-system perl-build-system)
    (propagated-inputs
     `(("perl-bio-procedural" ,perl-bio-procedural)
       ("perl-file-which" ,perl-file-which)))
    (home-page
     "https://metacpan.org/release/Bio-Kmer")
    (synopsis "Helper module for Kmer Analysis.")
    (description "Helper module for Kmer Analysis.")
    (license license:x11)))

(define-public perl-bio-procedural
  (package
    (name "perl-bio-procedural")
    (version "1.7.4")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/C/CJ/CJFIELDS/Bio-Procedural-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1nni65fj4pvz0dqhd8mc3a51qfw1qcm83kyn1vcf5vlin3xrrgfj"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-bioperl" ,bioperl-minimal)
       ("perl-test-most" ,perl-test-most)
       ("perl-module-build" ,perl-module-build)))
    (propagated-inputs
     `(("perl-bio-db-embl" ,perl-bio-db-embl)
       ("perl-bio-db-ncbihelper" ,perl-bio-db-ncbihelper)
       ("perl-bio-db-refseq" ,perl-bio-db-refseq)
       ("perl-bio-db-swissprot" ,perl-bio-db-swissprot)
       ("perl-bio-tools-run-remoteblast" ,perl-bio-tools-run-remoteblast)
       ("perl-bioperl" ,bioperl-minimal)))
    (arguments `(#:tests? #f))
    (home-page
     "https://metacpan.org/release/Bio-Procedural")
    (synopsis
     "Simple low-dependency procedural interfaces to BioPerl")
    (description  "Simple low-dependency procedural interfaces to BioPerl")
    (license license:perl-license)))

(define-public perl-bio-sketch
  (package
    (name "perl-bio-sketch")
    (version "0.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/L/LS/LSKATZ/Bio-Sketch-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "1qla1i1a3jvv08an8qc0825wlyk6rx7glg5sf868xqqar198bhvs"))))
    (build-system perl-build-system)
    (propagated-inputs
     `(("perl-class-interface" ,perl-class-interface)))
    (arguments
     `(#:tests? #f))
    (home-page "https://metacpan.org/release/Bio-Sketch")
    (synopsis "An interface module for Sketches, e.g., Mash")
    (description "An interface module for Sketches, e.g., Mash.")
    (license license:gpl3)))

(define-public perl-bio-sketch-mash
  (package
    (name "perl-bio-sketch-mash")
    (version "0.9")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/L/LS/LSKATZ/Bio-Sketch-Mash-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "100j0jyrwd87vymvpixvfrafpd61jighsmvdn8my4x2nqy5qa5ga"))))
    (build-system perl-build-system)
    (arguments
     `(#:tests?
       #f
       #:phases
       (modify-phases %standard-phases
         (add-before 'reset-gzip-timestamps 'make-compressed-files-writable
           ;; The 'reset-gzip-timestamps phase will throw a permission error
           ;; if gzip files aren't writable then.  This phase is needed when
           ;; building from a git checkout.
           (lambda _ (for-each make-file-writable (find-files %output ".*\\.t?gz$"))
             #t)))))
    (propagated-inputs
     `(("perl-bio-sketch" ,perl-bio-sketch)
       ("perl-class-interface" ,perl-class-interface)
       ("perl-json" ,perl-json)))
    (home-page
     "https://metacpan.org/release/Bio-Sketch-Mash")
    (synopsis "A module to read `mash info` output and transform it")
    (description "This is a module to read mash files produced by the Mash
executable. For more information on Mash, see mash.readthedocs.org. This
module is capable of reading mash files. Future versions will read/write mash
files.")
    (license license:gpl3)))

(define-public perl-bio-tools-run-remoteblast
  (package
    (name "perl-bio-tools-run-remoteblast")
    (version "1.7.3")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/C/CJ/CJFIELDS/Bio-Tools-Run-RemoteBlast-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0s1pqh90zdmrm4j29bz452za5sgbry0kli1kimqgm4lviks02sdq"))))
    (build-system perl-build-system)
    (propagated-inputs
     `(("perl-bioperl" ,bioperl-minimal)
       ("perl-http-message" ,perl-http-message)
       ("perl-io-string" ,perl-io-string)
       ("perl-libwww" ,perl-libwww)))
    (home-page
     "https://metacpan.org/release/Bio-Tools-Run-RemoteBlast")
    (synopsis
     "Object for remote execution of the NCBI Blast via HTTP")
    (description "Object for remote execution of the NCBI Blast via HTTP")
    (license license:perl-license)))

(define-public perl-bio-variation
  (package
    (name "perl-bio-variation")
    (version "1.7.5")
    (source
     (origin
       (method url-fetch)
       (uri (string-append
             "mirror://cpan/authors/id/C/CJ/CJFIELDS/Bio-Variation-"
             version
             ".tar.gz"))
       (sha256
        (base32
         "0kw23zjk2g1qigb0mrpj9pnra09zxdb61r00ywckjyay1c3dvzsb"))))
    (build-system perl-build-system)
    (native-inputs
     `(("perl-test-most" ,perl-test-most)
       ("perl-bioperl" ,bioperl-minimal)))
    (propagated-inputs
     `(("perl-bioperl" ,bioperl-minimal)
       ("perl-io-string" ,perl-io-string)
       ("perl-xml-twig" ,perl-xml-twig)
       ("perl-xml-writer" ,perl-xml-writer)))
    (home-page
     "https://metacpan.org/release/Bio-Variation")
    (synopsis
     "BioPerl variation-related functionality")
    (description "BioPerl variation-related functionality")
    (license license:perl-license)))

(define-public perl-class-interface
  (package
  (name "perl-class-interface")
  (version "1.01")
  (source
    (origin
      (method url-fetch)
      (uri (string-append
             "mirror://cpan/authors/id/S/SI/SINISTER/Class-Interface-"
             version
             ".tar.gz"))
      (sha256
        (base32
          "0ysb8izdffhyd90id42qx3115z1xvacfdfd0a7s3gba5lz6ibflz"))))
  (build-system perl-build-system)
  (arguments `(#:tests? #f))
  (home-page
    "https://metacpan.org/release/Class-Interface")
  (synopsis
   "A class for implementing/extending interfaces/abstracts in Perl.")
  (description "A class for implementing/extending interfaces/abstracts in
Perl.")
  (license #f)))
